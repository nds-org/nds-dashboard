# README #

National Data Service Labs Dashboard (Experimental)
===================================================

Pilot implementation of the NDS site.

To install:

```git clone https://bitbucket.org/nds-org/nds-dashboard```

Then change write permissions:

(on a mac, running apache)

```sudo chown -R _www nds-dashboard/protected/data/```

```sudo chown -R _www nds-dashboard/protected/runtime/```

```sudo chown -R _www nds-dashboard/public/assets```

(on ubuntu, running apache)

```sudo chown -R www-data nds-dashboard/protected/data/```

```sudo chown -R www-data nds-dashboard/protected/runtime/```

```mkdir nds-dashboard/public/assets```

```sudo chown -R www-data nds-dashboard/public/assets```

Create the following user on the DB

```username = ndsadmin```

```password = nds2adminmach1ne```

```mysql -u ndsadmin -p < nds-dashboard/database/nds.sql```

Enter password: ```nds2adminmach1ne```


lastly, in the nds-pilot directory, there are the following subdirectories:

```database  protected  public  yii```

You need the public directory to replace the root of your public html directory and the other directories outside the
public directory. So, keep the same structure and pointing the webroot to public is the optimal from a security
standpoint.

By this stage you should be able to browse the site...


If you want to watch a video demo of the dashboard, go here:

http://ndspilot.com/nds/ndspilot1080p.mp4


Docker
======

To build the container, from the root directory run:

```docker build -t ndslabs/nds-dashboard .``` 

To run the container, you need a DB container first:

```docker run -d --name db training/postgres:latest```

Then, run the dashboard container, and link it to the DB:

Interactive:
```docker run -it -p 127.0.0.1:8080:80 --link db:db --name dash ndslabs/nds-dashboard```

Daemonised:
```docker run -d -p 127.0.0.1:8080:80 --link db:db --name dash ndslabs/nds-dashboard```

The container expects the database to be pre-populated, rather than auto-populating, negating the risk of overwriting production data.
To populate the DB, after the DB container is running, replace ```/home/keyz/Code/nds-dashboard/database``` with the path to the psql file, then run:

```docker run -it --name db_client --link db:db -v /home/keyz/Code/nds-dashboard/database:/mnt training/postgres:latest /bin/bash```

Once inside, run:

```psql -h $DB_PORT_5432_TCP_ADDR -p $DB_PORT_5432_TCP_PORT -U docker -W docker -f /mnt/nds.psql```

Enter the password ```docker``` when prompted.
