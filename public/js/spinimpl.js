(function($) {
    $(function() {
        var opts = {
          lines: 13, // The number of lines to draw
          length: 30, // The length of each line
          width: 10, // The line thickness
          radius: 50, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 0, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#000', // #rgb or #rrggbb or array of colors
          speed: 1, // Rounds per second
          trail: 60, // Afterglow percentage
          shadow: true, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: '25%', // Top position relative to parent
          left: '50%' // Left position relative to parent
        };

        var spinner = new Spinner(opts);

        function startSpinner() {
            window.scrollTo(0, 0);

            document.getElementById('blackout').style.display = 'block';
            var target = document.getElementById('spinner');
            spinner.spin(target);
        }

        function stopSpinner() {
            document.getElementById('blackout').style.display = 'none';
            spinner.stop();
        }

        String.prototype.endsWith = function (s) {
          return this.length >= s.length && this.substr(this.length - s.length) == s;
        }

        String.prototype.startsWith = function( str ) {
            return this.substring( 0, str.length ) === str;
        }

        $('a').click(function() {
            var address = $(this).attr('href');

            if ( (address!=null) && (address.indexOf('#')<0) && (!address.startsWith("site/")) && (address.indexOf('page=')<0) && (!address.startsWith('javascript')) )
                startSpinner();
        });

        $('input.btn').click(function() {
            if ($(this).attr('type') != file) {
                $('input:submit').attr("disabled", true);
                startSpinner();
            }
        });

        $('form').submit(function(event) {
             $('input:submit').attr("disabled", true);
             startSpinner();
        });

    });

})(jQuery);
