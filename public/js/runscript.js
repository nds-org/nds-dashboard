window.runUpdateInterval = 500;
window.scriptState="Initialized";
window.scriptTerminal="Awaiting Instruction...";

var getProgress = function(baseurl, resourceID) {

    $.get(baseurl + "resource/status/resource_id/" + resourceID, function(data) {
        if (window.scriptState !== data) {
            window.scriptState = data;
            $('.script-state').html(window.scriptState);

            if (window.scriptState == "COMPLETED") {
                $('.button.download').show();
            } else {
                $('.button.download').hide();
            }

            }
        });

    $.get(baseurl + "resource/terminal/resource_id/" + resourceID, function(data) {
        if (window.scriptTerminal !== data) {
            window.scriptTerminal = data;
                $('.script-terminal').html(window.scriptTerminal);
            }
        });

    window.runtimerID = setTimeout(function(){getProgress(baseurl, resourceID);}, runUpdateInterval);
    };



