window.scaninterval=10000; // check for updates every 10 seconds.
window.systemAlertInterval=5000; // check for updates every 5 seconds.

function unwrapStringOrNumber(obj) {
    return (obj instanceof Number || obj instanceof String
        ? obj.valueOf()
        : obj);
}
function areEquivalent(a, b) {
    a = unwrapStringOrNumber(a);
    b = unwrapStringOrNumber(b);
    if (a === b) return true; //e.g. a and b both null
    if (a === null || b === null || typeof (a) !== typeof (b)) return false;
    if (a instanceof Date)
        return b instanceof Date && a.valueOf() === b.valueOf();
    if (typeof (a) !== "object")
        return a == b; //for boolean, number, string, xml

    var newA = (a.areEquivalent_Eq_91_2_34 === undefined),
        newB = (b.areEquivalent_Eq_91_2_34 === undefined);
    try {
        if (newA) a.areEquivalent_Eq_91_2_34 = [];
        else if (a.areEquivalent_Eq_91_2_34.some(
            function (other) { return other === b; })) return true;
        if (newB) b.areEquivalent_Eq_91_2_34 = [];
        else if (b.areEquivalent_Eq_91_2_34.some(
            function (other) { return other === a; })) return true;
        a.areEquivalent_Eq_91_2_34.push(b);
        b.areEquivalent_Eq_91_2_34.push(a);

        var tmp = {};
        for (var prop in a)
            if(prop != "areEquivalent_Eq_91_2_34")
                tmp[prop] = null;
        for (var prop in b)
            if (prop != "areEquivalent_Eq_91_2_34")
                tmp[prop] = null;

        for (var prop in tmp)
            if (!areEquivalent(a[prop], b[prop]))
                return false;
        return true;
    } finally {
        if (newA) delete a.areEquivalent_Eq_91_2_34;
        if (newB) delete b.areEquivalent_Eq_91_2_34;
    }
}

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

function getItem(subject, content, picurl, memberName, date, content_url) {
    return ' ' +
        '<div class="event"> '+
            '<div class="label"> ' +
                    '<img src="' + picurl +'">' +
            '</div>' +
            '<div class="content"> ' +
              '<div class="summary">' +
                  '<a href="' + content_url + '">' + subject + '</a>' +
                 '<div class="date">' +
                    '<i class="icon info"></i>' + date +
                 '</div>' +
              '</div>' +
              '<div class="extra text"><span style="font-style: oblique;">'  +
              '</div>' + content +
            '</div>' +
       '</div>';
}

var checkForSystemAlerts = function(baseurl) {
    $.get(baseurl + "alert/getAlert", function(data) {
        var cleandata = data.trim();
        if (cleandata == "gotodash") { // reset user because alert has finished
            var url = baseurl + "dashboard/home";
            location.href = url;
        }
        if (cleandata != -1) {
            $.get(baseurl + "alert/getalert/id/"+data, function(data) {
                location.href = baseurl + "alert/showalert/id/"+data;
            });
        }
    });
    window.messagetimerID = setTimeout(function(){checkForSystemAlerts(baseurl);}, window.systemAlertInterval);
};


window.alertsTotals= null;
window.alertsContent= null;


var getAlerts = function(alertsJSON) {

    //  moveDropDowns();

    var alerts = $.parseJSON(alertsJSON);

// icon, color, format Name, CSS Class for total, CSS class for content, total count url, content url

    if (window.alertsContent == null) {
        window.alertsContent = new Array(alerts.length);
        window.alertsTotals = new Array(alerts.length);
    }

//    alert(alertsJSON);

    for (var i=0; i<alerts.length; ++i) {
        (function(index) {
            var line = alerts[index];
            var icon = line[0];
            var color = line[1];
            var title = line[2];
            var css_total = line[3];
            var css_content = line[4];
            var url_total = line[5];
            var url_content = line[6];
            var raw_html = line[7];

            $.get(url_total, function(data) {
                if (window.alertsTotals[index] !== data) {
                    window.alertsTotals[index] = data;
                    $('.' + css_total).each(function() {
                        $(this).html(window.alertsTotals[index]);
                    });
                }
            });

            $.getJSON(url_content, function(data) {
                if (!areEquivalent(window.alertsContent[index], data)) {
                    window.window.alertsContent[index] = data;
                    var toDisplay = null;
                    if (raw_html) {
                        toDisplay = data[0];
                    } else { // it is a list
                        var listHtml = "";
                        $.each(data, function(key,value){
                            var subject = decodeURIComponent(value[0]);
                            var content = decodeURIComponent(value[1]);
                            var memberName = decodeURIComponent(value[2]);
                            var picurl = decodeURIComponent(value[3]);
                            var content_url = decodeURIComponent(value[4]);
                            var date = decodeURIComponent(value[5]);

                            listHtml += getItem(subject, content, picurl, memberName, date, content_url);
                        });

                         toDisplay =
                            '<div class="ui small feed">'+
                                 listHtml +
                             '</div>';
                    }

                    $('.' + css_content).each(function() {
                        $(this).html(toDisplay);
                    });
                }
            });
        })(i);
    }








    window.messagetimerID = setTimeout(function(){getAlerts(alertsJSON);}, scaninterval);
};


