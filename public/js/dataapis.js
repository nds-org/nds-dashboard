window.protocol_selection="";

var URL_HTTP=1;
var URL_IRODS=2;
var URL_DOCKER_FILES=3;
var URL_DROP_BOX =4;
var URL_GOOGLE_DRIVE =5;
var URL_SCIDRIVE = 6;

// SCIDRIVE JS

var SciDriveCallback;
var scidriveWindow;
window.baseurl=null;
window.articleid=null;

var SciDriveMessageHandler = function(e) {
	var result = JSON.parse(e.data);
	scidriveWindow.close();
	if(typeof SciDriveCallback !== "undefined"){
	    SciDriveCallback(result);
	}
	window.removeEventListener('message',  SciDriveMessageHandler, false);
};

function sciDriveGetFile(callback) {
	window.addEventListener('message',  SciDriveMessageHandler, false);
    scidriveWindow = window.open('//cadac-demo.sdsc.edu/scidrive_old/scidrive_chooser.html', 'authWindow', 'width=600, height=400');
    SciDriveCallback = callback;
}

var setRes = function(result) {

  //  alert("Url is--" + result.url + "--Path--" +  result.path + "--Path--");

    //	document.getElementById("res_url").setAttribute("href", result.url);
    //	document.getElementById("res_url").innerHTML = result.path;

   var data = addURLToArticle(window.baseurl, window.articleid, result.url, result.path);
   showModal(window.baseurl, window.articleid, "Scidrive", "Scidrive Import Complete", data, false);

//	console.debug(result);
};

// SCIDRIVE JS

function doAjaxSynCall(url) {

    var strReturn = "";

      jQuery.ajax({
        url: url,
        success: function(data) {
          strReturn = data;
        },
        async:false
      });

      return strReturn;
}

function addURLToArticle(baseURL, articleID, newURL, name) {
    var article_url = baseURL + "resource/addurl?article_id=" + articleID + "&url=" + encodeURI(newURL) + "&name=" + encodeURI(name);
    return doAjaxSynCall(article_url);
}

function browseRepository(baseURL, url) {
    var browse_url = baseURL + "article/filelist/protocol/" + url;
    return doAjaxSynCall(browse_url);
}

function doArticleModalUpdate(baseURL, articleID, url, name) {
    var data = addURLToArticle(baseURL, articleID, url, name);
    showModal(baseURL, articleID, "GoogleDrive", "Chooser Selection Of File Status", data, false);
}

function showModal(baseURL, articleID, titleInfo, statusInfo, actionInfo, showcancel, doCallback) {
                doCallback = typeof doCallback !== "undefined" ? doCallback : false;

                var status = $(".infolog.status");
                var action = $(".infolog.action");
                var title = $(".infolog.title");
                var cancelbut = $(".cancel-enable");

                if (showcancel)
                    cancelbut.show();
                else
                    cancelbut.hide();

                title.html(titleInfo);
                status.html(statusInfo);
                action.html(actionInfo);

                // need this in case a dropdown is added ....

                $(".ui.dropdown").dropdown();

                $(".info.modal")
                    .modal("setting", {
                    onDeny    : function(){
                        return true;
                    },
                    onApprove : function() {
                        if (doCallback) {
                            var selected = $('.epiphyte.item.active');
                            var url = $(selected).data("value");
                            var name = $(selected).html();

                            setTimeout(function() {
                                doArticleModalUpdate(baseURL, articleID, url, name);
                            }, 1000);
                            }
                        return true;
                    }
                }).modal("show");
}


function initDataAPIs(baseURL, articleID) {
    window.baseurl = baseURL;
    window.articleid = articleID;

    var options = {

        // Required. Called when a user selects an item in the Chooser.
        success: function(files) {
            var data = addURLToArticle(baseURL, articleID, files[0].link, files[0].name);
            showModal(baseURL, articleID, "Dropbox", "Chooser Selection Of File Status", data, false);
        },

        // Optional. Called when the user closes the dialog without selecting a file
        // and does not include any parameters.
        cancel: function() {

        },

        // Optional. "preview" (default) is a preview link to the document for sharing,
        // "direct" is an expiring link to download the contents of the file. For more
        // information about link types, see Link types below.
        linkType: "preview", // or "direct"

        // Optional. A value of false (default) limits selection to a single file, while
        // true enables multiple file selection.
        multiselect: false, // or true

        // Optional. This is a list of file extensions. If specified, the user will
        // only be able to select files with these extensions. You may also specify
        // file types, such as "video" or "images" in the list. For more information,
        // see File types below. By default, all extensions are allowed.
        extensions: [".pdf", ".doc", ".docx"]
    };

    $(".item").on("click", function (val) {
         var new_val = $(this).data("value");
         window.protocol_selection = new_val;
         if (new_val ==URL_GOOGLE_DRIVE) {
             $( ".button.browse" ).attr("id", "pick");
             var picker = new FilePicker({
             				apiKey: "AIzaSyB8GO_fuy4VRkh1y32XTQjYlfDY0fqHv_o",
             				clientId: "998908397634-outhrcdlv493hkjtldunuk0pj9laqmur.apps.googleusercontent.com",
             				buttonEl: document.getElementById("pick"),
             				onSelect: function(file, url) {
                                var data = addURLToArticle(baseURL, articleID, url, file.title);
                                showModal(baseURL, articleID, "GoogleDrive", "Chooser Selection Of File Status", data, false);
             				}
             			});
         } else {
             $( ".button.browse" ).attr("id", "chooser");
             }
     });

     $( ".button.url" ).on( "click", function() {
         var url = $("#url_address").val();

         var data = addURLToArticle(baseURL, articleID, url, "");
         showModal(baseURL,articleID, "Adding URL", "Status Of Adding URL ", data, false);
     });

     $( ".button.browse" ).on( "click", function() {
        if (window.protocol_selection == URL_DROP_BOX) {
             Dropbox.choose(options);
        } else if (window.protocol_selection == URL_SCIDRIVE) {
            sciDriveGetFile(setRes);
        } else if (window.protocol_selection == URL_DOCKER_FILES) {
            var choices = browseRepository(baseURL, window.protocol_selection);
            showModal(baseURL, articleID, "Browsing Data Service", "Choose File From The List Below", choices, true, true);
        }
     });
}
