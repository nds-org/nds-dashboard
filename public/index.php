<?php

ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3000); //300 seconds = 50 minutes


// change the following paths if necessary
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$config=dirname(__FILE__).'/../protected/config/main.php';

// remove the following lines when in production mode
 defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
//defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',5);

error_reporting(E_ALL);
ini_set('display_errors', 'on');

require_once($yii);

Yii::createWebApplication($config)->run();
