FROM ubuntu:trusty

MAINTAINER Kieran David Evans

# install nginx
RUN apt-get update
RUN apt-get install -y software-properties-common supervisor
RUN apt-get update
RUN add-apt-repository -y ppa:nginx/stable
RUN apt-get install -y nginx

# install php
RUN apt-get install -y nginx php5-fpm php5-cli php5-mcrypt php5-gd php5-pgsql

RUN mkdir /www/

ADD yii /www/yii
ADD public /www/public
ADD protected /www/protected
#ADD database /www/database

RUN chown -R www-data:www-data /www/protected/data/
RUN chown -R www-data:www-data /www/protected/runtime/
RUN mkdir /www/public/assets
RUN chown -R www-data:www-data /www/public/assets/

RUN php5enmod pdo_pgsql

ADD /Docker/nginx.conf /etc/nginx/sites-available/default
ADD /Docker/startFPMWithDockerEnvs.sh /opt/startFPMWithDockerEnvs.sh
RUN chmod +x /opt/startFPMWithDockerEnvs.sh
ADD /Docker/supervisord.conf /etc/supervisor/supervisord.conf
RUN mkdir -p /var/log/supervisor

EXPOSE 80:8080

# The whole container runs as if it was just the supervisord executable
ENTRYPOINT ["/usr/bin/supervisord"]
 
# We use CMD to supply a default argument to the entrypoint command (if none is specified)
# NOTE1: CMD in a Dockerfile is completely replaced by what we provide on the commandline.
# NOTE2: An image imported via docker pull (or docker import) won't know what command to run. Any image will lose all of its associated metadata on export
CMD ["--nodaemon"]
