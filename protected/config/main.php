<?php

$db_host = getenv('DB_PORT_5432_TCP_ADDR');
$db_port = getenv('DB_PORT_5432_TCP_PORT');

if($db_host && $db_port){
    $db_conf = array(
        'connectionString' => 'pgsql:host='.$db_host.';port='.$db_port.';dbname=docker',
        'schemaCachingDuration' => 600,
        'emulatePrepare' => true,
        'username' => 'docker',
        'password' => 'docker',
        'charset' => 'utf8',
    );
}else{
    $db_conf = array(
        'connectionString' => 'mysql:host=127.0.0.1;dbname=nds',
        'schemaCachingDuration' => 600,
        'emulatePrepare' => true,
        'username' => 'ndsadmin',
        'password' => 'nds2adminmach1ne',
        'charset' => 'utf8',
        //'enableProfiling' => true,
        'initSQLs'=>array("set time_zone='+00:00';"),
    );
}


// This is the main Web application configuration. Any writable
// application properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'NDS',

    'defaultController' => 'site/index',   // <--- add this line and replace with correct controller/action

        // preloading 'log' component
    'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
        'application.components.*',
        'application.components.encrypter.*',
        'application.components.yii-mail.*',
        'application.components.geodata.*',
        'application.components.media.*',
        'application.components.ajax.*',
        'application.components.urls.*',
        'application.components.tablesorter.*',
        'application.components.time.*',
        'application.components.widgets.*',
        'application.components.geodata.gmap.*',
	),

      // RequireLogin is called before anything is done.
    'behaviors' => array(
        'onBeginRequest' => array(
            'class' => 'application.components.RequireLogin'
        )
    ),

    'modules'=>array(
         // uncomment the following to enable the Gii tool

         'gii'=>array(
             'generatorPaths'=>array(
                 'bootstrap.gii',
             ),
             'class'=>'system.gii.GiiModule',
             'password'=>'easy4you',
             // If removed, Gii defaults to localhost only. Edit carefully to taste.
             'ipFilters'=>array('127.0.0.1','::1'),
         ),
     ),

	// application components
	'components'=>array(

        'encrypter'=>array (
            'class'=>'Encrypter',
            'key'=>'f1sh&ch1psarec00l',
        ),

        'user'=>array(
             'loginUrl'=>array('/member/login'),
             // enable cookie-based authentication
             'allowAutoLogin'=>true,
         ),

         // uncomment the following to enable URLs in path-format

         'urlManager'=>array(
             'urlFormat'=>'path',
             'showScriptName'=>false,
             'rules'=>array(
                 ''=> 'site/index',
                 'oauth2callback' => 'site/oauth2callback',
                 '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                 '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                 '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
             ),
         ),

         /*	'db'=>array(
                 'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
             ), */
         // uncomment the following to use a MySQL database

        'db'=>$db_conf,

         'errorHandler'=>array(
             // use 'site/error' action to display errors
             'errorAction'=>'site/error',
         ),

         'mail' => array(
             'class' => 'application.components.yii-mail.YiiMail',
             'transportType' => 'smtp',
             'transportOptions' => array(
                 'port' => '587',
                 'host' => '216.248.198.48', // easy work order mail
                 'username' => 'noreply@easyworkorder.com',
                 'password' => 'noreply',
                // 'encryption'=>'ssl',
             ),
             'viewPath' => 'application.views.mail',
             'logging' => true,
             'dryRun' => false
         ),

         'log'=>array(
             'class'=>'CLogRouter',
             'routes'=>array(
                 array(
                     'class'=>'CFileLogRoute',
                     'levels'=>'warning, error, info',
                     'logFile'=> 'account.log',
                     'categories'=>'account.*',
                 ),
                 array(
                     'class'=>'CFileLogRoute',
                     'levels'=>'info',
                     'logFile'=> 'info.log'
                 ),
                 array(
                     'class'=>'CFileLogRoute',
                     'levels'=>'warning',
                     'logFile'=> 'warnings.log'
                 ),
                 array(
                     'class'=>'CFileLogRoute',
                     'levels'=>'error',
                     'logFile'=> 'errors.log'
                 ),
                 array(
                     'class'=>'CFileLogRoute',
                     'levels'=>'warning, error, info',
                     'logFile'=> 'signup.log',
                     'categories'=>'account.signup.*',
                 ),

                 // uncomment the following to show log messages on web pages
                 /*
                 array(
                     'class'=>'CWebLogRoute',
                 ),
                 */
             ),
         ),
     ),

     // application-level parameters that can be accessed
     // using Yii::app()->params['paramName']
     'params'=>array(
         // this is used in contact page
         'adminEmail'=>'no-reply@fenetex.com',
         'timezone' =>'America/New_York',
     ),
);
