<?php

class SiteController extends CController
{

    public function actionIndex()
    {
        $records = null;


        if(isset($_POST['search'])) {

            $searchString = $_POST['search'];

            if (empty($searchString)) {
                $this->render('index',
                    array(
                        "/site/index",
                        'error'=>"Please Enter a valid Search String",
                    )
                );
                exit;
            }
            $records=Article::model()->findAllArticlesContaining($searchString);
        }

        if(count($records) > 0 && isset($_POST['lucky'])) {
            $article = $records[0];
            $this->redirect(array("/article/view", "id" => $article->id));
        }

        $this->render('index',array(
            'records'=>$records,
        ));

    }


    public function actionError()
    {
        $error=Yii::app()->errorHandler->error;

        $this->render('error', $error);
    }

    public function actionMailtest() {
        $subject = "Testing the mailer";
        $content = "This is a test. Do not leave the building !";

        SendMailMessage::sendEmail(Yii::app()->user->id, $content, $subject);
    }


    public function actionOauth2callback() {
//        return;

        echo '{"web":{"auth_uri":"https://accounts.google.com/o/oauth2/auth","client_secret":"F0E-wUytLihqPeKIeDOFksy0","token_uri":"https://accounts.google.com/o/oauth2/token","client_email":"586279902204-hqm38k4af5d44btd6qqd0gkuh64cauk8@developer.gserviceaccount.com","redirect_uris":["http://www.ndspilot.com/oauth2callback"],"client_x509_cert_url":"https://www.googleapis.com/robot/v1/metadata/x509/586279902204-hqm38k4af5d44btd6qqd0gkuh64cauk8@developer.gserviceaccount.com","client_id":"586279902204-hqm38k4af5d44btd6qqd0gkuh64cauk8.apps.googleusercontent.com","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","javascript_origins":["http://www.ndspilot.com"]}}';
    }
}