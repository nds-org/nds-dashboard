<?php

class MemberController extends Controller
{

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            array('application.components.HttpsFilter', 'https'=>true),
        );
    }


    /**
     * Returns the list of components (i.e. actions) within this controller than can be exposed to the
     * authentication subsystem
     *
     * @return mixed
     */
    public function getComponentActions()
    {
        return array('view', 'admin', 'create', 'error', 'update',
            'delete', 'profile', 'dashboard', 'finder', 'calendar',
            'finderConnector', 'preview', 'profilepic', 'upload', 'mypicture',
            'getpicture', 'editor', 'date', 'numpubs', 'numcitations',
            'numdata', 'pubs', 'citations', 'data', 'calendarfirst', 'calendarnext');
    }


    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('login', 'logout', 'freetrial', 'support', 'error', 'signup'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>$this->getComponentActions(),
                'roles'=>array('admin', 'siteadmin', 'office', 'dealer'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>$this->getComponentActions(),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $member = $this->loadModel($id);

        $this->render('view',array(
            'model'=>$member,
        ));

    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new Member('registration');


        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Member']))
        {
            $model->attributes=$_POST['Member'];

            //print_r($_POST);
            // exit;

            if (isset($_POST['Member']['password']))
                $model->password = $_POST['Member']['password'];


            if ($model->validate()) {
                if($model->save())
                    $this->redirect(array('view','id'=>$model->member_id));
            }
        }

        $this->render('create',array(
            'model'=>$model
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Member']))
        {
//            print_r($_POST);
            //          exit();

            $this->saveMember($model, null);
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }


    public function actionLock() {
        $this->render('lock');
    }

    private function saveMember(Member $model) {
        $model->attributes=$_POST['Member'];

        //print_r($_POST);
        // exit;

        if (isset($_POST['Member']['password']))
            $model->password = $_POST['Member']['password'];


        if ($model->validate()) {
            if(!$model->save())
                throw new CHttpException("Error in saving member: ".print_r($model->getErrors(), true));
        }

        $this->redirect(array('view','id'=>$model->member_id));

    }



    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {

        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));

    }



    public function actionAdmin() {

        $model=new Member('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Member']))
            $model->attributes=$_GET['Member'];


        $records=Member::model()->findAll();
        $this->render('admin',array(
            'records'=>$records,
            'model' => $model,
        ));

    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     * @return Member
     */
    public function loadModel($id)
    {
        $model=Member::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model=new LoginForm;

        // if it is ajax validation request
        if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        if(isset($_GET['CompanyURL'])) {
            $companyURL = $_GET['CompanyURL'];
        }

        // collect user input data
        if(isset($_POST['LoginForm']))
        {
            $model->attributes=$_POST['LoginForm'];

            // validate user input and redirect to the previous page if valid
            if($model->validate() && $model->login()) {

                $role = Yii::app()->user->getState('userrole');
                $redirect = Yii::app()->user->returnUrl;

                if ($redirect === Pages::baseURL())
                    $redirect = array('/member/dashboard');
                else if (StringUtilities::endsWith($redirect, "/index.php"))
                    $redirect = array('/member/dashboard');

                $this->redirect($redirect);
            }
        }
        // display the login form
        $this->render('login',array('model'=>$model));
    }


    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        $assigned_roles = Yii::app()->authManager->getRoles(Yii::app()->user->id); //obtains all assigned roles for this user id
        if(!empty($assigned_roles)) //checks that there are assigned roles
        {
            $auth=Yii::app()->authManager; //initializes the authManager
            foreach($assigned_roles as $n=>$role)
            {
                if($auth->revoke($n,Yii::app()->user->id)) //remove each assigned role for this user
                    Yii::app()->authManager->save(); //again always save the result
            }
        }

        Yii::app()->user->logout();

                // clear and destroy session information
        Yii::app()->session->clear();
        Yii::app()->session->destroy();

        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionDashboard() {
        $member = $this->getMember();

        $this->render('dashboard',array(
            'member' => $member
        ));
    }



    public function actionProfile() {
        $member = Member::model()->getByMemberID(Yii::app()->user->id);
        $memberpref = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

        if (!empty($memberpref))
            $model =$memberpref;
        else
            $model=new MemberPreferences();

        if(isset($_POST['Member']))
        {
            $member->attributes=$_POST['Member'];

            if (isset($_POST['Member']['password'])) {
                $bcrypt = new Bcrypt();
                $member->password = $bcrypt->hash($_POST['Member']['password']);
            }

            if (!$member->save())
                new CHttpException("Opps, something went wrong ! Please try again");

            $member = Member::model()->getByMemberID(Yii::app()->user->id);
        }

        if(isset($_POST['MemberPreferences']))
        {
            $memberpref = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

            if (!empty($memberpref))
                $model=$memberpref;

            $model->attributes=$_POST['MemberPreferences'];

            $model->member_id=$member->member_id;
            if (!$model->save())
                new CHttpException("Opps, something went wrong ! Please try again");
        }

        $this->render('profile',array(
            'member' => $member,
            'preferences' => $model
        ));
    }

    public function actionProfilepic() {

        $member = Member::model()->getByMemberID(Yii::app()->user->id);
        $this->render('profilepic',array(
            'member' => $member,
        ));
    }

    public function actionMypicture() {

        $member = Member::model()->getByMemberID(Yii::app()->user->id);

        $pictureprofile = $member->getProfilePhotoFilePath();

        $img=getimagesize($pictureprofile);

        header('Content-Type: '.$img['mime']);

        readfile($pictureprofile);
    }

    public function actionPreview() {
        $directory = $_GET['directory'];
        $filename =  $_GET['filename'];

        $content = $directory.$filename;

        $img=getimagesize($content);

        header('Content-Type: '.$img['mime']);

        readfile($content);

    }

    public function actionUpload() {

        //      print_r($_FILES);

        //    exit;

        $allowedExts = array("gif", "jpeg", "jpg", "png", "pjpeg", "png");

        $name = $_FILES['filename']['name'];
        $type = $_FILES['filename']['type'];
        $tmpfile = $_FILES['filename']['tmp_name'];
        $error= $_FILES['filename']['error'];
        $size= $_FILES['filename']['size'];

        $extension= StringUtilities::substringAfterLast($name, ".");
        $extension = strtolower($extension);
        if ( in_array($extension,$allowedExts) )
        {
            if ($error > 0)
            {
                throw new CHttpException("Sorry, error in upload: " . MemberController::codeToMessage($error));
            }
            else
            {
                move_uploaded_file($tmpfile,
                    DataDirectories::getCurrentMemberProfilePictureDirectory(). $name);

                $member = Member::model()->getByMemberID(Yii::app()->user->id);

                $member->profile_picture_loc = $name;

                $member->save();
            }
        }
        else
        {
            throw new CHttpException("Sorry, error in upload, Invalid file type with extension: ". $extension);
        }
        $this->actionDashboard();
    }

    public static function codeToMessage($code)
    {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = "The file is too large";
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = "The uploaded file was only partially uploaded";
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = "No file was uploaded";
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = "Missing a temporary folder";
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = "Failed to write file to disk";
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = "File upload stopped by extension";
                break;

            default:
                $message = "Unknown upload error";
                break;
        }
        return $message;
    }

    public function actionNumpubs() {
        $isGuest = StateVariables::isGuest();

        if ($isGuest) {
            $num_pubs = 0;
        } else {
            $articles = Article::model()->getArticlesFor($this->getMemberID());
            $num_pubs = count($articles);
        }
        echo $num_pubs;
    }

    public function actionNumcitations() {
        $isGuest = StateVariables::isGuest();

        if ($isGuest) {
            $num_citations = 0;
        } else {
            $num_citations =  78;

        }
        echo $num_citations;
    }

    public function actionNumdata() {
        $isGuest = StateVariables::isGuest();

        if ($isGuest) {
            $num_data_items = 0;
        } else {
            $resources = Article::model()->getResourcesForAllMemberArticles($this->getMemberID());
            $num_data_items = count($resources);

        }
        echo $num_data_items;
    }

    public function actionPubs() {


        $jsonArray = array();

        $articles = Article::model()->getArticlesFor($this->getMemberID());

        $i=0;

        foreach ($articles as $article) {

            /** @var  $article Article*/

            $this->cleanUpDeletedContent($article->id);

            $subject = $article->title;

            $content = TextFormat::trimText($article->abstract, 80);
            $member = Member::model()->getCurrentMember();

            $picurl = $member->getPictureURL();
            $sender_member_name = $member->getFullName();

            $message_url = Pages::baseURL().'article/'.$article->id;

            $items = array($subject, $content, $sender_member_name, $picurl, $message_url, DateFormat::dateFormat($article->date_added));

            $jsonArray[] = $items;
            ++$i;

            if ($i>10)
                break;
        }

        echo json_encode($jsonArray);
    }


    /**
     * Removes all Resource objects relating to files that may have been deleted by the user. This
     * is run within the ajax thread so it refreshes often in case a file is removed during the
     * "files" article action
     *
     * @param $article_id
     */
    public function cleanUpDeletedContent($article_id) {
        $directory = DataDirectories::getUserArticleDir($article_id);

        $dataItemsObj = new DataSearch($directory);
        $dataItemsObj->setIsInternal(true);
        $data_items = $dataItemsObj->getImages();

        $resources = Resource::model()->getAllByArticleID($article_id);

        foreach ($resources as $resource) {
            /** @var $resource Resource */

            if ($resource->isALocalFile()) {
                $image_loc = $resource->getResourceLocationFile();

                //file_put_contents("/tmp/debug.txt", $image_loc);

                if (!file_exists($image_loc)) {
                    $resource->delete();
                }
            }
        }

    }

    public function actionCitations() {


        $jsonArray = array();

        $articles = Article::model()->getArticlesFor($this->getMemberID());

        $i=0;

        foreach ($articles as $article) {

            /** @var  $article Article*/

            $subject = $article->title;

            $content = TextFormat::trimText($article->abstract, 80);
            $sender_id = Yii::app()->user->id;

            $message_url = Pages::baseURL().'article/'.$article->id;


            $items = array($subject, $content, $message_url);

            $jsonArray[] = $items;
            ++$i;

            if ($i>10)
                break;
        }

        echo json_encode($jsonArray);
    }


    public function actionData() {

        $resources = Article::model()->getResourcesForAllMemberArticles($this->getMemberID());
        $jsonArray = array();

        $i=0;

        foreach ($resources as $resource) {

            /** @var  $resource Resource */
            $subject = $resource->title;

            $resource->date_added;

            $description = TextFormat::trimText($resource->description, 80);
            $sender_member = Member::model()->getCurrentMember();

            $sender_member_name=$sender_member->getFullName();

            $data_image_url = $resource->getResourceContentURL();

            $resource_url = Pages::baseURL().'resource/'.$resource->id;

            $items = array($subject, $description, $sender_member_name, $data_image_url, $resource_url, DateFormat::dateFormat($resource->date_added));

            $jsonArray[] = $items;
            ++$i;

            if ($i>10)
                break;
        }

        echo json_encode($jsonArray);
    }


    public function actionCalendarfirst() {
        $upcoming = new Upcoming();
        echo $upcoming->getFirstItem();

        echo "";

    }

    public function actionCalendarnext() {
        $upcoming = new Upcoming();
        echo $upcoming->getNextEvents();

        echo "";
    }

    public function actionGetpicture($id, $pass=null) {

        //      if ($pass !== StateVariables::getSecurityKey()) {
        //           echo "State var...".StateVariables::getSecurityKey()."<br>";
        //         echo "Pass var...".$pass."<br>";
        //      echo "Sorry, you need access to this service...<br>";
        //        exit;
        //  }

        $member = Member::model()->getByMemberID($id);

        $pictureprofile = $member->getProfileThumbnail();


        $img=getimagesize($pictureprofile);

        header('Content-Type: '.$img['mime']);
        //        header("Content-Length: " . $img['size']);


        $fp = fopen($pictureprofile, 'rb');
        fpassthru($fp);

        exit;

    }


    /**
      * Displays the signup page
      */
     public function actionSignup()
     {
         $model = new SignupForm;

         // if it is ajax validation request
         if( isset( $_POST[ 'ajax' ] ) && $_POST[ 'ajax' ] === 'signup-form' )
         {
             echo CActiveForm::validate( $model );
             Yii::app()->end();
         }

 		/* ???
         if(isset($_GET['CompanyURL'])) {
             $companyURL = $_GET['CompanyURL'];
         }
 		*/

 		// collect user input data
         if( isset( $_POST[ 'SignupForm' ] ) )
         {
             $model->attributes = $_POST[ 'SignupForm' ];

             // validate user input and redirect to the previous page if valid
 			if( $model->validate() && $model->signup() )
 			{
 				// Redirect to previous page.
 				$role = Yii::app()->user->getState('userrole');
 				$redirect = Yii::app()->user->returnUrl;
 				if( $redirect === Pages::baseURL() )
 				{
 					$redirect = array('/member/dashboard');
 				}
 				else if( StringUtilities::endsWith( $redirect, "/index.php" ) )
 				{
 					$redirect = array('/member/dashboard');
 				}
 				$this->redirect( $redirect );
 			}
         }

         // display the Signup form
         $this->render('signup',array('model'=>$model));
     }

}
