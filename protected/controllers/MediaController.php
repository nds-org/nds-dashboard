<?php


class MediaController extends Controller
{

    protected $THUMBNAIL_EXTENSION = ".thmb";

    /**
     * Returns the list of components (i.e. actions) within this controller than can be exposed to the
     * authentication subsystem
     *
     * @return mixed
     */
    public function getComponentActions()
    {
        return array('uploader', 'upload', 'preview', 'filelist', 'videos', 'browse', 'album',
                        'thumbnail', 'delete', 'finder', 'download', 'videolist', 'share', '');
    }


    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>$this->getComponentActions(),
                'users'=>array('*'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>$this->getComponentActions(),
                'users'=>array('admin', 'siteadmin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    // helper functions

    public function actionUpload($location) {

        $location = urldecode($location);

        if (!empty($_FILES)) {
            $notAllowedExts = array("exe");

            $name = $_FILES['file']['name'];
            $type = $_FILES['file']['type'];
            $tmpfile = $_FILES['file']['tmp_name'];
            $error= $_FILES['file']['error'];
            $size= $_FILES['file']['size'];

            $extension= StringUtilities::substringAfterLast($name, ".");
            $extension = strtolower($extension);
            if ( !in_array($extension, $notAllowedExts) )
            {
                if ($error > 0)
                {
                    throw new CHttpException("Sorry, error in upload: " . MemberController::codeToMessage($error));
                }
                else
                {
                    if (move_uploaded_file($tmpfile, $location.$name)) {
                        $this->generateThumbnail($location.$name);
                    }
                }
            }
            else
            {
                throw new CHttpException("Sorry, error in upload, Invalid file type with extension: ". $extension);
            }
        }
    }


    /**
     * Lists the files in the directory
     *
     * @param $location
     */
    public function actionFilelist($location) {
        $result  = array();

        $files = scandir(urldecode($location));

        if ( false!==$files ) {
            foreach ( $files as $file ) {
                if (!is_dir($location.$file) && !StringUtilities::startsWith($file, ".") && (!StringUtilities::endsWith($file, $this->THUMBNAIL_EXTENSION))) {
                    $obj['name'] = $file;
                    $obj['size'] = filesize($location.$file);
                    $result[] = $obj;
                }
            }
        }

        header('Content-type: text/json');
        header('Content-type: application/json');
        echo json_encode($result);
    }

    public function actionVideolist($location) {

        $videos = new VideoSearch(urldecode($location));
        $imagesBase = Pages::baseURL(). "images/";

        $videoList = "[
        ";
        $video_files = $videos->getVideos();

        if (!empty($video_files)) {
            foreach ($video_files as $video_name => $video_url) {

                $videoList .= ' {
                                 title:"'.VideoSearch::getTitleFrom($video_name).'",
                                 artist:"EasyWorkOrder.com",
                                 '.VideoSearch::getJPlayerTypeForFile($video_url).': "'.$video_url.'",
                                 poster: "'.$imagesBase.'logo-high-res-square.png"
                            },';
                }
        }

            $videoList = substr($videoList, 0, strlen($videoList)-1);

            $videoList .= "]";

        echo $videoList;
    }

    public function actionDelete() {

        if(Yii::app()->request->isPostRequest) {
            if (isset($_GET['directory']))
                $directory = urldecode($_GET['directory']);
            else
                throw new CHttpException("Directory Not set!!");

            if (isset($_GET['filename']))
                $filename =  urldecode($_GET['filename']);
            else
                throw new CHttpException("Filename not set !!");

            $content = $directory.$filename;

            if (file_exists($content))
                unlink($content);
            if (file_exists($content.$this->THUMBNAIL_EXTENSION))
                unlink($content.$this->THUMBNAIL_EXTENSION);
        } else {
            throw new CHttpException("Sorry, you cannot access this method this way");
        }

    }

    public function actionPreview() {
        $directory = urldecode($_GET['directory']);
        $filename =  urldecode($_GET['filename']);

         $content = $directory.$filename;

         $img=getimagesize($content);

         header('Content-Type: '.$img['mime']);

         readfile($content);

     }



    public function actionDownload() {

        $directory = urldecode($_GET['directory']);
        $filename =  urldecode($_GET['filename']);

        $image = $directory.urldecode($filename);

        $img=getimagesize($image);

        if ((StringUtilities::endsWith($filename, ".doc")) || (StringUtilities::endsWith($filename, ".docx"))) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
        }

        else if ((StringUtilities::endsWith($filename, ".ppt")) || (StringUtilities::endsWith($filename, ".pptx"))) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
            header('Content-Disposition: attachment;filename="' . $filename . '"');

        }
        else if ((StringUtilities::endsWith($filename, ".xls")) || (StringUtilities::endsWith($filename, ".xlsx"))) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $filename . '"');
        } else {
            header('Content-Type: '.$img['mime']);
            header('Content-Disposition: attachment;filename="' . $filename . '"');

        }

        ob_clean();
        flush();
        readfile($image);
        exit;
    }

    public function actionThumbnail() {
        $directory = urldecode($_GET['directory']);
        $filename =  urldecode($_GET['filename']);

        if ($this->isAMovie($filename)) {
            // a placeholder thumbnail
            $filename = Pages::getPublicBaseDir().'/images/movie.png';
            $directory="";
            $content = $directory.$filename;
        } else if ( StringUtilities::endsWith($filename, ".h5")) {
            $filename = Pages::getPublicBaseDir().'/images/hdf.jpg';
            $directory="";
            $content = $directory.$filename;
        } else {

            $thirdPartyIcon = ImageIcons::getIconPathFor($filename);

            if ($thirdPartyIcon == $filename) { // no special icon so generate our own.
                $thumb_content = $directory.$filename.$this->THUMBNAIL_EXTENSION;

                if (file_exists($directory.$filename)) {
                    $this->generateThumbnail($directory.$filename);
                } else {
                    $thumb_content = Pages::getPublicBaseDir()."/images/icons/questionmark.png";
                }
                $content= $thumb_content;
            } else {
                $content = $thirdPartyIcon;
            }
        }

        $img=getimagesize($content);

        header('Content-Type: '.$img['mime']);

        readfile($content);

    }
    private function isAMovie($filename) {
        $lowerCaseFile = strtolower($filename);
        return (
            StringUtilities::endsWith($lowerCaseFile, ".mp4") ||
            StringUtilities::endsWith($lowerCaseFile, ".mov") ||
            StringUtilities::endsWith($lowerCaseFile, ".mov")
        );
    }

    private function generateThumbnail($directoryAndFile) {
        if ($this->isAMovie($directoryAndFile))
            return;

//        if (ImageIcons::getIconPathFor($directoryAndFile) !== $directoryAndFile)
  //          return; // do not try and resize an object that we have to generate an icon for !!

        $image = new ThumbnailGenerator();
        $image->load($directoryAndFile);
        $image->resize(300,300);

        $image->save($directoryAndFile.$this->THUMBNAIL_EXTENSION);
    }

    /**
     * Uses the video ffmpeg tool to generate a thumbnail for the video
     * @param $directoryAndFile
     */
    private function generateMovieThumbnail($directoryAndFile) {
        $ffmpeg = 'encoder\ffmpeg'; //put the relative path to the ffmpeg.exe file
        $second = 15; //specify the time to get the screen shot at (can easily be randomly generated)
        $image = $directoryAndFile.'.thmb'; //define the output file
        //finally assemble the command and execute it
        $command = "$ffmpeg  -itsoffset -$second  -i $directoryAndFile -vcodec mjpeg -vframes 1 -an -f rawvideo -s 150×84 $image";
        exec($command);
    }

    // FINDER Stuff here


    public function actions()
     {
         return array(
             'finderConnector' => "ext.ezzeelfinder.ElFinderConnectorAction",
         );
     }

}