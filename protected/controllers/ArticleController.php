<?php

class ArticleController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
      //      'postOnly + delete', // we only allow deletion via POST request
             array('application.components.HttpsFilter', 'https'=>true),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('view'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('overview','update', 'list', 'admin','delete', 'files', 'filelist'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete'),
//				'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model=Article::model()->getByArticleID($id);

        $this->render('view',array(
            'model'=>$model,
        ));
    }


    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionOverview($article_id=NULL)
    {

        if (isset($article_id)) {
            $model=Article::model()->getByArticleID($article_id);
        } else {
            $model=new Article;
        }
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Article']))
        {
            $model->attributes=$_POST['Article'];
            if(!$model->save()) {
                echo print_r($model->getErrors());
                exit;
            }

            $map = ArticleMap::model()->getArticleMapFor($this->getMemberID(), $model->id);

            if (empty($map)) {
                $map = new ArticleMap();
                $map->member_id = $this->getMemberID();
                $map->article_id = $model->id;
                $map->save();
            }

            $this->redirect(array('files','article_id'=>$model->id));
        }

        $this->render('overview',array(
            'model'=>$model,
        ));
    }

    public function actionFiles($article_id=NULL) {
        $model=Article::model()->getByArticleID($article_id);

        $directory = DataDirectories::getUserArticleDir($article_id);

        $this->render('files',array(
            'directory'=>$directory,
            'name' => "Home Directory",
            "article_id" => $article_id
        ));

    }

    public function actionContent($article_id=NULL) {

        // scans the files and makes sure we have a resource for each data item.

        $directory = DataDirectories::getUserArticleDir($article_id);

        $dataItemsObj = new DataSearch($directory);
        $dataItemsObj->setIsInternal(true);
        $data_items = $dataItemsObj->getImages();

        if (!empty($data_items)) {
            foreach ($data_items as $file_loc => $image) {
                $resource = Resource::model()->getByResourceLocation($file_loc);
                if (empty($resource)) {
                    $resource = new Resource();
                    $resource->article_id = $article_id;
                    $resource->location = $file_loc;
                    $resource->save();
                }
            }
        }

        $allURLs = Resource::model()->getAllDataResourcesFor($article_id);

        foreach ($allURLs as $url_resource) {
            $data_items[$url_resource->location] = $url_resource->location;
        }

        if(isset($_POST['Resource'])) {

          //   print_r($_POST);
        //    exit;

             $resource_ids = $_POST['Resource'];

             foreach ($resource_ids as $resource_id => $resource_attributes) {
              //   print "TOP LEVEL id = ".$screen_id.'<br>';
                // print_r($screen_attributes);
                // print '<br>';
                 //exit;
                 $resource = Resource::model()->getByResourceID(intval($resource_id));

                 if (!isset($resource)) {
                     throw new CDbException("Could not find resource number ". $resource_id);
                 }

                 $resource->description = $resource_attributes['description'];
                 $resource->content_type_id = $resource_attributes['content_type_id'];
                 $resource->title = $resource_attributes['title'];
                 $resource->save();
             }

             $this->render('view',array(
                 "model" => Article::model()->getByArticleID($article_id)
             ));
             exit;

         }

         $this->render('content',array(
             "article_id" => $article_id,
             "data_items" => $data_items
         ));

      }


 	/**
 	 * Updates a particular model.
 	 * If update is successful, the browser will be redirected to the 'view' page.
 	 * @param integer $id the ID of the model to be updated
 	 */
 	public function actionUpdate($id)
 	{
         $this->actionOverview($id);
 	}

    public function actionDeletedir() {

        if(Yii::app()->request->isPostRequest) {
            if (isset($_GET['directory']))
                $directory = $_GET['directory'];
            else
                throw new CHttpException("Directory Not set!!");

            if (isset($_GET['filename']))
                $filename =  $_GET['filename'];
            else
                throw new CHttpException("Filename not set !!");

            $content = $directory.$filename;

            if (file_exists($content))
                unlink($content);
            if (file_exists($content.$this->THUMBNAIL_EXTENSION))
                unlink($content.$this->THUMBNAIL_EXTENSION);
        } else {
            throw new CHttpException("Sorry, you cannot access this method this way");
        }

    }


    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($article_id)
    {

        $allResources = Resource::model()->getAllDataResourcesFor($article_id);

        foreach ($allResources as $resource) {
            $resource->delete();
        }

        $member = Member::model()->getCurrentMember();

        $map = ArticleMap::model()->getArticleMapFor($member->member_id, $article_id);

        $map->delete();

        $this->loadModel($article_id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Article');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    public function actionAdmin() {
        $this->actionList();
    }

    /**
     * Manages all models.
     */
    public function actionList()
    {
        $model=new Article('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Article']))
            $model->attributes=$_GET['Article'];

        $records=Article::model()->findAll();

        $this->render('admin',array(
            'model'=>$model,
            'records'=>$records,
        ));

    }

    public function actionFilelist($protocol) {
        $this->layout = NULL;

        $protocolImpl = UrlFactory::getUrlHandlerForProtocol($protocol);

        if (!empty($protocolImpl))
            echo $protocolImpl->browse();
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Article the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Article::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Article $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='article-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
