<?php

class ResourceController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
      //      'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view', 'url'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update', 'admin', 'delete', 'run', 'start', 'status', 'terminal', 'reset', 'download', 'listdirs', 'addurl', 'store', 'containers'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionRun($resource_id)
    {
        $this->render('run',array(
            'model'=>$this->loadModel($resource_id),
        ));
    }


    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate($article_id)
    {
        $model=new Resource;
        $model->article_id = $article_id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Resource']))
        {
            $model->attributes=$_POST['Resource'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('create',array(
            'model'=>$model,
            'article_id' => $article_id
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionAddurl($article_id, $url, $name)
    {
        $model=new Resource;
        $model->article_id = $article_id;

        $eds = new EpiphyteDataService();
        $epiphyte_url = $eds->store($url);

        $model->location = $epiphyte_url;
        $model->title = (empty($name) ? $url : $name);
        $model->content_type_id = ContentType::$URL;
        $model->description = "Source URL is: ".$url;

        if($model->save())
            echo "Saved ".$url." as a resource URL";
        else
            echo "ERROR! Could not save the URL ".$url;
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Resource']))
        {
            $model->attributes=$_POST['Resource'];

            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($resource_id)
    {
        $this->loadModel($resource_id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('Resource');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new Resource('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Resource']))
            $model->attributes=$_GET['Resource'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Resource the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=Resource::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Resource $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='resource-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }


    // Execution stuff

    public function actionStart($resource_id, $data="", $container="") {
        $this->layout = NULL;

        $resource = Resource::model()->getByResourceID($resource_id);

        if (empty($container))
            $container = "keyz182/test_container";

        $script_url = $resource->getDownloadURL();

        $boatload = new BoatloadAPI(BoatloadAPI::$CARDIFF);

        $return = $boatload->createContainerAndExecute(urldecode($data), $script_url, $container);

        // echo "Return from POST is ".$return;

        //  exit;

        $jarr =json_decode($return);

        Yii::log($return,'info','resource.controller');

        if (!isset($jarr->{'container_id'})) {
            echo "Request Failed! Could not retrieve a job ID. Please check that the backend is running or contract NDS Support.";
            exit;
        }
        $guid = $jarr->{'container_id'};

//        $tmp = explode(" ", $return);
        //      $job_id = explode(",", $tmp[1]);


        RunScript::model()->deleteAllByResourceID($resource_id);

        $job = new RunScript();
        $job->resource_id=$resource_id;
        $job->run_id = $guid;
        $job->guid = $guid;
        $job->save();

        echo $guid;
    }


    public function actionReset($resource_id) {
        $this->layout = NULL;

        $boatload = new BoatloadAPI(BoatloadAPI::$CARDIFF);

        $boatload->reset($resource_id);

       // RunScript::model()->deleteAllByResourceID($resource_id);
    }

    public function actionStatus($resource_id) {
        $this->layout = NULL;

        $boatload = new BoatloadAPI(BoatloadAPI::$CARDIFF);

        echo $boatload->status($resource_id);
    }


    public function actionTerminal($resource_id) {
        $boatload = new BoatloadAPI(BoatloadAPI::$CARDIFF);

        echo $boatload->stdout($resource_id);
    }


    public function actionDownload($resource_id) {
        $this->layout = NULL;

        $runScript = RunScript::model()->getByResourceID($resource_id);

        if (empty($runScript)) {
            echo "ERROR";
            exit;
        } else {
            $boatload = new BoatloadAPI(BoatloadAPI::$CARDIFF);

            $file = $boatload->download($resource_id);

            header("Cache-Control: no-cache private");
            header("Content-Description: File Transfer");
            header('Content-Type: application/download');
            header("Content-Transfer-Encoding: binary");
            header('Content-Length: '.strlen($file));
            header('Content-Disposition: attachment;filename="results.zip"');
            echo $file;
            exit;
        }

    }

    /**
     * Returns the URL of the specified resource id
     *
     * @param $resource_id
     */
    public function actionUrl($resource_id)
    {
        $resource = Resource::model()->getByResourceID($resource_id);

        header("Cache-Control: no-cache private");
        header("Content-Description: File Transfer");
        header('Content-Type: application/download');
        header("Content-Transfer-Encoding: binary");
        header('Content-Disposition: attachment;filename="'.$resource->getFileName().'"');
        readfile($resource->getResourceLocationFile());
        exit;
    }


    public function getModes() {

        $sel = '<select class="run-choice">';
        $sel .= '<option value="execute">Execute Script</option>';
        //  $sel .= '<option value="notebook">Run Notebook</option>';
        $sel .='</select>
        ';
        echo $sel;
    }

    /**
     * Store does nothing as we have no data backend now.
     */
    public function actionStore($url) {
        return $url;
    }

    public function actionListdirs($article_id) {

        $sel = '<select class="dataset">';
        $sel .= '<option value=""> --None-- </option>';

        $urlResources = Resource::model()->getAllDataResourcesFor($article_id);
        foreach ($urlResources as $resource) {
            /** @var $resource Resource */
            $sel .= '<option value="'.$resource->getDownloadURL().'">'.$resource->getTitle().'</option>';
        }

        $sel .='</select>
        ';
        echo $sel;
    }

    public function actionContainers() {

        $sel = '<select class="container">';
        $sel .= '<option value=""> --None-- </option>';

        $result = $this->get("list_images");

        $images = json_decode($result);

        if (isset($images)) {
            foreach ($images as $images_name) {
                $sel .= '<option value="'.$images_name.'">'.$images_name.'</option>';
            }
        }

        $sel .='</select>
          ';
        echo $sel;
    }
}
