<?php

/* @var $step_num int */
/* @var $vertical bool */
/* @var $append_to_title string */

if (isset($append_to_title))
    $title_add = $append_to_title;
else
    $title_add = ""


?>

<div class="ui raised segment">
   <div class="ui huge ribbon label"><?php echo "NDS Wizard ".$title_add ?></div>
    <div class="ui large<?php if ($vertical) echo " vertical" ?> steps">
        <div class="ui huge label-inverse">
            <h3></h3>
        </div>
        <br>
        <br>
       <div class="ui <?php echo ($step_num == 1)? "active" : "disabled" ?> step">
         Article Overview
       </div>

       <div class="ui <?php echo ($step_num == 2)? "active" : "disabled" ?> step">
         Add Files
       </div>
       <div class="ui <?php echo ($step_num == 3)? "active" : "disabled" ?> step">
         Describe Content
       </div>
        <div class="ui <?php echo ($step_num == 4)? "active" : "disabled" ?> step">
          Finish
        </div>


    </div>
</div>