<?php
/* @var $this ArticleController */

$article =  Article::model()->getArticlesFor($this->getMemberID());

$articleView = new AccordionTable("Your Articles", $article, count($article));

?>

<div class="ui inverted teal fluid label">Search Results</div>


    <div class="ui page basic segment">


    <?php
            echo $articleView->getWidget();
    ?>

    </div>

<div class="clearfix"></div>
<br>
