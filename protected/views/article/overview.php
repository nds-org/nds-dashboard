<?php
/* @var $this ArticleController */
/* @var $model Article */

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
        "Your Articles" => "article/list",
        "Article #".$model->id => "article/".$model->id,
        $model->id." Overview" => "")
);


?>

<div class="ui page basic segment">

    <div class="ui blue segment" >
        <div class="ui left corner blue label">
            <i class="white info icon"></i>
        </div>
        <div style="padding-left: 20px;">
            This wizard helps your create an article in three simple steps: article info, add data (or links) and descriptions of the data/links.
        </div>
    </div>

    <?php

        $this->renderPartial('wizardsteps', array(
            'step_num' => 1,
            'vertical' => false
        ));
    ?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>

</div>
