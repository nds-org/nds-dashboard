<?php
/* @var $this ArticleController */
/* @var $model Article */

$resources =  Resource::model()->getAllByArticleID($model->id);

$articleResources = new AccordionTable("Your Data", $resources, count($resources));

function getItem($name, $value) {

    if (empty($value)) return "";

    return '<div class="item">
                <i class="map marker icon"></i>
                <div class="content">
                    <header class="header">'.$name.'</header>
                    <div class="description">'.$value.'</div>
                </div>
            </div>';
}

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
            "Your Articles" => "article/list",
          "View Article ".$model->id => "")
);

?>


<div class="ui page basic segment">

    <div class="ui red stacked segment">
        <div class="ui grid">
            <div class="eight wide column">
                <div class="ui large black header">Article Details</div>
            </div>
            <div class="ui right aligned eight wide column">
                <a target="_blank" href="http://graves.cl/visualRDF/?url=<?php echo Pages::getAbsoluteBaseURL()?>ore/article/<?php echo $model->id ?>/xml"> <span class="ui primary button"><i class="sitemap icon"></i>Visualize ORE Map</span></a>
                <a target="_blank" href="<?php echo StateVariables::getBaseUrl()?>/ore/article/<?php echo $model->id ?>/xml"> <span class="ui primary button"><i class="download icon"></i>Download</span></a>
                <a href="<?php echo StateVariables::getBaseUrl()?>/article/overview/article_id/<?php echo $model->id ?>"> <span class="ui primary button"><i class="edit icon"></i>Edit</span></a>
            </div>
        </div>
    </div>


    <div class="ui divided list">
        <?php echo getItem("Short Name", $model->short_name); ?>
        <?php echo getItem("Article Type", $model->article_type_name); ?>
        <?php echo getItem("Title", $model->title); ?>
        <?php echo getItem("Journal Name", $model->journal_name); ?>
        <?php echo getItem("Journal Details", $model->getCitation()); ?>

        <?php echo getItem("Status", $model->getStatus()); ?>
        <?php echo getItem("Date Added", DateFormat::dateFormat($model->date_added)); ?>
        <?php echo getItem("Abstract", $model->abstract); ?>
        <?php echo getItem("Authors", $model->authors); ?>
        <?php echo getItem("DOI", $model->doi); ?>
        <?php echo getItem("Primary Research Topic", ResearchTopics::model()->getResearchTopicFor($model->primary_research_field)); ?>
        <?php echo getItem("Secondary Research Topic", ResearchTopics::model()->getResearchTopicFor($model->primary_research_field)); ?>
        <?php echo getItem("URL", $model->url); ?>
        <?php echo getItem("Notes", $model->notes); ?>
        <?php echo getItem("Tags", $model->tags); ?>



    </div>

    <div class="ui red stacked segment">
        <div class="ui grid">
            <div class="eight wide column">
                <div class="ui large black header">Data</div>
            </div>
            <div class="ui right aligned eight wide column">
                <a href="<?php echo StateVariables::getBaseUrl()?>/article/files/article_id/<?php echo $model->id ?>"> <span class="ui primary button"><i class="edit icon"></i>Edit</span></a>
            </div>
        </div>
    </div>

    <div class="bump"></div>
    <?php
    echo $articleResources->getWidget();
    ?>

</div>
