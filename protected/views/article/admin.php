<?php
/* @var $this ArticleController */

$article =  Article::model()->getArticlesFor($this->getMemberID());

$articleView = new AccordionTable("Your Articles", $article, count($article));

$member = Member::model()->getByMemberID(Yii::app()->user->id);

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
          "Your Articles" => "")
);

?>

<div class="ui blue stacked segment">
    <div class="ui grid">
        <div class="eight wide column">
            <div class="ui large black header"><?php echo $member->getFullName()?>'s Articles</div>
        </div>
        <div class="ui right aligned eight wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/overview"> <span class="ui primary button"><i class="new file icon"></i>Create Article</span></a>
        </div>
    </div>
</div>

<div class="bump">

</div>

<div class="ui page basic segment">

    <?php
            echo $articleView->getWidget();
    ?>

</div>
