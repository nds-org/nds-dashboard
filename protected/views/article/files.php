
<script src="https://www.google.com/jsapi?key=AIzaSyB8GO_fuy4VRkh1y32XTQjYlfDY0fqHv_o"></script>
<script src="https://apis.google.com/js/client.js"></script>


<?php
/** @var $directory string */
/** @var $name string */
/** @var $header boolean */
/** @var $title boolean */
/** @var $article_id int */

$model = Article::model()->getByArticleID($article_id);

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
            "Your Articles" => "article/list",
        "Article #".$model->id => "article/".$model->id,
        $model->id." Data Files" => "")
);

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScriptFile(Pages::baseURL(). "js/filepicker.js", CClientScript::POS_BEGIN);
$cs->registerScriptFile(Pages::baseURL(). "js/dataapis.js", CClientScript::POS_BEGIN);

$cs->registerScript("external-urls", '

    $( document ).ready(function() {
        initDataAPIs("'.Pages::getAbsoluteBaseURL().'", '.$article_id.');
    });

', CClientScript::POS_BEGIN);

?>

<?php
header_remove("SAMEORIGIN");
?>

<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="0yxeac9va37kq86"></script>


<div class="ui page basic segment">
    <div class="ui grid">
        <div class="ui four wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/overview/article_id/<?php echo $article_id ?>"> <span class="ui button"><i class="left arrow icon"></i>Article Overview</span></a>
        </div>
        <div class="ui eight wide column">
            <div class="ui blue segment" >
                <div class="ui left corner blue label">
                    <i class="white info icon"></i>
          	    </div>
                <div style="padding-left: 20px;">
                    For files accessed through external protocols, browse or enter URL below. For smaller files,
                    simply drop them into the browser as indicated (or click space to upload).
                </div>
            </div>
        </div>
        <div class="ui right aligned four wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/content/article_id/<?php echo $article_id ?>"> <span class="ui button"><i class="right arrow icon"></i>Describe Content</span></a>
        </div>
    </div>

        <?php
        $this->renderPartial('wizardsteps', array(
            'step_num' => 2,
            'vertical' => false
        ));
        ?>

    <div class="ui purple segment" >
        <div class="ui left corner purple label">
            <i class="white plus icon"></i>
        </div>

        <div class="ui form segment">
            <div class="header">Enter URL</div>
            <div class="ui grid">
                <div class="ui twelve wide column">
                     <div class="field">
                         <div class="ui left labeled icon input">
                            <input placeholder="Enter URL ..." name="url_address" id="url_address" type="text" />
                            <i class="external url icon"></i>
                         </div>
                     </div>
                </div>
                <div class="ui four wide column">
                    <div class="field">
                        <div class="ui large url button">Add URL <i class="external url icon"></i></div>
                    </div>
                </div>
            </div>

            <div class="ui horizontal divider">OR</div>

            <div class="header">Browse Files (Select Protocol)</div>
            <div class="ui grid">
                <div class="ui twelve wide column">
                     <div class="field">
                         <div class="ui input">
                            <?php echo UrlFactory::getProtocolListAsDropdown(); ?>
                         </div>
                     </div>
                </div>
                <div class="ui four wide column">
                    <div class="field">
                        <div class="ui large browse button">Browse <i class="browser icon"></i></div>
                    </div>
                </div>
            </div>

            <div class="ui horizontal divider">OR</div>


            <?php $this->renderPartial('/media/uploader', array(
                    'directory'=>$directory,
                    'name' => $name
                )); ?>

        </div>
    </div>

    <div class="ui grid">
        <div class="ui left aligned eight wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/overview/article_id/<?php echo $article_id ?>"> <span class="ui button"><i class="left arrow icon"></i>Article Overview</span></a>
        </div>
        <div class="ui right aligned eight wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/content/article_id/<?php echo $article_id ?>"> <span class="ui button"><i class="right arrow icon"></i>Describe Content</span></a>
        </div>
    </div>

<br>
    <br>


</div>


<div class="ui info modal">
  <i class="close icon"></i>
  <div class="header">
    <a class="ui red huge label infolog title">
        </a>
  </div>
  <div class="content">
    <a class="ui large purple label">Status For </a> <i class="ui basic segment infolog status" style="text-transform: uppercase;"></i>

      <div class="ui horizontal divider">
        And
      </div>
    <a class="ui large purple label">Content </a> <i class="ui basic segment infolog action" style="text-transform: uppercase;"></i>
  </div>
  <div class="actions">
     <div class="ui negative right labelled icon button cancel-enable">
        Cancel
        <i class="close icon"></i>
      </div>
      <div class="ui positive right labeled icon button">
        Ok
        <i class="checkmark icon"></i>
      </div>
  </div>
</div>


