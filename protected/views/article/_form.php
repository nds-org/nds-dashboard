<?php
/* @var $this ArticleController */
/* @var $model Article */
/* @var $form CActiveForm */


?>

<div class="ui basic segment page">

    <div class="ui error form segment">


        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'article-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
        ));

        // format = name="Article[article_type]" id="Article_article_type"
        ?>

        <div class="field">
            <?php echo CHtml::htmlButton("Next<i class='right arrow icon'></i>", array(
                   'encode' => true,
                   'class' => 'ui button', "id" => "button",
                    'style' => "float: right",
                   'type' => 'submit'
          		    ));?>
            </div>

        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>


        <div class="two fields">
            <div class="field">
                <label>Article Status</label>

                <?php echo SemanticWidgets::getDropDown($model,'status',Article::model()->getStatusTypesAsArray()); ?>
                <?php echo $form->error($model,'status'); ?>
            </div>

            <div class="field">
                <?php
                $topics = ArticleType::model()->getAsArray();
                echo $form->labelEx($model,'article_type'); ?>
                <?php echo SemanticWidgets::getDropDown($model, 'article_type', $topics) ?>
                <?php echo $form->error($model,'article_type'); ?>
            </div>
        </div>


        <div class="field">
            <?php echo $form->labelEx($model,'short_name'); ?>
            <?php echo $form->textField($model,'short_name',array('size'=>60,'maxlength'=>200)); ?>
            <?php echo $form->error($model,'short_name'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'title'); ?>
            <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>1000)); ?>
            <?php echo $form->error($model,'title'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'abstract'); ?>
            <?php echo $form->textArea($model,'abstract',array('size'=>60,'maxlength'=>5000)); ?>
            <?php echo $form->error($model,'abstract'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'authors'); ?>
            <?php echo $form->textArea($model,'authors',array('size'=>60,'maxlength'=>2000)); ?>
            <?php echo $form->error($model,'authors'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'url'); ?>
            <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>200)); ?>
            <?php echo $form->error($model,'url'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'doi'); ?>
            <?php echo $form->textField($model,'doi',array('size'=>50,'maxlength'=>50)); ?>
            <?php echo $form->error($model,'doi'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'journal_name'); ?>
            <?php echo $form->textField($model,'journal_name',array('size'=>60,'maxlength'=>200)); ?>
            <?php echo $form->error($model,'journal_name'); ?>
        </div>

        <div class="two fields">
            <div class="field">
                   <?php echo $form->labelEx($model,'month'); ?>
                   <?php echo $form->textField($model,'month'); ?>
                   <?php echo $form->error($model,'month'); ?>
               </div>

               <div class="field">
                   <?php echo $form->labelEx($model,'year'); ?>
                   <?php echo $form->textField($model,'year'); ?>
                   <?php echo $form->error($model,'year'); ?>
               </div>
        </div>


        <div class="three fields">
            <div class="field">
                       <?php echo $form->labelEx($model,'pages'); ?>
                       <?php echo $form->textField($model,'pages',array('size'=>10,'maxlength'=>10)); ?>
                       <?php echo $form->error($model,'pages'); ?>
                   </div>

                   <div class="field">
                       <?php echo $form->labelEx($model,'volume'); ?>
                       <?php echo $form->textField($model,'volume',array('size'=>10,'maxlength'=>10)); ?>
                       <?php echo $form->error($model,'volume'); ?>
                   </div>

                   <div class="field">
                       <?php echo $form->labelEx($model,'number'); ?>
                       <?php echo $form->textField($model,'number'); ?>
                       <?php echo $form->error($model,'number'); ?>
                   </div>
        </div>


        <div class="two fields">
            <div class="field">
                <?php
                $topics = ResearchTopics::model()->getAsArray();

                echo $form->labelEx($model,'primary_research_field'); ?>
                <?php echo SemanticWidgets::getDropDown($model,'primary_research_field', $topics); ?>
                            <?php echo $form->error($model,'primary_research_field'); ?>
            </div>

            <div class="field">
                <?php



                echo $form->labelEx($model,'secondary_research_field'); ?>
                <?php echo SemanticWidgets::getDropDown($model,'secondary_research_field', $topics); ?>
                <?php echo $form->error($model,'secondary_research_field'); ?>
            </div>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'notes'); ?>
            <?php echo $form->textField($model,'notes',array('size'=>60,'maxlength'=>5000)); ?>
            <?php echo $form->error($model,'notes'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'tags'); ?>
            <?php echo $form->textField($model,'tags',array('size'=>60,'maxlength'=>500)); ?>
            <?php echo $form->error($model,'tags'); ?>
        </div>

        <div class="ui right aligned basic segment">
            <?php echo CHtml::htmlButton("Next<i class='right arrow icon'></i>", array(
                   'encode' => true,
                   'class' => 'ui button', "id" => "button",
                   'type' => 'submit'
          		    ));?>
            </div>
        </div>

    </div>


<?php $this->endWidget(); ?>


<br>
<br>