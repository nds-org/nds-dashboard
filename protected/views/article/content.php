<?php
/** @var $article_id int */
/** @var $data_items array */

/* @var $form CActiveForm */

$model = Article::model()->getByArticleID($article_id);

$form=$this->beginWidget('CActiveForm', array(
    'id'=>'resource-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
));

$resourceWidget = new ResourceFormWidget("Photos", $this, $form, $article_id, $data_items);

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
            "Your Articles" => "article/list",
        "Article #".$model->id => "article/".$model->id,
        $model->id." Description" => "")
);

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScript("content-type-dropdown", "
    $('.ui.dropdown').dropdown();
", CClientScript::POS_END);

?>


<div class="form">

    <div class="ui grid">
        <div class="ui eight wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/files/article_id/<?php echo $article_id ?>"> <span class="ui button"><i class="left arrow icon"></i>Add Files</span></a>
        </div>
        <div class="ui aligned right eight wide column">
            <div style="float:right">
                <?php echo CHtml::htmlButton("Finish<i class='right arrow icon'></i>", array(
                       'encode' => true,
                       'class' => 'ui button', "id" => "button",
                       'type' => 'submit'
              		    ));?>
                </div>
        </div>
    </div>


<div class="ui page basic segment">

        <?php
        $this->renderPartial('wizardsteps', array(
            'step_num' => 3,
            'vertical' => false
        ));
        ?>
</div>

<?php

echo $resourceWidget->getContent();

?>


    <div class="ui grid">
        <div class="ui left aligned eight wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/files/article_id/<?php echo $article_id ?>"> <span class="ui button"><i class="left arrow icon"></i>Add Files</span></a>
        </div>
        <div class="ui right aligned eight wide column">
            <div style="float:right">
                <?php echo CHtml::htmlButton("Finish<i class='right arrow icon'></i>", array(
                       'encode' => true,
                       'class' => 'ui button', "id" => "button",
                       'type' => 'submit'
              		    ));?>
            </div>
        </div>
    </div>



<br>
    <br>


    <?php $this->endWidget(); ?>

  </div><!-- form -->

  <br>
  <br>