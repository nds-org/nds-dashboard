<?php

$siteBaseURL = Pages::baseURL();

$isGuest = StateVariables::isGuest();

 $role = Yii::app()->user->getState('userrole');

 $hasInternalAccess = ($role=="admin") || ($role=="siteadmin") ||
     (($role=="support"));

 $isAdmin = $hasInternalAccess;

 $isproxy=Yii::app()->user->getState("isproxy");

 if ($isGuest) {
     $member=new Member();
 } else if ($hasInternalAccess) {
     //$admin = Admin::model()->getByAdminID(Yii::app()->user->id);
     //$firstName = $admin->first_name;
     //$lastName = $admin->last_name;
     //$member=new Member();
     //$accounthome="/admin/home";
     //$accounthomename="Dashboard...";
 } else {
     $member = Member::model()->getByMemberID(Yii::app()->user->id);
     $firstName = $member->first_name;
     $lastName = $member->last_name;
 }


 if ($isGuest) {
     $accountAccessURL = $siteBaseURL.'/member/login';
     $accountAccessName = "Login";
     $dashboardUrl = $siteBaseURL;
     $dashboardName = "Home";
  } else {
     $accountAccessName = "Logout";
     $accountAccessURL = $siteBaseURL.'/member/logout';
     $dashboardUrl = $siteBaseURL.'member/dashboard';
     $dashboardName = "Dashboard...";
  }

 if ($isAdmin) {
     $accountAccessName = "Logout";
     $accountAccessURL = $siteBaseURL.'/member/logout';
     $dashboardName = "Dashboard...";
     $dashboardUrl = $siteBaseURL.'member/dashboard';
 }
 $accountAccessOption = '<a href="'.$accountAccessURL.'">'.$accountAccessName.' </a>';

?>

<script>


    $( document ).ready(function() {

        var sidebarButton  = $('.fixed.launch.button');

        $('.emaslow.menu .item')
            .tab();


        $('.ui.dropdown')
          .dropdown()
        ;

        $('.left.emaslow.sidebar').first()
          .sidebar('attach events', '.launch.button');

        var width = 600;
        var dropmenu = $('.ui.emaslow.menu');

        if( $(window).width() < width ) {
            dropmenu.width($(window).width());
        }
    })


 </script>


<div class="ui left emaslow white vertical labeled sidebar icon menu">
    <a class="red item" href="<?php echo $dashboardUrl ?>">
        <?php
        if ($isGuest) {
        ?>
          <i class="home icon"></i>
        <?php
        } else {
        ?>
        <i class="dashboard icon"></i>
        <?php
        }
        ?>

      <?php echo $dashboardName ?>
    </a>

    <?php
      if ($isGuest) {
      ?>

       <a class="item" href="<?php echo $siteBaseURL."member/login" ?>">
          <i class="<?php echo "icon ".($isGuest?"user":"power off") ?>"></i>
          <?php echo "login"?>
       </a>
       <a class="item" href="<?php echo $siteBaseURL.'member/signup' ?>">
          <i class="icon add"></i>
          Sign Up
        </a>
        <?php
        }
        ?>

    <?php
       if (!$isGuest) {
       ?>

       <a class="item" href="<?php echo $siteBaseURL.'article/overview' ?>">
         <i class="student icon"></i>
           Create Article
       </a>
       <a class="item" href="<?php echo $siteBaseURL.'article/list' ?>">
       <i class="list icon"></i>
           My Articles
       </a>

       <?php
       }
       ?>


      <?php
      if (!$isGuest) {
      ?>

      <a class="green item" href="<?php echo $siteBaseURL."member/logout" ?>">
          <i class="<?php echo "icon ".($isGuest?"user":"power off") ?>"></i>
          <?php echo "logout" ?>
       </a>
      <?php
      }
      ?>

</div>






