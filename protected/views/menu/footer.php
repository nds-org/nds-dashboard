<?php

?>

<div class="ui masthead footer stackable grid" style="font-weight: 100">
        <div class="ten wide column">
            <h3>About us</h3>

            <p>The National Data Service is community-driven effort, guided by the National Data Service
                Consortium. Membership is open to stakeholding organizations. If you would like more
                information about getting involved, contact <a href="mailto:info@nationaldataservice.org">info@thenationaldataservice.org</a>.</p>

        </div>
    <div class="two wide column">
    </div>
    <div class="four wide column">
            <h3>More Information</h3>
            <p><a href="http://www.nationaldataservice.org" target="_blank"><i class="icon linkify"></i> National Data Service Site</a></p>
    </div>
    <br>
    <div class="sixteen wide column">
        <div class="ui divider"></div>

        <p>Copyright 2015, National Data Service.</p>
        <br>

    </div>
</div>