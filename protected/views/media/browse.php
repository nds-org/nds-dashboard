<?php

/** @var $directory string */
/** @var $name string */
/** @var $this MediaController */



$videos = new VideoSearch($directory);
$videos->setIsInternal(true);

?>

<h3><?php echo $name ?> Media Browser</h3>

<p>You can use these tools to upload and viewing media files (pictures, images, files, movies, etc) for storing
    within your member profile. All uploaded content is private and others will not be able to view
    your files. If you want to upload files for others to see, consider using the building or
    Work Order finders to upload files into common areas.</p>


<div class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab"><h3>Uploader</h3></a></li>
      <li><a href="#tab2" data-toggle="tab"><h3>Finder</h3></a></li>
      <li><a href="#tab3" data-toggle="tab"><h3>Videos</h3></a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab1">
        <?php
        $this->renderPartial('uploader',array(
            'directory'=>$directory,
            'name' => "Home Directory"
        ));
        ?>
        <br>
    </div>
    <div class="tab-pane" id="tab2">
        <?php
        $this->renderPartial('finder',array(
            'directory'=>$directory,
            'name' => "Home Directory"
        ));
        ?>
        <br>
    </div>

      <div class="tab-pane" id="tab3">
          <?php
          $this->renderPartial('videos',array(
              'videos' => $videos,
              'name' => "Home Directory"
          ));
          ?>
          <br>
      </div>

  </div>
</div>



