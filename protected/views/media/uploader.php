<?php
/** @var $directory string */
/** @var $name string */
/** @var $header boolean */
/** @var $title boolean */

if (!isset($title))
    $title=true; // provide a title unless told to turn off

if (!isset($header))
    $header=false;

if ($header) {

}

$dropzoneBase = Pages::baseURL(). "dropzone/";

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerCssFile($dropzoneBase."css/dropzone.css");

$cs->registerScriptFile($dropzoneBase."dropzone.min.js", CClientScript::POS_END );

$preview = Pages::getAbsoluteBaseURL()."media/preview";
$thumbnail = Pages::getAbsoluteBaseURL()."media/thumbnail";

$uploadUrl = Pages::getAbsoluteBaseURL()."media/upload";
$listFileUrl = Pages::getAbsoluteBaseURL()."media/filelist?location=".urlencode($directory);

$deleteFileUrl = Pages::getAbsoluteBaseURL()."media/delete";

$downloadFileUrl = Pages::getAbsoluteBaseURL()."media/download?directory=".urlencode($directory)."&filename=";

$uploadAction = $uploadUrl."?location=".urlencode($directory);

$cs->registerScript("get-existing-files", "

window.scaninterval=2000;
window.data=null;

$(document).on({
    mouseenter: function () {
        $(this).css({'cursor':'pointer'});
    },
    mouseleave: function () {
        $(this).css({'cursor':'auto'});
    }
},'.dz-image-preview');

$(document).on({
    mouseenter: function () {
        if (! $('#click-me').is(':visible'))
            $('#click-me').show();
        },
    mouseleave: function () {
        if ($('#click-me').is(':visible'))
            $('#click-me').hide();
        }
},'.dz-message');




$(document).on('click', '.dz-image-preview', function(){
    fileName = $(this).find('.dz-filename>span').text();

    var url = '".$downloadFileUrl."' + fileName;

    window.open(url, '_blank');
});


var getImages = function() {

    var thisDropzone = Dropzone.forElement('#fileupload');

    $.get('".$listFileUrl."', function(data) {
        if (JSON.stringify(window.data) !== JSON.stringify(data)) {
            $('#dropzonePreview').empty();
            window.data = data;
            $.each(data, function(key,value){
                var mockFile = { name: value.name, size: value.size };

                thisDropzone.options.addedfile.call(thisDropzone, mockFile);

                thisDropzone.options.thumbnail.call(thisDropzone, mockFile, '".$thumbnail."?filename=' + value.name + '&directory=".$directory."');
                });
            }
        });


    timerID = setTimeout(getImages, scaninterval);
   }

function deleteFile(file) {
    var url = '".$deleteFileUrl."?directory=".$directory."&filename=' + file;

    var request = $.ajax({
        url: url,
        type: 'POST',
    });

    request.done(function( msg ) {
        getImages(); // update content.
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( 'Request failed: ' + textStatus );
    });

}

Dropzone.options.fileupload = {
    paramName: 'file', // The name that will be used to transfer the file
    maxFilesize: 2047, // MB
    addRemoveLinks: true,
    previewsContainer: '#dropzonePreview',
    dictDefaultMessage : 'Click/Drag and Drop Files Here',

    accept: function(file, done) {
        if (file.name == 'ian.jpg') {
            done('Naha, you dont.');
        }
        else {
            done();
            }
        },
    removedfile: function(file, serverFileName) {
            deleteFile(file.name);
            },
    canceled: function(file, serverFileName) {
             deleteFile(file.name);
             },
    error: function(file, errorMessage, xhr) {
        $('#myModal').modal('show');
        // errorMessage
        },
    init: function() {
        getImages();
    }
};

");
?>


<div class="row-fluid">

<div class="thumbnail">

<form action="<?php echo $uploadAction?>"
      class="dropzone"
      id="fileupload">

    <div class="fallback">
        <input name="file" type="file" multiple />
      </div>
</form>
    <div class="ui blue large message" id="click-me" style="display: none">Drop a file into this area or click To upload a file!</div>

    <div class="dropzone-previews" id="dropzonePreview">
    </div>

    </div>


</div>
