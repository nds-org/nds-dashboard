<?php

/** @var $member Member */
/** @var $header boolean */
/** @var $directory string */
/** @var $name string */

if (!isset($header))
    $header=false;

if ($header) {
        Breadcrumbs::myBreadcrumbs("Finder For ".$name);
}


$filesUrl = Pages::getAbsoluteBaseURL() . "media/preview?directory=".$directory."&filename=";

//echo "PATH = ".$filesPath."<br>";
//echo "URL = ".$filesUrl."<br>";

?>

<h3>Finder For <?php echo $name ?></h3>

<?php

$prefs = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

if ($prefs)
    $dataformat = $prefs->date_style;
else
    $dataformat =  'j M Y H:i';

$this->widget("ext.ezzeelfinder.ElFinderWidget", array(
    'selector' => "div#file-uploader",
    'clientOptions' => array(
       // 'lang' => "es",
        'resizable' => true,
        'wysiwyg' => "ckeditor",
        'getFileCallback' => "js: function(file) {
                                  alert('Selected file is \"' + file + '\".')
                              }",
        'handlers' => array(
            'init' => "js: function(event, elFinder) {
                      //     alert('ElFinder has been initialized.')
                       }",
            'open' => "js: function(event, elFinder) {
                           var path = event.data.options.path;
                          // alert('Directory \"' + path + '\" is open.')
                       }",
            'save' => "js: function(event, elFinder) {
                           var path = event.data.options.path;
                           alert('File being saved is \"' + path + '\"')
                       }",

//            'upload'    => file_put_contents($filesPath, "Uploading !!!! "

//            'ls'        => array('target' => true, 'mimes' => false),
//            'tree'      => array('target' => true),
//'parents'   => array('target' => true),
//'tmb'       => array('targets' => true),
//'file'      => array('target' => true, 'download' => false),
//'size'      => array('targets' => true),
//'mkdir'     => array('target' => true, 'name' => true),
//'mkfile'    => array('target' => true, 'name' => true, 'mimes' => false),
//'rm'        => array('targets' => true),
//'rename'    => array('target' => true, 'name' => true, 'mimes' => false),
//'duplicate' => array('targets' => true, 'suffix' => false),
//'paste'     => array('dst' => true, 'targets' => true, 'cut' => false, 'mimes' => false),
//'upload'    => array('target' => true, 'FILES' => true, 'mimes' => false, 'html' => false),
//'get'       => array('target' => true),
//'put'       => array('target' => true, 'content' => '', 'mimes' => false),
//'archive'   => array('targets' => true, 'type' => true, 'mimes' => false),
//'extract'   => array('target' => true, 'mimes' => false),
//'search'    => array('q' => true, 'mimes' => false),
//'info'      => array('targets' => true),
//'dim'       => array('target' => true),
//'resize'

        )
    ),
    'connectorRoute' => "member/finderConnector",
    'connectorOptions' => array(
        'roots' => array(
            array(
                'driver'  => "LocalFileSystem",
                'path' => $directory,
                'URL' => $filesUrl,
                'tmbPath' => $directory. DIRECTORY_SEPARATOR . ".thumbs",
                'mimeDetect' => "internal",
                'accessControl' => "access",
                'dateFormat'   => $dataformat,  // file modification date format
                'tmbAtOnce'    => 20,            // number of thumbnails to generate per request
                'tmbCleanProb' => 200,            // how frequiently clean thumbnails dir (0 - never, 200 - every init request)
            )
        )
    )
));

?>

<div id="file-uploader"></div>

