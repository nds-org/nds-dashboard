<?php
/** @var $directory string */
/** @var $name string */
/** @var $this MediaController */

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$yoxBase = Pages::baseURL(). "yoxview/";

//$cs->registerCssFile($yoxBase."yoxview.css");

$cs->registerScriptFile($yoxBase."yoxview-init.js",  CClientScript::POS_END  );

$cs->registerScript("yox", '
$(document).ready(function(){
				$(".yoxview").yoxview({
                    skin: "top_menu",
                    allowInternalLinks: true
				});
			});

');

$images = new DataSearch($directory);
$images->setIsInternal(true);
?>

<style>


body
{
	font-family: Arial, Sans-Serif;
	margin: 0;
}
#container{ width: 700px; margin: 40px auto; }
h1{ font-size: 1.5em; font-weight: bold; }
hr{ clear: both; }

.thumbnails a
{
	display: block;
	float: left;
	margin: 4px;
}
.thumbnails a img{ border: solid 1px Black; }
.thumbnails a:hover img{ border: solid 1px #aaa }

</style>

<h1>YoxView demo - Basic usage</h1>

Click on the thumbnails to open YoxView:

<!-- This is how your thumbnails should look like: -->
<div class="thumbnails yoxview">

    <?php
    $imageList = $images->getImages();
    $html = "";
    foreach ($imageList as $name => $image) {
        $html .= '
    <a href="'.$image.'"><img src="'.$image.$this->THUMBNAIL_EXTENSION.'" alt="First" title="'.$name.'" /></a>
        ';
    }

    echo $html;



    ?>

    <a href="/freeimages/server.png"><img src="/freeimages/server.png" alt="First" title="The first image" /></a>
	<a href="/freeimages/whiteboard.png"><img src="/freeimages/whiteboard.png" alt="First" title="The SECOND image" /></a>

</div>

<div style="clear: both;"></div>
