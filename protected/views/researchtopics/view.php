<?php
/* @var $this ResearchtopicsController */
/* @var $model ResearchTopics */

$this->breadcrumbs=array(
	'Research Topics'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ResearchTopics', 'url'=>array('index')),
	array('label'=>'Create ResearchTopics', 'url'=>array('create')),
	array('label'=>'Update ResearchTopics', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ResearchTopics', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ResearchTopics', 'url'=>array('admin')),
);
?>

<h1>View ResearchTopics #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'key',
		'name',
	),
)); ?>
