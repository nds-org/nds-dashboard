<?php
/* @var $this ResearchtopicsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Research Topics',
);

$this->menu=array(
	array('label'=>'Create ResearchTopics', 'url'=>array('create')),
	array('label'=>'Manage ResearchTopics', 'url'=>array('admin')),
);
?>

<h1>Research Topics</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
