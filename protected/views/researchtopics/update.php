<?php
/* @var $this ResearchtopicsController */
/* @var $model ResearchTopics */

$this->breadcrumbs=array(
	'Research Topics'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ResearchTopics', 'url'=>array('index')),
	array('label'=>'Create ResearchTopics', 'url'=>array('create')),
	array('label'=>'View ResearchTopics', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ResearchTopics', 'url'=>array('admin')),
);
?>

<h1>Update ResearchTopics <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>