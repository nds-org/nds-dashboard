<?php
/* @var $this ResearchtopicsController */
/* @var $model ResearchTopics */

$this->breadcrumbs=array(
	'Research Topics'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ResearchTopics', 'url'=>array('index')),
	array('label'=>'Manage ResearchTopics', 'url'=>array('admin')),
);
?>

<h1>Create ResearchTopics</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>