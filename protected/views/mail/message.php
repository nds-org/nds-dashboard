<?php

/** @var $recipient Member */
/** @var $sender Member */
/** @var $message string */

$imagesURL = $this->createAbsoluteUrl('/images');
$baseurl = $this->createAbsoluteUrl('/');

?>

<p><?php echo 'Dear '. $recipient->getFullName()?><p>

<p>
    <?php echo $message ?>
</p>

<br>


<?php
    if (!empty($sender)) {
        echo '<p>Yours Sincerely,</p>
        ';
        echo $sender->getSignature();
    }
?>

