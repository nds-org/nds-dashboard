<?php

/**
 * A Rather inflexible widget for display stats. It contains 4 items only
 * @var $name1
 * @var $name2
 * @var $val1
 * @var $val2
 */
?>


<div class="ui statistics">
  <div class="red statistic">
    <div class="value">
      <?php echo $val1 ?>
    </div>
    <div class="label">
        <?php echo $name1 ?>
    </div>
  </div>
  <div class="blue statistic">
    <div class="value">
        <?php echo $val2 ?>
    </div>
    <div class="label">
        <?php echo $name2 ?>
    </div>
  </div>
</div>

