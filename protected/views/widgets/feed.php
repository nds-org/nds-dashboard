<?php

/**
 * @var $ajax bool whether this is an ajax call.
 */

if (isset($ajax))
    $includeJS = !$ajax;
else
    $includeJS = true;

$feeds = Feed::model()->getActiveFeeds();
$myself = $this->getMember();

?>

<?php
if ($includeJS) {
    echo '<div id="feedme">';
};
?>


<div class="ui" style="height: 600px; overflow-y: auto;">

<div class="ui large header">School Bulletin Board</div>
    <div class="ui feed">

    <?php
        foreach ($feeds as $feed) {
            /** @var $feed Feed */
            $id=$feed->id;
            $pic = $feed->sender_avatar;
            $date = DateFormat::dateFormat($feed->date);
            $name = $feed->sender_name;
            $title = $feed->title;
            $message = $feed->message;
            $likes = $feed->likes;
            $icon = $feed->getFeedIcon($feed->type);
            $to = " to " .$feed->receiver_name;
            if ($feed->receiver_id >0)
                $to .= "<span style='float:right;'>".$feed->receiver_avatar."</span>";

            $include = $feed->memberHasAccess($myself);

            if ($include) {
                $summary = $feed->type_string." from ".$name.$to;
                $extraText =  $title." : ".$message;
    ?>


  <div class="event">
    <div class="label">
      <?php echo $pic?>
    </div>
    <div class="content">
      <div class="summary">
          <?php echo $summary ?>
        <div class="date">
            <i class="<?php echo $icon ?>"></i><?php echo $date; ?>
        </div>
      </div>
      <div class="extra text"><span style="font-style: oblique;"><?php echo $extraText ?>
      </div>
      <div class="meta">
        <a class="like">
          <i class="like icon" id="<?php echo $id?>"></i> <span class="like me" id="<?php echo $id?>"><?php echo $likes?></span> Likes
        </a>
      </div>
    </div>
  </div>
        <?php
        }
        }
?>

    </div>
</div>

<?php
if ($includeJS) {
    echo "</div>";
}
else return;
?>

<script>
    window.feedInterval = 10000;
    window.feeds = null;

    var getLikes = function () {
        $(".like.me").each(function(index){
            var likeText = $(this);
            var feedID = likeText.attr("id");
            var url = "<?php echo Pages::baseURL() ?>" + "feed/likes/id/" + feedID
            $.get(url, function(data) {
                likeText.text(data);
            });
        });
    };
    var getLikesTimed = function() {
        window.feedTimerID = setTimeout(function(){getLikesTimed();}, window.feedInterval);

        var url = "<?php echo Pages::baseURL() ?>" + "feed/feeds/"
        $.get(url, function(data) {
            if (data !== window.feeds) {
                $('#feedme').html(data);
                window.feeds = data;
            }
        });

    };

    $(document).on("click", ".like.icon", function(){
        var feedID = $(this).attr("id");
        $.get("<?php echo Pages::baseURL() ?>" + "feed/like/id/" +  feedID, function(data) {
            getLikes();
        });
    });

    $(document).ready(function(){
        window.feedTimerID = setTimeout(function(){getLikesTimed();}, window.feedInterval);
    });

</script>

