<?php
/** @var $models CActiveRecord */
/** @var $title bool */
/** @var $headings array*/
/** @var $column_names array */
/** @var $id_column array */
/** @var $include_submit */
/** @var $pagination */
/** @var $paginate */
/** @var $return_url */

if (!isset($return_url))
    $return_url="";
if (!isset($include_submit))
    $include_submit="";

if (!isset($paginate))
    $paginate=false;

if (!isset($pagination))
    $pagination=10;

$records = count($models);

$pages = intval($records / $pagination);

if (($pages * $pagination) < $records)
    $pages+=1;

$recs = array_values($models);
$arr1 = $recs[0];
$global_model_name = get_class($arr1);


// $column_names must start with a value on whether this row is selected.
?>

<div class="ui icon message">
    <i class="edit icon"></i>
    <div class="content">
        <div class="header">
            Edit the Current <?php echo $title ?>
        </div>
        <p>Use the Toggle Checkboxes to set the active <?php echo $title?>.</p>
    </div>
</div>

<div class="ui menu" style="margin-right: 20px; margin-left: 20px">
    <div class="item">
        <div class="ui basic reset button">Show All Rows</div>
    </div>
    <div class="item">
        Page <span class="ui circular page number label"> </span>
    </div>
    <div class="item">
        In View <span class="ui circular number of items label"> </span>
    </div>
    <div class="item">
        Items <span class="ui circular label"><?php echo $records; ?> </span>
    </div>
    <div class="item">
        Selected <span class="ui circular number selected label"></span>
    </div>

    <div class="ui right aligned item">
        <div class="ui icon input">
            <input id="selectable-search" type="text" placeholder="Search ..." style="height: 30px; ">
            <i class="search icon"></i>
        </div>
    </div>
</div>

<div class="ui form" style="margin-right: 20px">
    <form method="post">

        <input type="hidden" name="<?php echo $global_model_name."[-1]"?>" value="off">
        <table class="ui compact celled definition table">
            <thead>
            <tr>
                <?php
                foreach ($headings as $heading)
                    echo "<th>".$heading."</th>"
                ?>
            </tr>
            </thead>
            <tbody>

            <?php

            foreach ($models as $model) {

                $model_name = get_class($model);

                $current_id = $model->getAttribute($id_column);

                //  echo $model_name;

                echo "    <tr>
    ";

                foreach ($column_names as $index => $column) {
                    $name = $model_name.'['.$current_id.']'; // indexed by ID for this row so we can cross check in the controller

                    $id = $model_name.'_'.$current_id;

                    $current_value = $model->getAttribute($column);

                    if ($index == 0) {
                        if ($current_value == true)
                            $checked = 'checked="checked"';
                        else
                            $checked = '';

                        // echo "Row ID ".$current_id." is selected = ".$current_value." column name ".$column ;
                        echo '  <td class="collapsing">
                        <div class="ui selectable toggle checkbox">
                            <input name="'.$name.'" type="checkbox" '.$checked.'>
                            <label><i class="plus icon"></i></label>
                        </div>
                    </td>
            ';
                    } else {
                        echo '      <td>'.$current_value.'</td>
            ';
                    }
                }

                echo "    </tr>
    ";
            }
            ?>
            </tbody>
            <tfoot>
            </tfoot>

        </table>

    <div class="ui segment">
        <div class="ui stackable grid">
            <?php
            $button_space = "sixteen";
            if ($paginate) {
                $button_space = "eight";
            ?>
            <div class="left aligned eight wide column">
                <div class="ui compact pagination menu">
                  <a class="previous item">
                    <i class="left arrow icon"></i>
                  </a>
                    <?php
                    for ($i=0; $i<$pages; ++$i) {
                        echo '          <a class="page item">'.($i+1).'</a>
                        ';
                    }
                    ?>
                  <a class="next item">
                     <i class="icon right arrow"></i>
                  </a>
                </div>
            </div>
            <?php
            }
            ?>
            <div class="<?php echo $button_space?> wide column" style="float: right">

                <div class="ui grid">
                    <div class="eight wide column">
                        <div class="ui buttons">
                            <div class="ui positive check small button">
                                Check
                            </div>
                            <div class="or"></div>
                            <div class="ui negative uncheck small button">
                                UnCheck
                            </div>
                            <div class="ui " style="text-align: center">Apply In View</div>
                        </div>
                    </div>
                    <div class="eight wide column">
                        <div class="ui buttons">
                             <div class="ui positive checkall small button">
                                 Check
                             </div>
                            <div class="or"></div>
                             <div class="ui negative uncheckall small button">
                                 UnCheck
                             </div>
                            <div class="ui " style="text-align: center">Apply To All</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <?php
        if ($include_submit) {
        ?>
        <div class="ui basic segment">
                <?php
                echo CHtml::submitButton('Submit Changes' , array(
                    'class' => 'ui button', "id" => "button"));
                if (!empty($return_url)) {
                ?>
                <a href="<?php echo $return_url?>">
                    <div class="ui primary small button">
                             Cancel
                    </div>
                </a>
            <?php
                }
            ?>

                <br>
                <br>
        </div>
        <?php
        }
        ?>
    </form>
</div>

<script>

    window.selectablePage = 1;

    function setPage() {
        $(".pagination.menu").show();
        var selectedPage = window.selectablePage;
        var pages = parseInt(<?php echo $pages?>);
        if (selectedPage > pages)
            selectedPage  = pages;

        if (selectedPage < 1)
            selectedPage  = 1;

        window.selectablePage = selectedPage;

        var count=0;
        var pagination = parseInt(<?php echo $pagination ?>);
        var prePage = selectedPage-1;
        var lowerLimit = prePage * pagination;
        var upperLimit = selectedPage * pagination;

     //   alert(pagination + " " + lowerLimit  + " " + upperLimit  + " " + prePage  + " " + selectedPage + " " + pages);

        $.each($(".table tbody").find("tr"), function() {
            if ((count >= lowerLimit) && (count < upperLimit))
                $(this).show();
            else
                $(this).hide();
            ++count;
        });
        setNumbers();
    }

    function setNumbers() {
        numberSelected=0;
        var items = 0;
        $.each($(".table tbody").find("tr"), function() {
            if ($(this).is(':visible')) {
                items +=1;
            }
            var checkbox = $(this).find('input:checkbox');

            if (checkbox.prop('checked'))
                ++numberSelected
        });

        $('.number.of.items').html(items);
        $('.page.number').html(window.selectablePage);
        $('.number.selected').html(numberSelected)
    }

    $(document).ready(function(){
        $('.selectable.checkbox').checkbox('attach events');

        $(".check.button").click(function (event) {
            $.each($("input:checkbox"), function() {
                var line = $(this).closest('tr');
                if (line.is(':visible')) {
                    $(this).prop('checked', true);
                }
            });
            setNumbers();
        });

        $(".uncheck.button").click(function (event) {
            $.each($("input:checkbox"), function() {
                var line = $(this).closest('tr');
                if (line.is(':visible')) {
                    $(this).prop('checked', false);
                }
            });
            setNumbers();
        });

        $(".uncheckall.button").click(function (event) {
            $.each($("input:checkbox"), function() {
                var line = $(this).closest('tr');
                $(this).prop('checked', false);
            });
            setNumbers();
        });

        $(".checkall.button").click(function (event) {
             $.each($("input:checkbox"), function() {
                 var line = $(this).closest('tr');
                 $(this).prop('checked', true);
             });
            setNumbers();
         });

        $(".toggle.button").click(function (event) {
            $.each($("input:checkbox"), function() {
                var line = $(this).closest('tr');
                if (line.is(':visible')) {
                    if ($(this).prop('checked'))
                        $(this).prop('checked', false);
                    else
                        $(this).prop('checked', true);
                }
            });
            setNumbers();
        });

        $("input:checkbox").change(function (event) {
            setNumbers();
        });


        $("#selectable-search").on('paste, keydown, keyup, input propertychange', function (event) {
            var searchStr = $(this).val();
           // alert ("Got an event: " + event.which);
            var showing = 0;
            if (event.which != 13) {
                // alert("enter pressed");
                $.each($(".table tbody").find("tr"), function() {
                    if($(this).text().toLowerCase().indexOf(searchStr.toLowerCase()) == -1)
                        $(this).hide();
                    else {
                        $(this).show();
                        ++showing;
                    }
                });
            }
            if (showing < window.selectablePage) {
                $(".pagination.menu").hide();
                setNumbers();
            }
        });

        $(".reset").click(function() {
            $.each($(".table tbody").find("tr"), function() {
                $(this).show();
            });
            setPage();
        });

        <?php
        if ($paginate) {
        ?>

        $(".next.item").click(function() {
            var selectedPage = parseInt(window.selectablePage);
            window.selectablePage = selectedPage+1;
            setPage();
        });

        $(".previous.item").click(function() {
            var selectedPage = parseInt(window.selectablePage);
            window.selectablePage = selectedPage-1;
            setPage();
        });
        $(".page.item").click(function() {
            window.selectablePage=parseInt($(this).text().trim());
            setPage();
        });


        setPage();
        <?php
        }
        ?>

        setNumbers();

    });

</script>
