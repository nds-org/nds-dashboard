<?php
/** @var $menu array */

$baseurl = Yii::app()->request->baseUrl;

$controller = StateVariables::getCurrentControllerName();

//array('label'=>'View Institutes', 'url'=>array('view', 'id'=>$model->id));
// add icons ? <i class="aligned right file new icon">
//  array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->member_id),'confirm'=>'Are you sure you want to delete this member?')),

?>
    <div class="ui buttons">

        <?php
            foreach ($menu as $index => $array_item) {
                $confirm_string = "";
                $label = $array_item['label'];
                $url = isset($array_item['url']) ? $array_item['url'] : null;

                if (is_array($url)) {
                    if (isset($url['id']))
                        $url = $this->createUrl($url[0], array("id" => $url['id']));
                    else
                        $url = $this->createUrl($url[0]);
                } else {
                    $linkOptions = isset($array_item['linkOptions']) ? $array_item['linkOptions'] : null;

                    if (isset($linkOptions)) {
                        $url = $this->createUrl($linkOptions['submit'][0], array('id'=>$linkOptions['submit']['id']));
                        $confirm_string = 'onclick="return confirm(\''.$linkOptions["confirm"].'?\');"';
                        }
                }

        ?>

            <a href="<?php echo $url ?>" <?php echo $confirm_string ?>>
                <div class="ui blue basic button"> <?php echo $label ?>
                </div>
            </a>

        <?php
        }
        ?>

    </div>

