<?php

/**
 * A Yii compatible dropdown designed for use in forms. It takes in a model that you are creating or editing
 * and sets the id, name and default value according to that. Then it takes a list of records and uses the
 * supplied id_attribute and text_attribute to generate a semantic ui compatible list of entries.
 */

/** @var $model CActiveRecord */
/** @var $attribute string */

/** @var $records array of CActiveRecords or an array of key/value pairs */
/** @var $id_attribute string */
/** @var $text_attribute string */


$model_name = get_class($model);

$name = $model_name.'['.$attribute.']';
$id = $model_name.'_'.$attribute;

$default_value = $model->getAttribute($attribute);

$vals = "";

if (count($records) == 0) {
    echo '<div class="ui red message">Sorry, there were no elements in this list.</div>';
}

$recs = array_values($records);

if (count($recs) == 0) return;

$arr1 = $recs[0];

if (is_string($arr1)) {
    $default_text = $text_attribute;
    foreach ($records as $key => $val) {
        $vals .='<div class="item" data-value="'.$key.'">'.$val.'</div>
        ';
    }
} else {
    $default_text = $model->getAttribute($text_attribute);

    foreach ($records as $index => $record) {
        /** @var $record CActiveRecord */
        $key = $record->getAttribute($id_attribute);
        $val = $record->getAttribute($text_attribute);
        $vals .='<div class="item" data-value="'.$key.'">'.$val.'</div>
        ';
    }
}

    echo '

     <div class="ui selection fluid dropdown">
  <input  type="hidden" name="'.$name.'" id="'.$id.'" value="'.$default_value.'">
  <div class="default text">'.$default_text.'</div>
  <i class="dropdown icon"></i>
  <div class="menu">
  '.$vals.'
  </div>
</div>
';