<?php

/** @var $model CActiveRecord */
/** @var $columns array */

$baseurl = Yii::app()->request->baseUrl;

$class_name_lc = strtolower(StateVariables::getCurrentControllerName());
?>

<table class="ui red table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Value</th>
        </tr>
    </thead>

  <tbody>

  <?php

      foreach ($columns as $column_name) {
          echo "        <tr>
            ";
            $current_value = $model->getAttribute($column_name);
            echo "
                    <td>".$model->getAttributeLabel($column_name)."</td>
                    <td>".$current_value."</td>";

           echo "        </tr>
            ";

            }

    ?>

  </tbody>
  <tfoot>

  <tr>

      <th></th>
      <th></th>

  </tr></tfoot>
</table>

