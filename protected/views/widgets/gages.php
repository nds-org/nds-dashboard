<?php


if (!Plugins::shouldShowAlerts())
    return;

$baseurl = Yii::app()->request->baseUrl;

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScriptFile(Pages::baseURL()."vendor/justgage.1.0.1/resources/js/raphael.2.1.0.min.js");
$cs->registerScriptFile(Pages::baseURL()."vendor/justgage.1.0.1/resources/js/justgage.1.0.1.min.js");

$cellphone = Mobile::isMobileDevice();
?>

<style>

    #g1, #g3 {
        <?php if ($cellphone) { ?>
            width:200px; height:160px;
        <?php } else { ?>
            width:200px; height:160px;
        <?php } ?>
        display: block;
        margin: 1.4em;
    }

    tspan {
        color: green;
        padding-bottom: 30px;
    }
</style>


<script>


    var g1, g2, g3;

    window.onload = function(){

        var width = 700;
        var gage1 = $('#g1');
        var gage2 = $('#g3');
        if( $(window).width() < width ) {
            $(gage1).width(300).height(240);
            $(gage2).width(300).height(240);
          }

        var g1 = new JustGage({
            id: "g1",
            value: <?php $this->actionNumdata(); ?>,
            min: 0,
            max: 10,
            showMinMax: false,
            shadowOpacity: 1,
            shadowSize: 0,
            shadowVerticalOffset: 10,
            levelColors: ["#6ECFF5"],
            title: "   ",
            levelColorsGradient: false,
            label: "Data Items"

        });

        var g3 = new JustGage({
            id: "g3",
            value: <?php $this->actionNumpubs(); ?>,
            min: 0,
            max: 10,
            showMinMax: false,
            shadowOpacity: 1,
            shadowSize: 0,
            shadowVerticalOffset: 10,
            levelColors: ["#D95C5C"],
            title: "   ",
            levelColorsGradient: false,
            label: "Publications"

        });



        setInterval(function() {
            $.get('<?php echo Pages::getAbsoluteBaseURL()."dashboard/totalchallenges";?>',
                function (newValue) { g1.refresh(newValue); });
            $.get('<?php echo Pages::getAbsoluteBaseURL()."dashboard/totalscore";?>',
                function (newValue) { g3.refresh(newValue); });

        }, 2500);
    };
</script>

<div class="ui stackable grid">

    <div class="middle aligned eight wide column">
         <div class="ui fluid card">
             <div class="image" style="background-color: white">
                 <a href="<?php echo $baseurl.'/resource/admin'?>">
                     <div id="g1"></div>
                 </a>
             </div>
             <div class="content">
               <a class="large header" style="text-align: center">Your Data Items</a>
               <div class="meta">
                 <a class="time">
                     <a href="<?php echo $baseurl.'/resource/admin'?>">
                         <div class="ui basic fluid orange button">
                           <i class="icon eye open"></i>
                           View
                         </div>
                     </a>
                 </a>
               </div>
             </div>
         </div>
     </div>

    <div class="middle aligned eight wide column">
        <div class="ui fluid card">
            <div class="image" style="background-color: white">
                <a href="<?php echo $baseurl.'/article/list'?>">
                        <div id="g3" ></div>
                    </a>
            </div>
            <div class="content">
                <a class="large header" style="text-align: center">Your Publications</a>
              <div class="meta">
                <a class="time">
                    <a href="<?php echo $baseurl.'/article/list'?>">
                        <div class="ui basic fluid orange button">
                          <i class="icon eye open"></i>
                          View
                        </div>
                    </a>

                </a>
              </div>
            </div>

        </div>
    </div>

</div>
