<?php

/** @var $model CActiveRecord */
/** @var $records array */
/** @var $columns array */
/** @var $id_name string */
/** @var $pagination */
/** @var $paginate */

$baseurl = Yii::app()->request->baseUrl;

$class_name_lc = strtolower(StateVariables::getCurrentControllerName());

if (isset($records))
    $all_items = $records;
else
    $all_items = $model->findAll();

if (!isset($include_submit))
    $include_submit=False;

if (!isset($paginate))
    $paginate=false;

if (!isset($pagination))
    $pagination=10;


$record_count = count($all_items);

$pages = intval($record_count / $pagination);

if (($pages * $pagination) < $record_count)
    $pages+=1;

?>


<div class="ui menu">
  <a class="item" href="<?php echo $baseurl."/".$class_name_lc."/create/" ?>">
          <div class="ui icon blue basic button">Add <?php echo ucfirst($class_name_lc) ?> <i class="file new icon"></i>
          </div>
  </a>

    <div class="item">
         <div class="ui basic reset button">Show All Rows</div>
     </div>
     <div class="item">
         Page <span class="ui circular page number label"> </span>
     </div>
     <div class="item">
         In View <span class="ui circular number of items label"> </span>
     </div>
     <div class="item">
         Items <span class="ui circular label"><?php echo $record_count; ?> </span>
     </div>

     <div class="ui right aligned item">
         <div class="ui icon input">
             <input id="selectable-search" type="text" placeholder="Search ..." style="height: 30px; ">
             <i class="search icon"></i>
         </div>
     </div>

</div>

<table class="ui red table">
    <thead>
        <tr>
            <?php
            foreach ($columns as $column_name) {
                $display_name = $model->getAttributeLabel($column_name);
                echo "
                      <th>".ucfirst($display_name)."</th>";
            }
            ?>
            <th>Tools</th>
        </tr>
    </thead>

  <tbody>

  <?php
  foreach ($all_items as $obj) {
      echo "        <tr>
        ";

      foreach ($columns as $column_name) {
            $current_value = $obj->getAttribute($column_name);
            echo "
                    <td>".$current_value."</td>";
            }

            ?>

      <td>
          <a href="<?php echo $baseurl."/".$class_name_lc."/view/".$obj->getAttribute($id_name) ?>"><i class="large eye icon"></i></a>
          <a href="<?php echo $baseurl."/".$class_name_lc."/update/".$obj->getAttribute($id_name) ?>"><i class="large edit icon"></i></a>
      </td>

      <?php
          echo "        </tr>
            ";
      }
  ?>

  </tbody>
<tfoot>
<tr>
</tr>
</tfoot>
</table>

<?php
$button_space = "sixteen";
if ($paginate) {
    $button_space = "eight";
?>

<div class="ui segment">
     <div class="ui stackable grid">
         <div class="left aligned eight wide column">
             <div class="ui compact pagination menu">
               <a class="previous item">
                 <i class="left arrow icon"></i>
               </a>
                 <?php
                 for ($i=0; $i<$pages; ++$i) {
                     echo '          <a class="page item">'.($i+1).'</a>
                     ';
                 }
                 ?>
               <a class="next item">
                  <i class="icon right arrow"></i>
               </a>
             </div>
         </div>
     </div>
 </div>
<?php
}
?>

     <?php
     if ($include_submit) {
     ?>
     <div class="ui basic segment">
         <div class="field">
             <?php
             echo CHtml::submitButton('Submit Changes' , array(
                 'class' => 'ui button', "id" => "button"));
             ?>
             <br>
             <br>
         </div>
     </div>
     <?php
     }
     ?>

<br>
<br>
<br>

<script>

 window.selectablePage = 1;

 function setPage() {
     var selectedPage = window.selectablePage;
     var pages = parseInt(<?php echo $pages?>);
     if (selectedPage > pages)
         selectedPage  = pages;

     if (selectedPage < 1)
         selectedPage  = 1;

     window.selectablePage = selectedPage;

     var count=0;
     var pagination = parseInt(<?php echo $pagination ?>);
     var prePage = selectedPage-1;
     var lowerLimit = prePage * pagination;
     var upperLimit = selectedPage * pagination;

  //   alert(pagination + " " + lowerLimit  + " " + upperLimit  + " " + prePage  + " " + selectedPage + " " + pages);

     $.each($(".table tbody").find("tr"), function() {
         if ((count >= lowerLimit) && (count < upperLimit))
             $(this).show();
         else
             $(this).hide();
         ++count;
     });
     setNumbers();
 }

 function setNumbers() {
     var items = 0;
     $.each($(".table tbody").find("tr"), function() {
         if ($(this).is(':visible')) {
             items +=1;
         }
     });

     $('.number.of.items').html(items);
     $('.page.number').html(window.selectablePage);
 }

 $(document).ready(function(){
     $('.selectable.checkbox').checkbox('attach events');

     $(".check.button").click(function (event) {
         $.each($("input:checkbox"), function() {
             var line = $(this).closest('tr');
             if (line.is(':visible')) {
                 $(this).prop('checked', true);
             }
         });
     });

     $(".uncheck.button").click(function (event) {
         $.each($("input:checkbox"), function() {
             var line = $(this).closest('tr');
             if (line.is(':visible')) {
                 $(this).prop('checked', false);
             }
         });
     });

     $(".toggle.button").click(function (event) {
         $.each($("input:checkbox"), function() {
             var line = $(this).closest('tr');
             if (line.is(':visible')) {
                 if ($(this).prop('checked'))
                     $(this).prop('checked', false);
                 else
                     $(this).prop('checked', true);
             }
         });
     });


     $("#selectable-search").keyup(function (event) {
         var searchStr = $(this).val();
         if (event.which != 13) {
             // alert("enter pressed");
             $.each($(".table tbody").find("tr"), function() {
                 if($(this).text().toLowerCase().indexOf(searchStr.toLowerCase()) == -1)
                     $(this).hide();
                 else
                     $(this).show();
             });
         }
         setNumbers();
     });

     $(".reset").click(function() {
         $.each($(".table tbody").find("tr"), function() {
             $(this).show();
         });
     });

     <?php
     if ($paginate) {
     ?>

     $(".next.item").click(function() {
         var selectedPage = parseInt(window.selectablePage);
         window.selectablePage = selectedPage+1;
         setPage();
     });

     $(".previous.item").click(function() {
         var selectedPage = parseInt(window.selectablePage);
         window.selectablePage = selectedPage-1;
         setPage();
     });
     $(".page.item").click(function() {
         window.selectablePage=parseInt($(this).text().trim());
         setPage();
     });


     setPage();
     <?php
     }
     ?>

     setNumbers();

 });

</script>