<?php
/**
 * @var $model CActiveRecord the model
 * @var $attribute string the attribute in the model that this checkbox is for.
 * @var $prefix string an alternative naming scheme for this toggle.
 * @var $callback_url string the callback url on a change, if wanted

 * if a prefix is set then the prefix can be Feed[22] for example because it is the Feed with id 22 in a list of feed
 * objects that have an approved flag on them
 *
Feed[22][approved]
 *
 * For callbacks the API is generated automatically but you need a controller to take it:
 *
 * $callback_url + "?id=22&val=0/1
 *
 * val is either 0 or 1 for the new value.
 *
 */

if (!isset($callback_url))
    $callback_url = "";

if (!isset($prefix))
   $prefix= get_class($model);

$name_prefix = $prefix;

$prefix = str_replace("[","_",$prefix);
$prefix = str_replace("]","",$prefix);

$name = $name_prefix.'['.$attribute.']';
$id = $prefix.'_'.$attribute;
$default_value = $model->getAttribute($attribute);

if ($default_value ==1 )
    $checked = 'checked="checked"';
else
    $checked = '';
?>

<div class="ui toggle checkbox" id="<?php echo $id ?>">
    <input name="<?php echo $name ?>"  type="checkbox" <?php echo $checked ?>>
    <label><i class="plus icon"></i></label>
</div>

<script>
    $(document).ready(function(){
        $('.ui.checkbox')
          .checkbox()
        ;

        $("input:checkbox").change(function (event) {
             var url = "<?php echo $callback_url ?>";
             var selected = $(this);
             var name = selected.prop("name");
             var pos1 = name.indexOf("[");
             var pos2 = name.indexOf("]");
             var id = name.substring(pos1+1, pos2);
             var newVal = 0;
             if ($(this).prop('checked'))
                newVal = 1;

            if ((url && url.length>0)) {
               $.get(url + "?id=" + id + "&val="+newVal, function(data) {
                 // alert(data);
                 });
            }
         });

    });
</script>
