<?php
/** @var $member Member  */

$school = School::model()->getBySchoolID($member->school_id);
$schoolName ="NA";
if (!empty($school))
    $schoolName = $school->name;

$baseurl = Yii::app()->request->baseUrl;

?>

<div class="ui fluid card">
  <div class="image">
      <p><a href="<?php echo $baseurl?>/member/profilepic" id="elem" rel="popover" data-placement="top" data-original-title="Click To Change Profile Picture">
      <img src="<?php echo $baseurl?>/member/mypicture" width="100%" style="width: 100%;" >
      </a>
  </div>
  <div class="content">
    <a class="header">Welcome, <?php echo $member->getFullName() ?></a>
    <div class="meta">
      <span class="date">Joined: <?php echo DateFormat::dateFormat($member->date_registered) ?></span>
    </div>
    <div class="description">
        <?php echo $member->getFullName() ?> is a <?php echo $member->role ?> in <?php echo $schoolName ?>
    </div>
  </div>
  <div class="extra content">
    <a>
      <i class="user icon"></i>
        <a href="<?php echo $baseurl?>/member/profile">
        Edit Your Profile
        </a>
    </a>
  </div>
</div>

