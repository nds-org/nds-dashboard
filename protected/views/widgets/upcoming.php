
<?php

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScriptFile(Pages::baseURL()."js/upcoming.js");

$url = Pages::baseURL()."member/";

$cs->registerScript("get-calendars", "
getCalendar(\"".$url."\");
");

$cs->registerCssFile(Pages::baseURL()."css/upcoming.css");
?>


<div class="ui stacked segment" style="padding: 5px; padding-bottom: 10px">
    <div class="ui large orange fluid label" style="margin-bottom: 1px">Publications</div>
    <div class="events-calendar">
        <div class='row'>
            <div class='nearest'>
                <div class='heading'>
                    Last Added:
                </div>
                <div class="first-calendar-event">

                </div>
            </div>
            <div class='upcoming'>
                <div class='heading'>
                    Previous:
                </div>

                <div class="next-calendar-events">

                </div>
            </div>
        </div>
    </div>
</div>