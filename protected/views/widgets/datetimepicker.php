<?php
/**
 * A Basic datetime chooser from jquery. Just pass it the initialisation time and the name
 * you would like it to provide a value to as input. This widget needs to be wrapped in a form too.
 *
 * @var $name string
 * @var $datetime string
 *
 * Set the timezone to true if you want to add time zone support.
 *
echo $this->renderPartial('/widgets/datetimepicker', array(
     'name'=>"mytime",
     'datetime' => time()
 ));

 */

$datetime = DateFormat::dateFormatFromUnix($datetime);

$member = Member::model()->getCurrentMember();
$memberpref = MemberPreferences::model()->getByMemberID($member->member_id);

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerCssFile(Pages::baseURL()."vendor/datetimepicker/jquery.datetimepicker.css");
$cs->registerScriptFile(Pages::baseURL()."vendor/datetimepicker/jquery.datetimepicker.js");

$cs->registerScript("spectrum-".$name, "
$('#datetimepicker').datetimepicker({
        format:'".$memberpref->date_style."'
    });
");
?>


<input id="datetimepicker" type="text" name="<?php echo $name?>" value="<?php echo $datetime ?>">

<?php
/**
In controller you can get the time and convert it to the UTC as follows:
 *
 *
        $time = $_POST['mytime'];
        $new_time = DateFormat::dateFormatToMysqlUTCTime($time);
        echo $new_time."<br>";

 */
