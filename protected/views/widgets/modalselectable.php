<?php
/** @var $models CActiveRecord */
/** @var $title bool */
/** @var $headings array*/
/** @var $column_names array */
/** @var $id_column array */
// $column_names must start with a value on whether this row is selected.
?>

<div class="ui large funky choice button">
    Select Teacher Challenges
</div>

<div class="ui funky choice large modal">
  <i class="close icon"></i>
  <div class="header">
      Manage <?php echo $title ?>
  </div>

    <?php
    echo $this->renderPartial('/widgets/selectable', array(
        'models'=>$models,
        'title'=>$title,
        'headings'=>$headings,
        'id_column' => $id_column,
        'column_names'=>$column_names,
        'include_submit'=>false
    ));
    ?>
<br>
    <div class="actions">
      <div class="ui positive right labeled icon button">
        Select
        <i class="checkmark icon"></i>
      </div>
    </div>
</div>

<script>

    $(document).ready(function(){

        $('.funky.choice.button').click(function() {
            $('.funky.choice.modal')
              .modal('show')
            ;
        });

    });

</script>