<?php

/** @var $member Member  */

$school = School::model()->getBySchoolID($member->school_id);
$schoolName ="NA";
if (!empty($school))
    $schoolName = $school->name;

$baseurl = Yii::app()->request->baseUrl;

if ( $member->role_id == Role::$TEACHER) {
    if ($member->gender === "Male")
        $title = "Mr ".$member->last_name."'s Dashboard";
    else
        $title = "Miss ".$member->last_name."'s Dashboard";
} else
    $title = $member->first_name."'s Dashboard";

$myteam = Team::model()->getTeamForMember($member);

if (isset($myteam))
    $team_name = "In The ".$myteam->title. " Team ";
else
    $team_name = "";

?>

<div class="ui segment">
    <h2 class="ui header">
      <i class="settings icon"></i>
      <div class="content">
          <?php echo $title ?>
        <div class="sub header">
            <?php echo $team_name?>
            <div style="font-style: italic; ">Your eMaslow account stats and rankings!</div>
        </div>
      </div>
    </h2>



    <div class="ui fluid statistics">
      <div class="red statistic" style="float: left; margin-left: 10%">
        <div class="value">
          <?php echo $this->actionTotalscore() ?>
        </div>
        <div class="label">
            Points Earned
        </div>
      </div>
      <div class="blue statistic" style="float: right; margin-right: 10%">
        <div class="value">
            <?php echo $this->actionTotalchallenges() ?>
        </div>
        <div class="label">
            Challenges Completed
        </div>
      </div>
    </div>



</div>

