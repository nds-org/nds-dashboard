<?php

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScriptFile(Pages::baseURL()."js/alerts.js");

$isGuest = StateVariables::isGuest();

$alerts = array();

$rootController = Pages::getAbsoluteBaseURL()."member/";

// icon, color, format Name, CSS Class for total, CSS class for content, total count url, content url
$alerts[] = array("comment","red","Your Data Items", "total-data", "data", $rootController."numdata", $rootController."data", false);
$alerts[] = array("ticket","blue","Your Publications", "total-publications", "publications", $rootController."numpubs", $rootController."pubs", false);

$alertsJson = json_encode($alerts);

if (!$isGuest) {
    $member = Member::model()->getCurrentMember();
    $role_id = $member->role_id;

    $cs->registerScript("get-alerts", "
        getAlerts('".$alertsJson."');
    ", CClientScript::POS_END);

}

?>
<style>

    .drop.space {
        width:500px;
        height:500px;
        overflow-y: auto;
        overflow-x: hidden;
        word-wrap: break-word;
    }

    .ui.feed {
        width: 100%;
        overflow-x: hidden;
        word-wrap: break-word;
        white-space:normal;
    }

</style>

<script>
    // This code detects smaller devices upon startup and moves the dropdowns to the 0 x coordinate
    // so it looks more like an app

    $(document).ready(function(){
        $('.my.dropdown')
            .dropdown({
                transition: 'drop'
              });

        var width = 700;
        var dropspace = $('.drop.space');
        if( $(window).width() < width ) {
            $(dropspace).each(function() {
                $(this).css({ position: "fixed",
                                marginLeft: 0, marginTop: 0,
                                top: 80, left: 0});
                $(this).width(350).height(500);
            })
        }
    });

</script>
<?php

    $alertHTML = '<div class="ui menu" style="float: right; margin-right:2px;">';

    foreach ($alerts as $alert) {
        // format Name, CSS Class for total, CSS class for content, total count url, content url

        $alertHTML .= '
                   <div class="ui my right top pointing dropdown item">
                        <i class="icon '.$alert[0].'"></i>
                        <div class="ui floating '.$alert[1].' label">
                            <i class="'.$alert[3].'">--</i>
                        </div>
                        <div class="ui drop space menu">
                            <div class="item">'.ucfirst($alert[2]).'</div>
                            <div class="item">
                               <div class="'.$alert[4].'">Not Available</div>
                            '.
                            '</div>
                        </div>
                    </div>
        ';
    }
    $alertHTML .='</div>';


if (Plugins::shouldShowAlerts()) {
    ?>
    <div class="computer only four wide column">
        <?php echo $alertHTML; ?>
    </div>

    <div class="tablet only four wide column">
        <?php echo $alertHTML; ?>
    </div>

    <div class="mobile only seven wide column">
        <?php echo $alertHTML; ?>
    </div>

<?php
} else {
    ?>
    <div class="computer only five wide column"></div>
    <div class="tablet only five wide column"></div>
    <div class="mobile only five wide column"></div>
<?php
}
?>



