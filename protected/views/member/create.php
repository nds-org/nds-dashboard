<?php
/* @var $this MemberController */
/* @var $model Member */

$this->breadcrumbs=array(
//    'Dashboard'=>array('member/dashboard', 'id'=>12),
    'Dashboard'=>array('member/dashboard'),
    'Create User',
);
?>

<h1>Create a User</h1>
<?php

echo $this->renderPartial('_form', array(
    'model'=>$model,
    'readonly'=>false,
    'create' => "TRUE",
));
?>

