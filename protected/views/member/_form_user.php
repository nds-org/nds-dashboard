<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */
/** @var $readonly boolean */
?>

<div class="ui form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'member-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="ui grid">

    <div class="eight wide column">

            <div class="field">
                <h3>Personal Details</h3>
            </div>
            <div class="field">
                <?php echo $form->labelEx($model,'username'); ?>
                <span class="add-on"><span class="icon-user"></span></span>
                <?php echo $form->textField($model,'username',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly,
                        'placeholder'=>"Username", 'autocomplete'=>"off")); ?>

                <?php echo $form->error($model,'username'); ?>
            </div>


            <?php

            if (!$readonly) {
                echo '<div class="field">';

                $model->password="";

                echo $form->labelEx($model,'password'); ?>
                <span class="add-on"><span class="icon-key"></span></span>
                <?php echo $form->passwordField($model,'password',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly,
                        'placeholder' => $readonly? '' : 'Enter Password To Change...',
                        'autocomplete'=>"off")); ?>

                <?php echo $form->error($model,'password');

                echo '</div>';

            } ?>


            <div class="field">
                <?php echo $form->labelEx($model,'first_name'); ?>
                <span class="add-on"><span class="icon-user"></span></span>
                <?php echo $form->textField($model,'first_name',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

                <?php echo $form->error($model,'first_name'); ?>
            </div>


            <div class="field">
                <?php echo $form->labelEx($model,'last_name'); ?>
                <span class="add-on"><span class="icon-user"></span></span>
                <?php echo $form->textField($model,'last_name',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

                <?php echo $form->error($model,'last_name'); ?>
            </div>

            <div class="field">
                <?php echo $form->labelEx($model,'title'); ?>
                <span class="add-on"><span class="icon-user"></span></span>
                <?php echo $form->textField($model,'title',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

                <?php echo $form->error($model,'title'); ?>
            </div>


            <div class="field">
                <?php echo $form->labelEx($model,'email'); ?>
                <span class="add-on"><span class="icon-mail-reply"></span></span>
                <?php echo $form->textField($model,'email',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>
                <?php echo $form->error($model,'email'); ?>
            </div>

    </div>
    </div>


    <br>
     <br>

     <div class="field">
         <?php
         if (!$readonly)
             echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array(
                 'class' => 'ui button', "id" => "button"));
         ?>
     </div>


    <?php $this->endWidget(); ?>

</div><!-- form -->