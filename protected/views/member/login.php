<?php

$this->pageTitle=Yii::app()->name . ' - Login';

?>

<?php
$baseurl = Yii::app()->request->baseUrl;

?>

<?php
$this->widget('zii.widgets.CBreadcrumbs', array(
    'links'=>$this->breadcrumbs,
));
?>

<div class="ui page basic segment">

    <h1 class="ui header">
        Login <small> Into Your Account</small>
    </h1>


            <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableClientValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>true,
                ),
            ));


            $baseurl = Yii::app()->request->baseUrl;

            ?>

    <p>Please fill out the following form with your login credentials:</p>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

        <div class="ui two column middle aligned relaxed grid basic segment">
          <div class="column">
            <div class="ui form segment">
              <div class="field">
                <label>Username</label>
                <div class="ui left labeled icon input">
                    <?php echo $form->textField($model,'username',
                                            array("placeholder"=>"Username ...")); ?>
                    <i class="user icon"></i>
                  <div class="ui corner label">
                    <i class="asterisk icon"></i>
                  </div>
                    <?php echo $form->error($model,'username'); ?>
                </div>
              </div>
              <div class="field">
                <label>Password</label>
                <div class="ui left labeled icon input">
                    <?php echo $form->passwordField($model,'password',
                        array("placeholder"=>"Password ...")); ?>
                   <i class="lock icon"></i>
                  <div class="ui corner label">
                    <i class="asterisk icon"></i>
                  </div>
                    <?php echo $form->error($model,'password'); ?>
                </div>
              </div>
                <?php echo CHtml::submitButton('Login', array(
                     'class' => 'huge ui blue button', "id" => "button",
                 )); ?>

            </div>
          </div>
          <div class="ui vertical divider">
            Or
          </div>
          <div class="center aligned column">
            <div class="huge green ui labeled icon button">
              <i class="signup icon"></i>
              Sign Up
            </div>
          </div>
        </div>

    <?php echo $form->errorSummary($model); ?>

    <?php $this->endWidget(); ?>

    <br>
    <br>
    <br>

    </div>



<!--    <p> If you have forgotten your password, please <a href="--><?php //echo $baseurl?><!--/site/passwordreset">-->
<!--            reset your password</a> now.</p>-->

