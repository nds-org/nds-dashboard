<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="span12">
		<?php echo $form->label($model,'member_id'); ?>
		<?php echo $form->textField($model,'member_id'); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'username'); ?>
		<?php echo $form->textField($model,'username'); ?>
	</div>


	<div class="span12">
		<?php echo $form->label($model,'institute_id'); ?>
		<?php echo $form->textField($model,'institute_id'); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name'); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name'); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'date_registered'); ?>
		<?php echo $form->textField($model,'date_registered'); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'work_phone'); ?>
		<?php echo $form->textField($model,'work_phone'); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'extension'); ?>
		<?php echo $form->textField($model,'extension',array('size'=>5,'maxlength'=>5)); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'fax'); ?>
		<?php echo $form->textField($model,'fax'); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'mobile_phone'); ?>
		<?php echo $form->textField($model,'mobile_phone',array('size'=>14,'maxlength'=>14)); ?>
	</div>


	<div class="span12">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
	</div>


	<div class="span12">
		<?php echo $form->label($model,'active'); ?>
		<?php echo $form->textField($model,'active',array('size'=>1,'maxlength'=>1)); ?>
	</div>

	<div class="span12">
		<?php echo $form->label($model,'notes'); ?>
		<?php echo $form->textArea($model,'notes',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->