<?php

// Passed into this view:

/** @var $member Member */
/** @var $preferences MemberPreferences */

$baseurl = Yii::app()->baseUrl;
$imageurl = $baseurl ."/images/";
$role = Yii::app()->user->getState('userrole');

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScript("tooltip",  "
$('#elem').tooltip()
");

$this->breadcrumbs=array(
    'Dashboard'=>array('/member/dashboard'),
	'Profile',
);

/* @var $this MemberController */
/* @var $model Member */
/* @var $preferences MemberPreferences */

?>

<script>
  $(document)
    .ready(function() {
      $('.demo.menu .item')
        .tab()
      ;
    })
  ;
  </script>


<h3 class="page-title">
    <?php echo $member->first_name. " ". $member->last_name?> <small> User Profile</small>
</h3>

<div class="ui raised segment">
    <div class="ui red ribbon label">Profile</div>

   <div class="ui pointing secondary demo menu">
        <a class="active red item" data-tab="first">Overview</a>
        <a class="red item" data-tab="second">Profile</a>
        <a class="red item" data-tab="third">Edit Profile</a>
        <a class="red item" data-tab="forth">Edit Preferences</a>
    </div>

    <div class="ui active tab segment" data-tab="first">
        <div class="row-fluid">
            <div class="span3">

            <a href="<?php echo $baseurl?>/member/profilepic" id="elem" rel="popover" data-placement="top" data-original-title="Click To Change Profile Picture">
                 <img src="<?php echo $baseurl?>/member/mypicture" width="120px">
            </a>
            </div>
            <div class="span8">
            <div class="row-fluid">
                <div class="span8 profile-info">
                    <h1><?php echo $member->first_name. " ". $member->last_name?></h1>
                        <div class="naked-well">

                               <?php
                               if (!empty($member->title))
                                    echo TextFormat::getBackToBackTwoColumn('Title', $member->title)
                               ?>
                               <?php
                                    echo TextFormat::getBackToBackTwoColumn('Member ID', $member->member_id)
                               ?>
                            <?php
                                  echo TextFormat::getBackToBackTwoColumn('Role', $role)
                             ?>
                            <?php
                                  echo TextFormat::getBackToBackTwoColumn('Email', $member->email)
                             ?>

                            <div class="clearfix"></div>
                        </div>
                    <br>
                    <p>Thank you for being a member since <?php echo DateFormatter::dateFormat($member->date_registered) ?>.

                </div>
                <!--end span8-->
                </div>
                <!--end row-fluid-->
            </div>
            <!--end span9-->
        </div>
    </div>
<!--end tab-pane-->
    <div class="ui tab segment" data-tab="second">

    <?php
          echo $this->renderPartial('_form_user', array(
              'model'=>$member,
              'readonly'=>true
          ));

      ?>

    </div>
<!--tab_1_2-->
    <div class="ui tab segment" data-tab="third">

        <?php
            echo $this->renderPartial('_form_user', array(
                'model'=>$member,
                'readonly'=>false
            ));

        ?>

    </div>
<!--end tab-pane-->
    <div class="ui tab segment" data-tab="forth">

            <?php
                 echo $this->renderPartial('/memberpreferences/create', array(
                     'model'=> $preferences,
                     'readonly'=>false
                 ));

             ?>
    </div>

</div>
