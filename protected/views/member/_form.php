<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */
/** @var $readonly boolean */
/** @var $create string */

$creating = false;

if (isset($create)) {
    $creating = true;
}

Yii::app()->clientScript->registerScript('form-toggle', "
$('.memberlist').hide();
$(':checkbox').click(function(){
	$('.abracadabra').toggle();
	$('.memberlist').toggle();
	return true;
});");

?>

<div class="ui form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'member-form',
    'enableAjaxValidation'=>false,
)); ?>

<p class="note">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<input type="text" style="display:none">
<input type="password" style="display:none">


<div class="ui grid abracadabra">

<div class="eight wide column">

        <div class="field">
            <h3>Personal Details</h3>
        </div>

        <?php

//        if ($creating) {
            echo '<div class="field">';
            echo $form->labelEx($model,'username');
            echo '<span class="add-on"><span class="icon-user"></span></span>';
            echo $form->textField($model,'username',
                     array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly,
                         'placeholder'=>"Username", 'autocomplete'=>"off"));

            echo $form->error($model,'username');
            echo '</div>';

            echo '<div class="field">';
            $model->password="";

            echo $form->labelEx($model,'password'); ?>
            <span class="add-on"><span class="icon-key"></span></span>
            <?php echo $form->passwordField($model,'password',
                array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly,
                    'placeholder' => $readonly? '' : 'Enter Password To Change...', 'autocomplete'=>"off")); ?>

            <?php echo $form->error($model,'password');

            echo '</div>';
//            }
        ?>


        <div class="field">
            <?php echo $form->labelEx($model,'first_name'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->textField($model,'first_name',
                array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

            <?php echo $form->error($model,'first_name'); ?>
        </div>


        <div class="field">
            <?php echo $form->labelEx($model,'last_name'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->textField($model,'last_name',
                array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

            <?php echo $form->error($model,'last_name'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'title'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->textField($model,'title',
                array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

            <?php echo $form->error($model,'title'); ?>
        </div>


        <div class="field">
            <?php echo $form->labelEx($model,'email'); ?>
            <span class="add-on"><span class="icon-mail-reply"></span></span>
            <?php echo $form->textField($model,'email',
                array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>
            <?php echo $form->error($model,'email'); ?>
        </div>

    </div>

    <div class="eight wide column">
            <div class="field">
                <h3>Contact Info</h3>
            </div>

            <div class="field">
                <?php echo $form->labelEx($model,'work_phone'); ?>
                <span class="add-on"><span class="icon-phone"></span></span>
                <?php echo $form->textField($model,'work_phone',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

                <?php echo $form->error($model,'work_phone'); ?>
            </div>

            <div class="field">
                <?php echo $form->labelEx($model,'extension'); ?>
                <span class="add-on"><span class="icon-phone"></span></span>
                <?php echo $form->textField($model,'extension',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>

                <?php echo $form->error($model,'extension'); ?>
            </div>

            <div class="field">
                <?php echo $form->labelEx($model,'mobile_phone'); ?>
                <span class="add-on"><span class="icon-mobile-phone"></span></span>
                <?php echo $form->textField($model,'mobile_phone',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>
                <?php echo $form->error($model,'mobile_phone'); ?>
            </div>

            <div class="field">
                <?php echo $form->labelEx($model,'fax'); ?>
                <span class="add-on"><span class="icon-phone"></span></span>
                <?php echo $form->textField($model,'fax',
                    array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>
                <?php echo $form->error($model,'fax'); ?>
            </div>

        </div>

    </div>

<div class="ui grid abracadabra">
    <div class="eight wide column">
        <?php
        /* @var $form CActiveForm */

?>
        <div class="field">
            <h3>Other Info</h3>
        </div>


        <div class="field">
                      <?php echo $form->labelEx($model,'taxi_company_id'); ?>

                      <span class="add-on"><span class="icon-building"></span></span>
                      <?php
                      echo $form->dropDownList($model,'taxi_company_id', TaxiCompany::model()->getAsArray(),
                          array(
                              'options' => array($model->taxi_company_id=>array('selected'=>true)),
                              "id"=>"prependedInput", "class" => "span8",
                              'readonly' => $readonly,
                              'disabled' => $readonly,
                          )
                      ); ?>
                      <?php echo $form->error($model,'taxi_company_id'); ?>
             </div>

        <div class="field" style="color: #000000">
            <?php echo $form->labelEx($model,'role_id'); ?>
            <span class="add-on"><span class="icon-group"></span></span>
            <?php
                    $roles = Role::model()->getRoleDropdown();

                    echo $form->dropDownList($model, 'role_id', $roles,
                     array("id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly,
                         'disabled' => $readonly,
                             'options' => array($model->role => array('selected'=>'selected'))
                     ));


            ?>
        </div>


        <div class="field">
            <?php echo $form->labelEx($model,'active'); ?>

            <span class="add-on"><span class="icon-power-off"></span></span>
            <?php
            $yesno=array();
            $yesno["N"] = "N";
            $yesno["Y"] = "Y";
            echo $form->dropDownList($model,'active', $yesno,
                array(
                    'options' => array('Y'=>array('selected'=>true)),
                    "id"=>"prependedInput", "class" => "span8",
                    'readonly' => $readonly,
                    'disabled' => $readonly,
                )
            ); ?>

            <?php echo $form->error($model,'active'); ?>
        </div>
    </div>

    <div class="eight wide column">
        <div class="field">
            <h3>Extra Info</h3>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'notes'); ?>
            <span class="add-on"><span class="icon-info"></span></span>
            <?php echo $form->textArea($model,'notes',
                array('rows'=>8, 'cols'=>100, "id"=>"prependedInput", "class" => "span10", 'readonly' => $readonly)); ?>
            <?php echo $form->error($model,'notes'); ?>
        </div>

    </div>


</div>

<div class="field">
    <?php
    if (!$readonly)
        echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array(
            'class' => 'ui button', "id" => "button"));
    ?>
</div>


<?php $this->endWidget(); ?>

</div><!-- form -->

