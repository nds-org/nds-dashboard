<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $records Member */

$this->breadcrumbs=array(
	'Dashboard'=>array('/member/dashboard'),
	'List Users',
);

$this->menu=array(
    array('label'=>'Manage Users', 'url'=>array('admin')),
	array('label'=>'Create User', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('member-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Your Users</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<br>
<?php

$this->widget('application.components.tablesorter.Sorter', array(
    'data'=>$records,
    'columns'=>array(
        'role',
        'title',
        'username',
        'first_name',
        'last_name',
        'mobile_phone',
        'email',

          /*
		'dateregistered',
		'workPhone',
		'extension',
		'fax',
		'mobilePhone',
		'pager',
		'emergencyPhone',
		'email',
		'receiveEmails',
		'active',
		'wirelessEmail',
		'notes',
		*/
    )
));

?>
