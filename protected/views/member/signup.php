<?php
$this->pageTitle = Yii::app()->name . ' - Signup';
?>

<?php
$baseurl = Yii::app()->request->baseUrl;
?>

<?php
$this->widget( 'zii.widgets.CBreadcrumbs', array(
	'links' => $this->breadcrumbs,
) );
?>

<div class="ui page basic segment">

    <h1 class="ui header">
        Signup <small> for a New Account</small>
    </h1>


<?php
$form = $this->beginWidget( 'CActiveForm', array(
	'id' => 'signup-form',
	'enableClientValidation' => false,
	'clientOptions' => array(
		'validateOnSubmit' => true,
	),
	) );


$baseurl = Yii::app()->request->baseUrl;
?>

    <p><strong>Please fill out the following form with your account information:</strong></p>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="ui two column middle aligned relaxed grid basic segment">
		<div class="column">
            <div class="ui form segment">

				<div class="field">
					<label>Your First Name</label>
					<div class="ui left labeled icon input">
						<?php echo $form->textField( $model, 'first_name', array( "placeholder" => "First Name ..." ) );?>
						<i class="user icon"></i>
						<?php echo $form->error( $model, 'first_name' ); ?>
					</div>
				</div>

				<div class="field">
					<label>Your Last Name</label>
					<div class="ui left labeled icon input">
						<?php echo $form->textField( $model, 'last_name', array( "placeholder" => "Last Name ..." ) );?>
						<i class="user icon"></i>
						<?php echo $form->error( $model, 'last_name' ); ?>
					</div>
				</div>

				<div class="field">
					<label>Email Address</label>
					<div class="ui left labeled icon input">
						<?php echo $form->textField( $model, 'email', array( "placeholder" => "Email Address ..." ) );?>
						<i class="mail icon"></i>
						<div class="ui corner label">
							<i class="asterisk icon"></i>
						</div>
						<?php echo $form->error( $model, 'email' ); ?>
					</div>
				</div>

				<div class="field">
					<label>Select a Username</label>
					<div class="ui left labeled icon input">
						<?php echo $form->textField( $model, 'username', array( "placeholder" => "Username ..." ) );?>
						<i class="user icon"></i>
						<div class="ui corner label">
							<i class="asterisk icon"></i>
						</div>
						<?php echo $form->error( $model, 'username' ); ?>
					</div>
				</div>

				<div class="field">
					<label>Provide a Password</label>
					<div class="ui left labeled icon input">
						<?php echo $form->passwordField( $model, 'password', array( "placeholder" => "Password ..." ) );?>
						<i class="lock icon"></i>
						<div class="ui corner label">
							<i class="asterisk icon"></i>
						</div>
						<?php echo $form->error( $model, 'password' ); ?>
					</div>
				</div>

				<div class="field">
					<label>Confirm your Password</label>
					<div class="ui left labeled icon input">
						<?php echo $form->passwordField( $model, 'password2', array( "placeholder" => "Confirm Password ..." ) );?>
						<i class="lock icon"></i>
						<div class="ui corner label">
							<i class="asterisk icon"></i>
						</div>
						<?php echo $form->error( $model, 'password2' ); ?>
					</div>
				</div>

                <div class="field">
                    <label>Institute</label>
                    <div class="ui left labeled icon input">

                        <?php
                        $records = Institutes::model()->getAsArray();
                        echo $this->renderPartial("/widgets/dropdown", array(
                            "model" => $model,
                            "attribute" => "institute",
                            "records" => $records,
                            "id_attribute" => "institute",
                            "text_attribute" => "Choose Institute"
                            ));

                        ?>
                    </div>

                        <?php echo $form->error( $model, 'institute' ); ?>
                    </div>

                </div>

				<?php
				echo CHtml::submitButton( 'Signup', array(
					'class' => 'huge green ui labeled button', "id" => "button",
				) );
				?>

            </div>
		</div>

	</div>

	<?php echo $form->errorSummary( $model ); ?>

	<?php $this->endWidget(); ?>

    <br>
    <br>
    <br>

</div>



<!--    <p> If you have forgotten your password, please <a href="--><?php //echo $baseurl ?><!--/site/passwordreset">-->
<!--            reset your password</a> now.</p>-->

