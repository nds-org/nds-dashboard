<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $readonly boolean */

$this->breadcrumbs=array(
    'Dashboard'=>array('member/dashboard'),
    'Update User',
);

$this->menu=array(
    array('label'=>'Manage Users', 'url'=>array('admin')),
	array('label'=>'Create User', 'url'=>array('create')),
   	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->member_id),'confirm'=>'Are you sure you want to delete this member?')),
);

?>

<h1>Updating User <?php echo $model->getFullName(); ?></h1>

<?php

echo $this->renderPartial('_form', array(
    'model'=>$model,
    'readonly'=>false,
));
?>