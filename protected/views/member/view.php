<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $readonly boolean */

$this->breadcrumbs=array(
//    'Dashboard'=>array('member/dashboard', 'id'=>12),
    'Dashboard'=>array('member/dashboard'),
    'View User',
);

$this->menu=array(
    array('label'=>'Manage Users', 'url'=>array('admin')),
	array('label'=>'Create User', 'url'=>array('create')),
    array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->member_id)),
   	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->member_id),'confirm'=>'Are you sure you want to delete this member?')),
);

?>

<h1>Viewing User <?php echo $model->getFullName(); ?></h1>

<?php

echo $this->renderPartial('_form', array(
    'model'=>$model,
    'readonly'=>true,
));
?>