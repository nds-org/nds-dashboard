<?php

/** @var  $member Member */
$baseurl = Yii::app()->request->baseUrl;
$imageurl = $baseurl ."/images/";

$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

if ($android)
    $android = strpos($_SERVER['HTTP_USER_AGENT'],"Mobile");

$adjust_center = false;
if ((strpos($_SERVER['HTTP_USER_AGENT'],"Safari/533")) || (strpos($_SERVER['HTTP_USER_AGENT'],"Safari/534")))
    $adjust_center = true;

$old_android = false;
if ((strpos($_SERVER['HTTP_USER_AGENT'],"Android 2.")) || (strpos($_SERVER['HTTP_USER_AGENT'],"Android 3.")))
    $old_android = true;

//$ipod = true;

$cellphone = false;

if ($iphone || $android || $palmpre || $ipod || $berry == true)
    $cellphone = true;

echo Breadcrumbs::myBreadcrumbs(
    array(
        "Search Site" => "site/index",
        "Dashboard" => "",
        )
);

?>


<div class="ui page basic stackable grid">
    <div class="thirteen wide column">
        <h1 xmlns="http://www.w3.org/1999/html">Welcome to Your Dashboard, <?php echo $member->getFullName() ?></h1>

        <div class="ui aligned left basic header page segment">
             <?php
             if (!empty($member->institute))  {
                 $instobj = Institutes::model()->getByInstituteID($member->institute);
                 $inst = $instobj->name;
                 echo "
             <div class = 'ui large black label' style='margin-right: 10px'>
             Institute: </div>". $inst;
            echo "
            <div class = 'ui large black label' style='margin-right: 10px'>
             Role: </div>".ucfirst($member->role);
             }
            ?>

            <br><br>
                <a href="<?php echo StateVariables::getBaseUrl()?>/article/overview"> <span class="ui basic orange button"><i class="new file icon"></i>Create Article</span></a>
                <a href="<?php echo StateVariables::getBaseUrl()?>/article/admin"> <span class="ui basic orange button"><i class="list icon"></i>Your Articles</span></a>

        </div>
    </div>

    <div class="three wide column">
        <div class="ui segment">

            <p><a class="ui medium image" href="<?php echo $baseurl?>/member/profilepic" id="elem" rel="popover" data-placement="top" data-original-title="Click To Change Profile Picture">
            <img src="<?php echo $baseurl?>/member/mypicture" width="100%" style="width:100%">
            </a>
            </p>
            <div class="ui bottom attached label">
                <a href="<?php echo $baseurl?>/member/profile">
                Edit Profile
                </a>
            </div>
            </div>
    </div>
</div>

<div class="ui basic segment">
    <?php if ((!$cellphone) && (!$old_android)) { ?>
    <div class="ui middle aligned stackable grid">

        <div class="seven wide column">

            <div class="ui middle aligned stackable grid">
                <?php
                    $this->renderPartial("/widgets/gages", array());
                ?>

            </div>
        </div>
        <div class="three wide column">
        </div>
        <div class="six wide column">
        <?php
            $this->renderPartial("/widgets/upcoming", array(
                'a'=>null,
            ));
        ?>
        </div>
    </div>
    <?php } else if (!$old_android) {
    //looks like all cellphones are OK with 15 column grid on profile area.
    //Not making any adjustments for now.
    //Use $adjust_center if necessary...
    ?>
    <div class="ui three column grid">
        <div class="five wide column">
            <a href="<?php echo $baseurl.'/article/list'?>">
                <div id="g3"></div>
            </a>
        </div>
        <div class="five wide column">
            <a href="<?php echo $baseurl.'/article/list'?>">
                <div id="g1"></div>
            </a>
        </div>
        <div class="five wide column">
            <a href="<?php echo $baseurl.'/article/list'?>">
                <div id="g2"></div>
            </a>
        </div>
    </div>
    <div class="ui middle aligned grid">
        <div class="column">
        <?php
            $this->renderPartial("/widgets/upcoming", array(
                'a'=>null,
            ));
        ?>
        </div>
    </div>
    <?php } else { ?>
    <div class="ui middle aligned grid">
        <div class="column">
        <?php
            $this->renderPartial("/widgets/upcoming", array(
                'a'=>null,
            ));
        ?>
        </div>
    </div>
    <?php } ?>

</div>



<?php return; ?>

<div class="ui basic page grid">

    <div class="eight wide column">


        <h2 class="ui header">Publication Citations</h2>

        <div class="pubgraph"></div>
    </div>

    <div class="seven wide column">

        <h2 class="ui header">Cite Map</h2>
        
        <img src="<?php echo $imageurl.'citemap.gif'?>">
    </div>

</div>


<style>
     path.slice{
     	stroke-width:2px;
     }
     polyline{
     	opacity: .3;
     	stroke: black;
     	stroke-width: 2px;
     	fill: none;
     }
     svg text.percent{
     	fill:white;
     	text-anchor:middle;
     	font-size:12px;
     }
 </style>

 <script src="http://d3js.org/d3.v3.min.js"></script>

 <script>

     !function(){
     	var Donut3D={};

     	function pieTop(d, rx, ry, ir ){
     		if(d.endAngle - d.startAngle == 0 ) return "M 0 0";
     		var sx = rx*Math.cos(d.startAngle),
     			sy = ry*Math.sin(d.startAngle),
     			ex = rx*Math.cos(d.endAngle),
     			ey = ry*Math.sin(d.endAngle);

     		var ret =[];
     		ret.push("M",sx,sy,"A",rx,ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0),"1",ex,ey,"L",ir*ex,ir*ey);
     		ret.push("A",ir*rx,ir*ry,"0",(d.endAngle-d.startAngle > Math.PI? 1: 0), "0",ir*sx,ir*sy,"z");
     		return ret.join(" ");
     	}

     	function pieOuter(d, rx, ry, h ){
     		var startAngle = (d.startAngle > Math.PI ? Math.PI : d.startAngle);
     		var endAngle = (d.endAngle > Math.PI ? Math.PI : d.endAngle);

     		var sx = rx*Math.cos(startAngle),
     			sy = ry*Math.sin(startAngle),
     			ex = rx*Math.cos(endAngle),
     			ey = ry*Math.sin(endAngle);

     			var ret =[];
     			ret.push("M",sx,h+sy,"A",rx,ry,"0 0 1",ex,h+ey,"L",ex,ey,"A",rx,ry,"0 0 0",sx,sy,"z");
     			return ret.join(" ");
     	}

     	function pieInner(d, rx, ry, h, ir ){
     		var startAngle = (d.startAngle < Math.PI ? Math.PI : d.startAngle);
     		var endAngle = (d.endAngle < Math.PI ? Math.PI : d.endAngle);

     		var sx = ir*rx*Math.cos(startAngle),
     			sy = ir*ry*Math.sin(startAngle),
     			ex = ir*rx*Math.cos(endAngle),
     			ey = ir*ry*Math.sin(endAngle);

     			var ret =[];
     			ret.push("M",sx, sy,"A",ir*rx,ir*ry,"0 0 1",ex,ey, "L",ex,h+ey,"A",ir*rx, ir*ry,"0 0 0",sx,h+sy,"z");
     			return ret.join(" ");
     	}

     	function getPercent(d){
     		return (d.endAngle-d.startAngle > 0.2 ?
     				Math.round(1000*(d.endAngle-d.startAngle)/(Math.PI*2))/10+'%' : '');
     	}

     	Donut3D.transition = function(id, data, rx, ry, h, ir){
     		function arcTweenInner(a) {
     		  var i = d3.interpolate(this._current, a);
     		  this._current = i(0);
     		  return function(t) { return pieInner(i(t), rx+0.5, ry+0.5, h, ir);  };
     		}
     		function arcTweenTop(a) {
     		  var i = d3.interpolate(this._current, a);
     		  this._current = i(0);
     		  return function(t) { return pieTop(i(t), rx, ry, ir);  };
     		}
     		function arcTweenOuter(a) {
     		  var i = d3.interpolate(this._current, a);
     		  this._current = i(0);
     		  return function(t) { return pieOuter(i(t), rx-.5, ry-.5, h);  };
     		}
     		function textTweenX(a) {
     		  var i = d3.interpolate(this._current, a);
     		  this._current = i(0);
     		  return function(t) { return 0.6*rx*Math.cos(0.5*(i(t).startAngle+i(t).endAngle));  };
     		}
     		function textTweenY(a) {
     		  var i = d3.interpolate(this._current, a);
     		  this._current = i(0);
     		  return function(t) { return 0.6*rx*Math.sin(0.5*(i(t).startAngle+i(t).endAngle));  };
     		}

     		var _data = d3.layout.pie().sort(null).value(function(d) {return d.value;})(data);

     		d3.select("#"+id).selectAll(".innerSlice").data(_data)
     			.transition().duration(750).attrTween("d", arcTweenInner);

     		d3.select("#"+id).selectAll(".topSlice").data(_data)
     			.transition().duration(750).attrTween("d", arcTweenTop);

     		d3.select("#"+id).selectAll(".outerSlice").data(_data)
     			.transition().duration(750).attrTween("d", arcTweenOuter);

     		d3.select("#"+id).selectAll(".percent").data(_data).transition().duration(750)
     			.attrTween("x",textTweenX).attrTween("y",textTweenY).text(getPercent);
     	}

     	Donut3D.draw=function(id, data, x /*center x*/, y/*center y*/,
     			rx/*radius x*/, ry/*radius y*/, h/*height*/, ir/*inner radius*/){

     		var _data = d3.layout.pie().sort(null).value(function(d) {return d.value;})(data);

     		var slices = d3.select("#"+id).append("g").attr("transform", "translate(" + x + "," + y + ")")
     			.attr("class", "slices");

     		slices.selectAll(".innerSlice").data(_data).enter().append("path").attr("class", "innerSlice")
     			.style("fill", function(d) { return d3.hsl(d.data.color).darker(0.7); })
     			.attr("d",function(d){ return pieInner(d, rx+0.5,ry+0.5, h, ir);})
     			.each(function(d){this._current=d;});

     		slices.selectAll(".topSlice").data(_data).enter().append("path").attr("class", "topSlice")
     			.style("fill", function(d) { return d.data.color; })
     			.style("stroke", function(d) { return d.data.color; })
     			.attr("d",function(d){ return pieTop(d, rx, ry, ir);})
     			.each(function(d){this._current=d;});

     		slices.selectAll(".outerSlice").data(_data).enter().append("path").attr("class", "outerSlice")
     			.style("fill", function(d) { return d3.hsl(d.data.color).darker(0.7); })
     			.attr("d",function(d){ return pieOuter(d, rx-.5,ry-.5, h);})
     			.each(function(d){this._current=d;});

     		slices.selectAll(".percent").data(_data).enter().append("text").attr("class", "percent")
     			.attr("x",function(d){ return 0.6*rx*Math.cos(0.5*(d.startAngle+d.endAngle));})
     			.attr("y",function(d){ return 0.6*ry*Math.sin(0.5*(d.startAngle+d.endAngle));})
     			.text(getPercent).each(function(d){this._current=d;});
     	}

     	this.Donut3D = Donut3D;
     }();

 </script>

<script>

var salesData=[
  {label:"Big Data Paradigms", color:"#3366CC"},
  {label:"Workflow Survey", color:"#DC3912"},
  {label:"Attic Data", color:"#FF9900"},
  {label:"General Approach for Distributed Data Fusion", color:"#109618"},
  {label:"Another Paper", color:"#990099"}
];

var svg = d3.select(".pubgraph").append("svg").attr("width",700).attr("height",300);

svg.append("g").attr("id","publications");

Donut3D.draw("publications", randomData(), 150, 150, 130, 100, 30, 0.4);

function changeData(){
  Donut3D.transition("publications", randomData(), 130, 100, 30, 0.4);
}

function randomData(){
  return salesData.map(function(d){
      return {label:d.label, value:1000*Math.random(), color:d.color};});
}
</script>