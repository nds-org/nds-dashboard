

<style>

    div.custom_file_upload {
        width: 230px;
        height: 30px;
        margin: 20px auto;
    }

    input.file {
        width: 160px;
        height: 34px;
        border: 1px solid #BBB;
        border-right: 0;
        color: #888;
        padding: 5px;

        -webkit-border-top-left-radius: 5px;
        -webkit-border-bottom-left-radius: 5px;
        -moz-border-radius-topleft: 5px;
        -moz-border-radius-bottomleft: 5px;
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;

        outline: none;
    }

    div.file_upload {
        width: 80px;
        height: 34px;
        background-color: #4bb1cf;
        background-image: -moz-linear-gradient(top, #5bc0de, #339bb9);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#339bb9));
        background-image: -webkit-linear-gradient(top, #5bc0de, #339bb9);
        background-image: -o-linear-gradient(top, #5bc0de, #339bb9);
        background-image: linear-gradient(to bottom, #5bc0de, #339bb9);
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff339bb9', GradientType=0);

        display: inline;
        position: absolute;
        overflow: hidden;
        cursor: pointer;

        -webkit-border-top-right-radius: 5px;
        -webkit-border-bottom-right-radius: 5px;
        -moz-border-radius-topright: 5px;
        -moz-border-radius-bottomright: 5px;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;

        font-weight: bold;
        color: #FFF;
        text-align: center;
        padding: 8px;
    }
    div.file_upload:before {
        content: 'Select';
        position: absolute;
        left: 0; right: 0;
        text-align: center;

        cursor: pointer;
    }

    div.file_upload input {
        position: relative;
        height: 40px;
        width: 250px;
        display: inline;
        cursor: pointer;
        opacity: 0;
    }
</style>

<?php

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScript("getFile",  '

$("#upload").change(function(e){
    var val=$(this).val();

    var filesplit = val.split("\\\");

    if (filesplit.length > 0)
        filename = filesplit.pop();
    else
        filaname = val;

    $("#filetext").val(filename);
});

   $("#fileuploadform").on("submit", function(e) {
        var val = $("#filetext").val();
        if (val == "") {
            e.preventDefault();
            alert("You must first select a file to change your profile picture!!");
            return false;
            }
      });
');

?>
<div class="ui segment">
    <h1>Profile Picture</h1>

    Please select your profile picture using the file browser below.
</div>


<div class="ui segment">

        <form enctype="multipart/form-data" id="fileuploadform" class="well form-horizontal" action="upload" method="post">

            <div class="custom_file_upload">
                <input type="hidden" name="MAX_FILE_SIZE" value="3000000" />
                <input type="text" class="file" name="info" id="filetext">
                <div class="file_upload">
                    <input type="file" id="upload" name="filename">
                </div>
            </div>

            <div style="margin:auto;">
                <input class="ui large green button" type="submit" value="Set"  />
            </div>
        </form>
</div>