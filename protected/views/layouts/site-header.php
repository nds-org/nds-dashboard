<?php

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$isGuest = StateVariables::isGuest();

$baseurl = Yii::app()->request->baseUrl;

$adjust_header = false;
if ((strpos($_SERVER['HTTP_USER_AGENT'],"Safari/533")) || (strpos($_SERVER['HTTP_USER_AGENT'],"Safari/534")))
    $adjust_header = true;

$adjust_logo = false;

$URL = StateVariables::getSiteURL();

if ((StateVariables::isWhiteLabel()) && ($URL != 'http://easyworkorder.com'))
    $adjust_logo = true;

if ($isGuest)
    $logo_url = $baseurl;
else
    $logo_url = $baseurl."/dashboard/home";

?>

<div class="ui grid masthead segment">

    <div class="computer only two wide column">
        <div class="ui medium right attached launch button">
          <i class="large content icon"></i>
            <span class="text">Menu</span>
        </div>
    </div>

    <div class="tablet only two wide column">
        <div class="ui medium right attached launch button">
          <i class="large content icon"></i>
            <span class="text">Menu</span>
        </div>
    </div>

    <div class="mobile only three wide column">
        <div class="ui medium right attached launch button">
          <i class="large content icon"></i>
          <span class="text">Menu</span>
        </div>
    </div>

    <div class="computer only four wide column">
           <a href="<?php echo $logo_url?>">
               <div class="nds logo">
               </div>
           </a>
    </div>

     <div class="tablet only three wide column">
            <a href="<?php echo $logo_url?>">
                <div class="nds logo">
                </div>
            </a>
     </div>

    <div class="mobile only four wide column">
        <div style="margin-left: 4px">
            <a href="<?php echo $logo_url?>">
                <div class="nds logo">
                </div>
            </a>
        </div>
    </div>



    <div class="computer only six wide column">
        <div class="nds slogan">search <i class="search icon"></i> publish <i class="world icon"></i>  link <i class="linkify icon"></i>  reuse <i class="exchange icon"></i> </div>

    </div>

    <div class="tablet only six wide column">
        <div class="nds slogan">search <i class="search icon"></i> publish <i class="world icon"></i>  link <i class="linkify icon"></i>  reuse <i class="exchange icon"></i> </div>
    </div>

    <div class="mobile only one wide column">
    </div>

    <?php
    if (!$isGuest)
         $this->renderPartial("/widgets/alertdropdowns", array());
     ?>
</div>

