<?php
/**
 * This is used for the email mainly because it does not choose a default style ... So, this pulls in the email template and
 * then pulls in the content.
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;" />

<style type="text/css">
    body {
        font-family: "Helvetica Neue", Arial, Helvetica, sans-serif;
        font-size: 12pt;
     }
</style>

<body>

<?php echo $content; ?>

</body>

</html>