<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php
/* @var $this Controller */

$baseurl = Yii::app()->request->baseUrl;
$imageurl = $baseurl ."/images/";

/** @var  $cs CClientScript */
$cs = Yii::app()->getClientScript();

$cs->scriptMap=array(
    'jquery.js'=>false,
);

?>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!-- Standard Meta -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <script>
        (function () {
            var
                eventSupport = ('querySelector' in document && 'addEventListener' in window)
            jsonSupport  = (typeof JSON !== 'undefined'),
                jQuery       = (eventSupport && jsonSupport)
                    ? '<?php echo $baseurl?>/js/jquery.min.js'
                    : '<?php echo $baseurl?>/js/jquery.legacy.min.js'
            ;
            document.write('<script src="' + jQuery + '"><\/script>');
        }());
    </script>

    <title>National Data Service</title>

    <link rel="icon"
          type="image/png"
          href="<?php echo $baseurl?>/images/logo-icon.png">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700|Open+Sans:300italic,400,300,700' rel='stylesheet' type='text/css'>


    <!--    <script src="--><?php //echo $baseurl?><!--/js/spin.min.js"></script>-->
    <!--    <script src="--><?php //echo $baseurl?><!--/js/spinimpl.js"></script>-->

    <link rel="stylesheet" type="text/css" href="<?php echo $baseurl?>/vendor/semantic/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $baseurl?>/css/custom.css">

</head>
<body>


<?php
$this->renderPartial("/layouts/site-header");
$this->renderPartial("/menu/main-menu");
?>

<style>
    #blackout {
        width:100%;
        height:200%; /* make sure you have set parents to a height of 100% too*/
        position: absolute;
        left:0; top:0;
        z-index:20000 /*just to make sure its on top*/}
</style>

<div id="blackout" style="background-color: rgba(0,0,0,0.5); display:none;">
    <div id="spinner">
    </div>
</div>

<div class="ui page" style="padding-bottom: 10px">

    <?php echo $content; ?>

</div>

<script src="<?php echo $baseurl?>/js/jquery.address.js"></script>

<script src="<?php echo $baseurl?>/vendor/semantic/semantic.js"></script>

<?php $this->renderPartial("/menu/footer");
?>

</html>
