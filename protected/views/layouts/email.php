<?php
/**
 * This is used for the email mainly because it does not choose a default style ... So, this pulls in the email template and
 * then pulls in the content.
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;" />

<style type="text/css">
    body {
        font-family: "Helvetica Neue", Arial, Helvetica, sans-serif;
        font-size: 12pt;
        line-height:16px;
        text-align:justify;
     }
</style>

<body>

<?php


/** @var  $controller CController */

$controller = Yii::app()->controller;

$baseAbsURL = $controller->createAbsoluteUrl("/");
$baseAbsURL = str_replace("https", "http", $baseAbsURL);

$imagesURL = $baseAbsURL.'/images';

$companyName = Yii::app()->params['name'];

?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <div>
                <?php echo $content; ?>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <br>
            <strong>Delivered by: </strong><br>
            <br>
        </td>
    </tr>
    <tr>
        <td height="0" align="left" valign="top" class="header-top">
            <a href="<?php echo $baseAbsURL ?>"><img src="<?php echo $imagesURL.'/logo.png' ?>" alt="" border="0" title="<?php echo $companyName ?>.com "></a>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" style="padding:16px 0 25px 0;line-height:17px; background-color:inherit; text-align:center;">
            Copyright &copy; 2013 <a href="<?php echo $baseAbsURL ?>" title="<?php echo $companyName ?>"><?php echo $companyName ?></a>, All rights reserved.   <br>
        </td>
    </tr>
</table>

</body>

</html>
