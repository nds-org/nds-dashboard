<?php
/* @var $this ArticletypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Article Types',
);

$this->menu=array(
	array('label'=>'Create ArticleType', 'url'=>array('create')),
	array('label'=>'Manage ArticleType', 'url'=>array('admin')),
);
?>

<h1>Article Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
