<?php
/* @var $this ArticletypeController */
/* @var $model ArticleType */

$this->breadcrumbs=array(
	'Article Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ArticleType', 'url'=>array('index')),
	array('label'=>'Manage ArticleType', 'url'=>array('admin')),
);
?>

<h1>Create ArticleType</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>