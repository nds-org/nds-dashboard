<?php
/* @var $this ArticletypeController */
/* @var $model ArticleType */

$this->breadcrumbs=array(
	'Article Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ArticleType', 'url'=>array('index')),
	array('label'=>'Create ArticleType', 'url'=>array('create')),
	array('label'=>'View ArticleType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ArticleType', 'url'=>array('admin')),
);
?>

<h1>Update ArticleType <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>