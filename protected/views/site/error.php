<?php

if (!isset($message))
    $message = "Sorry, unknown error";
?>

<div class="ui header"><span class="frown"></span> Oops, Something went wrong ... </div>

    <div class="ui ignored red message">
        <?php
        echo '<strong>Message </strong>'.CHtml::encode($message);
        ?>
    </div>

    <div class="ui ignored red message">
        <?php
        echo '<strong>Request from </strong>'.
            Yii::app()->urlManager->parseUrl(Yii::app()->request)
        ?>
    </div>
