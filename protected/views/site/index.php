<?php

/** @var $error string */

/* @var $records array*/

if (!empty($error)) {
    $err = true;
    $search = true;
} else
    $err = false;

if (isset($records)) {
    $search = true;
    $articleView = new AccordionTable("Results", $records, count($records));

    if (count($records) == 0) {
        $err = true;
        $error = "Sorry, no matches were found ...";
    }
} else
    $search = false;

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$baseurl = Yii::app()->request->baseUrl;

$cs->registerScript('search-string', '


    $("#search-string").height(50);

    $(".orange.submit.search.button").on("click", function() {
        $("#search-form").submit();
        });

    $(".blue.submit.search.button").on("click", function() {
        $("#search-form").append(\'<input type="hidden" name="lucky" value="yes!" /> \');
        $("#search-form").submit();
    });

', CClientScript::POS_END);

$imageurl = $baseurl ."/images/";



?>
<br>
<div class="ui large icon inverted orange message">
    <i class="dashboard icon"></i>
    <div class="content">
        <div class="massive header" style="color: #ffffff;;">
            What is the NDS Dashboard?
        </div>
        <p>The NDS dashboard is a prototype researcher dashboard. It enables
            researchers to repeat experiments, containing data and methods, by providing
            Web-based access to support the lifecycle of NDS Labs Docker containers.</p>
    </div>
</div>

<form id="search-form" action="<?php echo $baseurl?>/site/index"  method="POST">
    <div class="ui basic segment">
        <div class="ui error form">
            <div class="nds search">
                  <div class="field">
                      <h1 class="ui header nds color">Search</h1>
                      <b>
                      <?php
                          if ($err) {
                      ?>

                      <div class="ui error message">
                         <div class="header">Sorry...</div>
                         <p><?php echo $error ?></p>
                       </div>
                      <?php }?>


                      <div class="ui huge icon input">
                        <input id="search-string" type="text" placeholder="Find Publications..." name="search">
                          <i class="search icon"></i>
                      </div>
                      <br>
                      <br>
                      <br>
                      <br>
                      <div class="ui huge buttons">
                        <div class="ui orange submit search button">Search
                            <i class="search icon"></i>
                        </div>
                        <div class="or"></div>
                        <div class="ui blue submit search button">
                            <i class="magic icon"></i>
                            Feeling Lucky
                        </div>
                      </div>
                  </div>
            </div>
        </div>
    </div>
</form>

<?php

if (($search) && (!$err)) {
    echo $articleView->getWidget();
}
?>

<div class="ui horizontal divider">
    What is the NDS
</div>

<div class="ui large icon inverted orange message">
    <i class="info icon"></i>
    <div class="content">
        <div class="massive header" style="color: #ffffff;;">
            What is the NDS?
        </div>
        <p>The National Data Service (NDS) is an emerging vision for how scientists and researchers across all disciplines can find, reuse, and publish data. It builds on the data archiving and sharing efforts already underway within specific communities and links them together with a common set of tools designed around the following capabilities:</p>
    </div>
</div>


<div class="ui stackable grid">
    <div class="four wide column">
        <div class="ui inverted teal segment">
            <a class="ui bottom right attached inverted label">
                <h2>1</h2>
            </a>

            <h2><em><i class="large search icon"></i></em>
                <span>Search</span></h2>
            <p>The NDS will allow users to easily search for data across disciplinary boundaries. As users hone in on data of interest, they can easily switch to discipline-specific tools.</p>
            <br><br>
        </div>
    </div>
    <div class="four wide column">
        <div class="ui inverted orange segment">
            <a class="ui bottom right attached inverted label">
                <h2>2</h2>
            </a>

            <h2><em><i class="large world icon"></i></em>
                <span>Publish</span></h2>
            <p>The NDS will connect users to tools for building and sharing collections of data. It will help users find and deliver data to the best repository for data-publishing.</p>
        </div>
    </div>
    <div class="four wide column">
        <div class="ui inverted green segment">
            <a class="ui bottom right attached inverted label">
                <h2>3</h2>
            </a>

            <h2><em><i class="large linkify icon"></i></em>
                <span>Link</span></h2>
            <p>The NDS will create robust connections between data and published articles.  When researchers reference an article, they have ready access to the underlying data.</p>
        </div>
    </div>
    <div class="four wide column">
        <div class="ui inverted purple segment">
            <a class="ui bottom right attached inverted label">
                <h2>4</h2>
            </a>

            <h2><em><i class="large exchange icon"></i></em>
                <span>Reuse</span></h2>
            <p>The NDS will not only provide access to data for download, it will provide tools for transferring data to processing platforms or allow analysis to be attached to the data.</p>
        </div>
    </div>
</div>

<div class="ui horizontal divider">
    Learn More
</div>



<div class="ui basic stackable grid segment">

    <div class="ui eight wide column">
        <iframe width="420" height="266" src="https://www.youtube.com/embed/BPT1FNFAvnc" frameborder="0" allowfullscreen></iframe>
    </div>

    <div class="ui eight wide column">
        <div class="ui message" style="font-size:160%; background-color: #E84D1C; color: #ffffff;;">
            WHAT IS THE NDS
        </div>
        <div class="ui message">
            Watch this video to learn about the revolutionary services
            taking shape at the National Data Service.
        </div>

        <a class="ui blue button" href="http://www.nationaldataservice.org/about/" target="_blank">
            READ MORE ABOUT THE NDS! <i class="right arrow icon"></i>
        </a>

    </div>
</div>



<?php
return;

if (!$err) {
?>


    <style>
    	.state{
    		fill: none;
    		stroke: #a9a9a9;
    		stroke-width: 1;
    	}
    	.state:hover{
    		fill-opacity:0.5;
    	}
    	#tooltip {
    		position: absolute;
    		text-align: center;
    		padding: 20px;
    		margin: 10px;
    		font: 12px sans-serif;
    		background: lightsteelblue;
    		border: 1px;
    		border-radius: 2px;
    		pointer-events: none;
    	}
    	#tooltip h4{
    		margin:0;
    		font-size:14px;
    	}
    	#tooltip{
    		background:rgba(0,0,0,0.9);
    		border:1px solid grey;
    		border-radius:5px;
    		font-size:12px;
    		width:auto;
    		padding:4px;
    		color:white;
    		opacity:0;
    	}
    	#tooltip table{
    		table-layout:fixed;
    	}
    	#tooltip tr td{
    		padding:0;
    		margin:0;
    	}
    	#tooltip tr td:nth-child(1){
    		width:50px;
    	}
    	#tooltip tr td:nth-child(2){
    		text-align:center;
    	}
    </style>

    <div id="tooltip"></div><!-- div to hold tooltip. -->

    <div style="margin-left: 100px;">
        <svg width="960" height="600" id="statesvg"></svg> <!-- svg to hold the map. -->

    </div>


    <script src="http://bl.ocks.org/NPashaP/raw/a74faf20b492ad377312/uStates.js"></script> <!-- creates uStates. -->
    <script src="http://d3js.org/d3.v3.min.js"></script>
    <script>

    	function tooltipHtml(n, d){	/* function to create html content string in tooltip div. */
    		return "<h4>"+n+"</h4><table>"+
    			"<tr><td>Low</td><td>"+(d.low)+"</td></tr>"+
    			"<tr><td>Average</td><td>"+(d.avg)+"</td></tr>"+
    			"<tr><td>High</td><td>"+(d.high)+"</td></tr>"+
    			"</table>";
    	}

    	var sampleData ={};	/* Sample random data. */
    	["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
    	"ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH",
    	"MI", "WY", "MT", "ID", "WA", "DC", "TX", "CA", "AZ", "NV", "UT",
    	"CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN",
    	"WI", "MO", "AR", "OK", "KS", "LS", "VA"]
    		.forEach(function(d){
    			var low=Math.round(100*Math.random()),
    				mid=Math.round(100*Math.random()),
    				high=Math.round(100*Math.random());
    			sampleData[d]={low:d3.min([low,mid,high]), high:d3.max([low,mid,high]),
    					avg:Math.round((low+mid+high)/3), color:d3.interpolate("#ffffcc", "#800026")(low/100)};
    		});

    	/* draw states on id #statesvg */
    	uStates.draw("#statesvg", sampleData, tooltipHtml);
    </script>


    <div class="ui fluid accordion">
        <div class="title">
          <i class="info letter icon"></i>
          Learn More
        </div>
        <div class="content">
            <div class="ui stacked segment">
                  <h1>THE NATIONAL DATA SERVICE</h1>
                  <p>The National Data Service is an emerging vision of how scientists and researchers across all disciplines can find, reuse, and publish data. It is an international federation of data providers, data aggregators, community-specific federations, publishers, and cyberinfrastructure providers. It builds on the data archiving and sharing efforts under way within specific communities and links them together with a common set of tools.</p>
              </div>

            <div class="ui stacked segment">
                  <h1>THE VISION</h1>
                  <p>It is widely believed that ubiquitous digital information will transform the very nature of research and education. The reasons for this excitement are clear: In essentially every field of science, simulations, experiments, instruments, observations, sensors, and/or surveys are generating exponentially growing data volumes. Information from different sources and fields can be combined to permit new modes of discovery. Data, including critical metadata and associated software models, can capture the precise scientific content of the processes that generated them, permitting analysis, reuse, and reproducibility. By digitizing communication among scientists and citizens, discoverable and shareable data can enable collaboration and support repurposing for new discoveries and cross-disciplinary research enabled by data sharing across communities. Open, shareable data also promise to transform education, society, and economic development.</p>
              </div>
        </div>
      </div>

<?php
}
?>

    <br>
    <br>



