<?php
/* @var $this ResourceController */
/* @var $model Resource */

$content_type = ContentType::model()->getByContentTypeID($model->content_type_id);
$article = Resource::model()->getArticleForResourceID($model->id);
$article_id = $article->id;

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
            "Article #".$model->id => "article/".$article_id,
                    " Run ".$model->title => "")
);

$cs = Yii::app()->getClientScript();
/** @var $cs CClientScript */

$cs->registerScriptFile(Pages::baseURL()."js/runscript.js");


$cs->registerScript("get-progress", '

function openTab(data) {
    // for Jupyter but non functional now.
    var win = window.open("http://labs.nationaldataservice.org/py/" + data,"_blank");
    win.focus();
}

function errorDialog(titleInfo, statusInfo) {
    $(".errorlog.status").html(statusInfo);
    $(".errorlog.title").html(titleInfo);
    $(".ui.dropdown").dropdown();
    $(".error.modal").modal("setting", {
        onApprove : function() {
        return true;
        }
    }).modal("show");
};


    getProgress("'.Pages::getAbsoluteBaseURL().'","'.$model->id.'");

    $( document ).ready(function() {
        $( ".button.start" ).on( "click", function() {

           // alert();
           $(".script-state").html("Request Sent");
           $(".script-terminal").html("Awaiting Script Output");

           var dataset = $(".dataset").val();
           var container = $(".container").val();

           //alert(dataset);

           var mode = $(".run-choice").val();

           if (dataset !== "")
                dataset = "?data=" + encodeURI(dataset);

           if (container !== "")
                container = "?container=" + encodeURI(container);

            var url = "'.Pages::getAbsoluteBaseURL().'" + "resource/start/resource_id/" + '.$model->id.' + dataset;

           // alert(url);
           // exit;

            $.get(url, function(data) {
                alert(data);
                return;
//                if (data.indexOf("Request Failed!") == 0) {
//                    errorDialog("Oops, something went wrong...", data);
//                } else {
//                    //errorDialog("Launching iPython Notebook", "Click ok and iPython Notebook will be launched after a few seconds...");
//                    if (mode == "notebook") {
//                       setTimeout(function() {
//                            openTab(data);
//                        }, 5000);
//                    }
//                }
                });
        });

           $( ".button.reset" ).on( "click", function() {
           $(".script-state").html("Reset Backend");
           $(".script-terminal").html("Awaiting Instruction");

            $.get("'.Pages::getAbsoluteBaseURL().'" + "resource/reset/resource_id/" + '.$model->id.', function(data) {
                // alert(data);
                if (data == "failed") {
                    // what should we do ???
                    }
                });
        });

        $( ".button.download" ).on( "click", function() {
           $(".script-state").html("Downloading Results");
            var url = "'.Pages::getAbsoluteBaseURL().'" + "resource/download/resource_id/" + "'.$model->id.'";
            window.location = url;
            });

    });

', CClientScript::POS_BEGIN);


?>


<div class="ui inverted stacked divided overview segment">

    <div class="ui stackable grid">
        <div class="ui five wide column">

            <div class="ui huge blue ribbon label">Execution Dashboard For <?php echo $model->getFileName() ?></div>
            <br><div style="padding-top: 5px"></div>

            <div class="ui red icon button start" style="margin-bottom: 5px;"> Run <i class="play icon"></i></div>
            <div class="ui purple icon button reset" style="margin-bottom: 5px;"> Reset <i class="eject icon"></i></div>
            <div class="ui green icon button download" style="margin-bottom: 5px; display: none"> Download Results <i class="cloud download icon"></i></div>
        </div>

        <div class="ui right aligned five wide column">
            <b class="ui huge red header" style="margin-top: 5px;margin-right: 10px">Data Set</b> <?php echo $this->actionListdirs($article_id); ?>
            <br><div style="padding-top: 5px"></div>
<!--            <b class="ui huge red header" style="margin-top: 5px;margin-right: 10px">Container</b>--><?php //echo $this->actionContainers(); ?>
        </div>


        <div class="ui six wide column">
            <b class="ui huge red header" style="margin-top: 5px;margin-right: 10px">Mode</b> <?php echo $this->getModes(); ?>
            <br><div style="padding-top: 5px"></div>
            <i class="ui huge red header" style="margin-top: 5px;">Status </i>
            <div class="ui huge blue label start run" style="padding: 8px; font-size: 1.2em;">
                <div class="script-state">Ready</div>
            </div>
        </div>

    </div>
</div>

<div class="ui stacked segment">
    <div class="ui huge header">Script Command Line Output</div>

    <textarea cols="120" rows="30" name="results" class="retrotextarea script-terminal">
    </textarea>

</div>

<div class="ui error modal">
  <i class="close icon"></i>
  <div class="header">
    <a class="ui red huge label errorlog title">
        </a>
  </div>
  <div class="content">
    <a class="ui large red label">Reason</a>
      <i class="ui basic segment errorlog status" style="text-transform: uppercase;"></i>

  </div>
  <div class="actions">
      <div class="ui positive right labeled icon button">
        Ok
        <i class="checkmark icon"></i>
      </div>
  </div>
</div>
