<?php
/* @var $this ResourceController */
/* @var $model Resource */

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
            "Your Data" => "resource/admin",
        "Update ".$model->id => "")
);

?>

<h1>Update Resource <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

