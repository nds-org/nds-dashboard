<?php
/* @var $this ArticleController */
/* @var $model Article */

$resources =  Article::model()->getResourcesForAllMemberArticles($this->getMemberID());

$articleResources = new AccordionTable("All Data", $resources, count($resources));
$member = Member::model()->getByMemberID(Yii::app()->user->id);

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
          "Your Data Items" => "")
);

?>

<div class="ui page basic segment">

    <div class="ui purple stacked segment">
        <div class="ui large black header"><?php echo $member->getFullName()?>'s Data</div>
    </div>

    <?php
    echo $articleResources->getWidget();
    ?>

</div>
