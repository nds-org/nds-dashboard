<?php
/* @var $this ResourceController */
/* @var $model Resource */
/* @var $form CActiveForm */
/* @var $resource Resource */
/* @var $article_id int */

if (!isset($form))
    $has_form = false;
else
    $has_form = true;


if (!isset($resource)) {
    $resource = $model;
}

$article = Resource::model()->getArticleForResourceID($resource->id);

if (empty($article)) {
    // one last attempt
    $article = Article::model()->getByArticleID($article_id);
}

$resource_id = "Resource[".$resource->id."]";


if (!$has_form) {
    $resource_id = "Resource";
?>



    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'resource-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
    )); ?>

    <?php
    }
    ?>

<div class="ui form">

        <div class="ui inverted purple fluid label"><?php echo $resource->getFileName() ?></div>
        <br>
        <br>

        <div class="field">
            <?php echo $form->labelEx($model,'title'); ?>
            <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>500,
                "name" => $resource_id."[title]", "id" => $resource_id."_title")); ?>
            <?php echo $form->error($model,'title'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'content_type_id'); ?>
            <?php echo SemanticWidgets::getDropDown($model,'content_type_id',
                ContentType::model()->getAsArray(),
                $resource_id."[content_type_id]", $resource_id."_content_type_id"); ?>
            <?php echo $form->error($model,'content_type_id'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'description'); ?>
            <?php echo $form->textArea($model,'description',array('size'=>60,'maxlength'=>5000,
                "name" => $resource_id."[description]", "id" => $resource_id."_description")); ?>
            <?php echo $form->error($model,'description'); ?>
        </div>

    <?php
    if (!$has_form) {
    ?>

    <?php echo CHtml::htmlButton("Update<i class='right arrow icon'></i>", array(
              'encode' => true,
              'class' => 'ui button', "id" => "button",
              'type' => 'submit'
     		    ));?>

    <?php $this->endWidget(); ?>
</div>

<?php
}
?>
