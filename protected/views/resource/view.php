<?php
/* @var $this ResourceController */
/* @var $model Resource */

echo Breadcrumbs::myBreadcrumbs(
    array("Dashboard" => "member/dashboard",
            "Your Data" => "resource/admin",
        $model->id." Data View" => "")
);

$article = Article::model()->getByArticleID($model->article_id);

?>

<div class="ui red stacked segment">
    <div class="ui grid">
        <div class="eight wide column">
            <div class="ui large black header">Details for <?php echo $model->getFileName() ?></div>
        </div>
        <div class="ui aligned right aligned eight wide column">
            <a href="<?php echo StateVariables::getBaseUrl()?>/article/view/id/<?php echo $article->id ?>"> <span class="ui primary button"><i class="edit icon"></i>Article Overview</span></a>
        </div>
    </div>
</div>


<div class="ui grid">
     <div class="six wide column">
         <?php echo $model->getDescriptionLeft() ?>
     </div>
    <div class="ten wide column">
        <?php echo  $model->getDescriptionRight() ?>
     </div>
 </div>