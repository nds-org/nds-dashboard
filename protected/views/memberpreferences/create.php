<?php
/* @var $this MemberpreferencesController */
/* @var $model MemberPreferences */
/* @var $form CActiveForm */
/** @var $readonly bool */

?>

<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'member-preferences-form',
        'enableAjaxValidation'=>false,
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

<h1>Create MemberPreferences</h1>

<?php echo $this->renderPartial('/memberpreferences/_form', array(
    'model'=>$model,
    'form'=>$form,
    'readonly'=>$readonly
)); ?>

    <br>

    <div class="row-fluid">
        <?php
        if (!$readonly)
            echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Update', array(
                'class' => 'btn btn-large', "id" => "button"));
        ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->