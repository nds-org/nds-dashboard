<?php
/* @var $this MemberpreferencesController */
/* @var $model MemberPreferences */
/* @var $form CActiveForm */
?>

<div class="ui form">

<div class="ui grid">
    <div class="eight wide column">

        <div class="field">
            <?php echo $form->labelEx($model,'country_code'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->dropDownList($model,'country_code', Country::model()->getCountryNames(),
                array("id"=>"prependedInput"));    ?>
            <?php echo $form->error($model,'country_code'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'zone_name'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->dropDownList($model,'zone_name', Zone::model()->getTimezones(),
                array("id"=>"prependedInput"));    ?>
            <?php echo $form->error($model,'zone_name'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'currency_code'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->dropDownList($model,'currency_code', Currencycode::model()->getCurrencyCodesDetail(),
                array("id"=>"prependedInput"));    ?>
            <?php echo $form->error($model,'currency_code'); ?>
        </div>
        <div class="field">
            <?php echo $form->labelEx($model,'date_style'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->dropDownList($model,'date_style', DateTypes::model()->getDateTypes(),
                array("id"=>"prependedInput"));    ?>
            <?php echo $form->error($model,'date_style'); ?>
        </div>
    </div>
    <div class="eight wide column">
        <div class="field">
            <?php echo $form->labelEx($model,'border_style'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->dropDownList($model,'border_style', GUIStyle::getBorderChoices(),
                array("id"=>"prependedInput"));    ?>
            <?php echo $form->error($model,'border_style'); ?>
        </div>

        <div class="field">
            <?php echo $form->labelEx($model,'content_style'); ?>
            <span class="add-on"><span class="icon-user"></span></span>
            <?php echo $form->dropDownList($model,'content_style', GUIStyle::getContentChoices(),
                array("id"=>"prependedInput"));    ?>
            <?php echo $form->error($model,'content_style'); ?>
        </div>
    </div>


    </div>
</div>

