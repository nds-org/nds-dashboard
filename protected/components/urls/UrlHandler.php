<?php
/**
 * A simple URL Handler that can provide some rudimentary operations for certain types of
 * URLS
 *
 * User: Ian T.
 * Date: 10/11/2014
 * Time: 11:38
 */

abstract class UrlHandler {
    private $url;

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }


    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Preview's the particular URL
     *
     * @return mixed
     */
    public abstract function preview();

    /**
     * Downloads the particular HTTP file
     *
     * @return mixed
     */
    public abstract function download();

    /**
     * Returns a list of remote endpoints a user can select from, if available
     *
     * @return mixed
     */
    public abstract function browse();

    /**
     * @param $data data to store
     *
     * @return mixed
     */
    public abstract function store($data);


    /**
     * @return mixed
     */
    public abstract function getDescription();

}