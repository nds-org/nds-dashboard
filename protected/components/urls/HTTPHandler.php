<?php
/**
 * An HTTP Handler.
 *
 * User: scmijt
 * Date: 10/11/2014
 * Time: 11:44
 */

class HTTPHandler extends UrlHandler {

    /**
     * Preview's the particular URL
     *
     * @return mixed
     */
    public function preview()
    {
        // TODO: Implement preview() method.
    }

    /**
     * Preview's the particular URL
     *
     * @return mixed
     */
    public function download()
    {
        // TODO: Implement download() method.
    }

    /**
     * @param $data data to store
     *
     * @return mixed
     */
    public function store($data)
    {
        // TODO: Implement store() method.
    }

    public function browse() {
        return null;
    }


    public function getDescription() {
        return "A resource that pointers to HTML file or other HTTP content";
    }

}