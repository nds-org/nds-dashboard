<?php
/**
 * A URL factory that interfaces with the underlying implementations for interacting
 * with multiple URLs
 *
 * User: scmijt
 * Date: 10/11/2014
 * Time: 11:43
 */

class UrlFactory {

    // URL TYPES

    public static $URL_HTTP=1;
    public static $URL_IRODS=2;
    public static $EPIPHYTE_DATA_SERVICE=3;
    public static $URL_DROP_BOX =4;
    public static $URL_GOOGLE_DRIVE =5;
    public static $URL_SCIDRIVE =6;

//    public static $URL_GRID_FTP=4;

    public static function getUrlHandlerFor($url) {
        $lc_url = strtolower($url);

        if (StringUtilities::startsWith($lc_url, "http")) {
            return new HTTPHandler($url);
        } else if (StringUtilities::startsWith($lc_url, "irod")) {
                return new IRODSHandler($url);
        } else return null;
    }

    public static function getUrlHandlerForProtocol($protocol_id) {
        if ($protocol_id == UrlFactory::$URL_HTTP) {
            return new HTTPHandler();
        } else if ($protocol_id == UrlFactory::$URL_IRODS) {
                return new IRODSHandler();
        } else if ($protocol_id == UrlFactory::$EPIPHYTE_DATA_SERVICE) {
                return new EpiphyteDataService();
        } else return null;
    }

    public static function getBrowseableProtocolList() {
        return array(
            "" => "--None--",
            UrlFactory::$EPIPHYTE_DATA_SERVICE => "Epiphyte Data Service",
            UrlFactory::$URL_DROP_BOX => "Dropbox",
            UrlFactory::$URL_SCIDRIVE => "Scidrive",
      //      UrlFactory::$URL_GOOGLE_DRIVE => "Google Drive",
        );
    }

    public static function getProtocolListAsDropdown() {

        $protocols = UrlFactory::getBrowseableProtocolList();

        $vals = "";

        foreach ($protocols as $key => $val) {

             $vals .='<div class="item" data-value="'.$key.'">'.$val.'</div>
             ';
         }

         return '
        <div class="ui fluid selection dropdown">
            <div class="default text">--None--</div>
            <i class="dropdown icon"></i>
            <div class="menu">
                '.$vals.'
            </div>
        </div>';
    }
} 