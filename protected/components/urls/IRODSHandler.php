<?php
/**
 * An HTTP Handler.
 *
 * User: scmijt
 * Date: 10/11/2014
 * Time: 11:44
 */

class IRODSHandler extends UrlHandler {

    /**
     * Preview's the particular URL
     *
     * @return mixed
     */
    public function preview()
    {
        // TODO: Implement preview() method.
    }

    /**
     * Preview's the particular URL
     *
     * @return mixed
     */
    public function download()
    {
        // TODO: Implement download() method.
    }

    /**
     * @param $data data to store
     *
     * @return mixed
     */
    public function store($data)
    {
        // TODO: Implement store() method.
    }

    public function browse() {
        return array(
            "File Test 1", "File Test 2", "File Test 3"
        );
    }


    public function getDescription() {
        return "A resource that interfaces with the iRODS data management system";
    }

}