<?php
/**
 * An HTTP Handler.
 *
 * User: scmijt
 * Date: 10/11/2014
 * Time: 11:44
 */

class EpiphyteDataService extends UrlHandler
{

    //    public $BASE_EXEC_URL = "http://data.ndspilot.com/v1";
    public static $BASE_EXEC_URL = "http://labs.nationaldataservice.org/ythub/v1";


    /**
     * Preview's the particular URL
     *
     * @return mixed
     */
    public function preview()
    {
        // TODO: Implement preview() method.
    }

    /**
     * Preview's the particular URL
     *
     * @return mixed
     */
    public function download()
    {
        // TODO: Implement download() method.
    }

    /**
     * This bypasses the backend storage service and deals with http addresses directly, for boatload.
     *
     * @param data $url
     * @return data
     */
    public function store($url) {
        return $url;
    }

    /**
     * @param $data data to store
     *
     * @return mixed
     */
    public function storeToEpiphyte($url)
    {
    // "curl -H "Content-type: application/json" -X POST -d '{"resource": {"url": "http://yt-project.org/data/castro_sod_x_plt00036.tgz"}}' http://labs.nationaldataservice.org/ythub/v1/register"    }

          $post = '{"resource": {"url": "'.$url.'"}}';

         // print_r($post);

        //  exit;

          $process = curl_init();
          curl_setopt($process, CURLOPT_URL, EpiphyteDataService::$BASE_EXEC_URL."/register");
          curl_setopt($process, CURLOPT_TIMEOUT, 128);
          curl_setopt($process, CURLOPT_POST, 1);
          curl_setopt($process, CURLOPT_POSTFIELDS, $post);
          curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($process, CURLOPT_HTTPHEADER,     array('Content-type: application/json'));

          $return = curl_exec($process);

          if($return === false)
          {
              echo 'Curl error: ' . curl_error($process);
              exit;
          }

          curl_close($process);

          $jarr =json_decode($return);

          $epitphyte_url = $jarr->{'local'};

          return $epitphyte_url;
    }


    public function browse() {
        $result = $this->get("list_data");

        $json = json_decode($result);

        if (empty($json))
            return "";

        $dirs = array("" => "--None--") ;

        foreach ($json as $id => $keyval) {
            $name = $keyval->{"name"};
            $url = $keyval->{'url'};
            $dirs[$url] = $name;
//            print "Name = ".$name." and URL = ".$url;
        }

       $vals = "";

        foreach ($dirs as $url => $name) {
             $vals .='<div class="epiphyte item" data-value="'.$url.'">'.$name.'</div>
             ';
         }

         return '
        <div class="ui fluid selection dropdown">
            <div class="default text">--None--</div>
            <i class="dropdown icon"></i>
            <div class="menu">
                '.$vals.'
            </div>
        </div>';
    }


    public function getDescription() {
        return "A resource that interfaces with the docker backend for browsing files";
    }


    private function get($command) {

         $process = curl_init(EpiphyteDataService::$BASE_EXEC_URL."/".$command);
         curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: text/html'));
         curl_setopt($process, CURLOPT_TIMEOUT, 30);
         curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);

         $return = curl_exec($process);

         if($return === false)
         {
             echo 'Curl error: ' . curl_error($process);
         }

         curl_close($process);

         return $return;
     }
}