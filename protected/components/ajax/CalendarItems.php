<?php
/**
 * A list formatted in the calendar format that displays all work orders on a calendar view.
 *
 */

class CalendarItems {

    private $calendarItems;
    private $only_forward_events;
    private $filter_start_time;

    function __construct($only_forward_events=False) {
        $this->calendarItems = array();
        $this->only_forward_events = $only_forward_events;
        if ($this->only_forward_events)
            $this->filter_start_time = time();
        else // grab a week's worth
            $this->filter_start_time = time() - (60*60*24*7);

        $articles = Article::model()->getArticlesFor(Yii::app()->user->id);

         foreach ($articles as $article) {

             /** @var  $article Article*/

             $subject = $article->title;

             $content = TextFormat::trimText($article->abstract, 80);
             $sender_id = Yii::app()->user->id;
             $sender_member = Member::model()->getByMemberID($sender_id);
             $article_url = Pages::baseURL().'article/'.$article->id;

             $this->calendarItems[] = new CalendarListItem($article->date_added, $article->abstract,$article->title, $article_url);
         }
    }

    /**
     * @return mixed
     */
    public function getCalendarItems()
    {
        return $this->calendarItems;
    }

}