<?php
/**
 * User: Ian Taylor
 * Date: 24/09/2014
 * Time: 12:31
 *
 * The backend for the upcoming tool.
 */

class Upcoming {

    private $calendarItems;
    private $calendarItemArray;
    private $memberPref;

    function __construct()
    {
        $this->calendarItems = new CalendarItems(true);
        $this->calendarItemArray = $this->calendarItems->getCalendarItems();
        $this->memberPref = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

      //  for ($i=0; $i<count($this->calendarItemArray); ++$i) {
    //        echo $this->getEvent($this->calendarItemArray[$i]);
       // }

    }

    public function getFirstItem() {
        if  ((isset($this->calendarItemArray)) && (count($this->calendarItemArray)>0)) {
            /** @var  $calendarFirstEvent CalendarListItem */
            $calendarFirstEvent = $this->calendarItemArray[0];
            if (!isset($calendarFirstEvent))
                return "";
            $unixTime = $calendarFirstEvent->getDate();
            $localtime = Timezone::model()->getLocalTimeFor($unixTime, $this->memberPref->zone_name);
            $first_month = new TimeObject($localtime);

            return '
                    <div class="month">
                        '.$first_month->getMonthAsString().'
                     </div>
                     <div class="clearfix"></div>
                     <div class="date">
                        '.$first_month->getDay().'
                     </div>
                     <div class="name">
                        <a href="'.$calendarFirstEvent->getUrl().'"> '.$calendarFirstEvent->getTitle().'</a>
                     </div>
                    ';
        }
        return "";
    }

    private function getEvent($event) {
        /** @var  $event CalendarListItem */
        $unixTime = $event->getDate();
        $localtime = Timezone::model()->getLocalTimeFor($unixTime, $this->memberPref->zone_name);

        $date = new TimeObject($localtime);

        return "
        <div class='event'>
            <div class='title'>
                ".$date->getMonthAsString()." ".$date->getDay()." ".$date->getYear()."
            </div>
            <div class='name'>
                <a href=\"".$event->getUrl()."\">".$event->getTitle()."</a>
            </div>
        </div>
        ";
    }

    public function getNextEvents() {
        $events = "";

        $event_count = count($this->calendarItemArray);
        $max = min(3, $event_count);

  //      echo "MAX = ".$max;
    //    echo "Events = ".$event_count;

        for ($i=1; $i<$max; ++$i) {
            $events .= $this->getEvent($this->calendarItemArray[$i]);
        }

        return $events;
    }
} 