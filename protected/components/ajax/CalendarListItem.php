<?php
/**
 * An event for the calendar app, which is of the form:
 *
 {
  "date": "2013-12-30 17:30:00",
   "type": "meeting",
   "title": "Test Last Year",
   "description": "Lorem Ipsum dolor set",
   "url": "me.com"
  }
 *
 *
 * User: scmijt
 * Date: 08/01/2014
 * Time: 13:22
 */

class CalendarListItem {
    private $date;
//    private $type; -- not used in the calendar to display anything...
    private $title;
    private $description;
    private $url;

    function __construct($date, $description, $title, $url)
    {
        if (is_int($date))
            $this->date = $date;
        else
            $this->date = DateFormat::mySQLToPhpDate($date);

//kjr begin
// Don't know the best way to do this - Need to trim [EOL] carriage returns or whatever they get turned into
//
        $max_to_display = 200;
        $pchr = "</p>";
        $dchr = "<div>";
        $bchr = "<br><br>";
        $display_desc = substr($description,0,$max_to_display);
        $spec_char_loc = strpos($display_desc,$pchr);
        if  (($spec_char_loc > 1) && ($spec_char_loc <  $max_to_display)) {
            $display_desc = substr($display_desc,0,$spec_char_loc);
            $max_to_display = $spec_char_loc;
        }

        $spec_char_loc = strpos($display_desc,$dchr);
        if  (($spec_char_loc > 1) && ($spec_char_loc <  $max_to_display)) {
            $display_desc = substr($display_desc,0,$spec_char_loc);
            $max_to_display = $spec_char_loc;
        }

        $spec_char_loc = strpos($display_desc,$bchr);
        if  (($spec_char_loc > 1) && ($spec_char_loc <  $max_to_display)) {
            $display_desc = substr($display_desc,0,$spec_char_loc);
            $max_to_display = $spec_char_loc;
        }

// Need to trim <p> at beginning of the line, if it exist
        $pchr = "<p>";
        $spec_char_loc = strpos($display_desc,$pchr);
        if  ($spec_char_loc === 0) {
            $display_desc = substr($display_desc,($spec_char_loc+3),$max_to_display);
            $max_to_display = $max_to_display - 3;
        }

        // IAN FIX FOR STUPID CHARACTER ISSUE !!!

        $this->description  = $this->removeInvalidChars($display_desc);


//        $this->description = $display_desc;

//        $this->description = $description;
// kjr end

        $this->title = $title;
        $this->url = $url;
// kjr 032814
//        print "description = ".$this->description;
//        print "date = ".$date." this date = ".$this->date;
//        exit();
//
    }

    function removeInvalidChars( $text) {
        $regex = '/( [\x00-\x7F] | [\xC0-\xDF][\x80-\xBF] | [\xE0-\xEF][\x80-\xBF]{2} | [\xF0-\xF7][\x80-\xBF]{3} ) | ./x';
        return preg_replace($regex, '$1', $text);
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function getAsArray()
     {
         $arr=array();
         $arr['date'] = strval($this->date*1000); // needs milliseconds
         $arr['title'] = $this->title;
         $arr['description'] = $this->description;
         $arr['url'] = $this->url;

         return $arr;
     }

    public function getAsJson()
       {
           return json_encode($this->getAsArray());
       }

} 