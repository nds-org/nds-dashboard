<?php
/**
 * Class to interface with the BoatLoad API.
 *
 * User: scmijt
 * Date: 10/11/2014
 * Time: 11:44
 */

class BoatloadAPI
{
    private $NOTRE_DAME_BOATLOAD_API = "http://129.74.247.86:8080/v1/container";
    private $CARDIFF_BOATLOAD_API = "http://131.251.172.57:8080/v1/container";

    public static $CARDIFF = "Cardiff (http://131.251.172.57:8080/v1/container)";
    public static $NOTRE_DAME = "Notre Dame (http://129.74.247.86:8080/v1/container)";

    private $base_API;

    /**
     * BoatloadAPI constructor for $CARDIFF or $NOTRE_DAME_APIs
     * @param $base_API
     */
    public function __construct($base_API)
    {
        if ($base_API === BoatloadAPI::$CARDIFF)
            $this->base_API = $this->CARDIFF_BOATLOAD_API;
        else
            $this->base_API = $this->NOTRE_DAME_BOATLOAD_API;
    }


    public function getBaseURL() {
        return $this->base_API;
    }

    public function getAPIList() {
        return array(
            $this->CARDIFF_BOATLOAD_API => BoatloadAPI::$CARDIFF,
            $this->NOTRE_DAME_BOATLOAD_API=> BoatloadAPI::$NOTRE_DAME
        );
    }

    public function createContainerAndExecute($dataFile, $script_url, $container ) {

        $script_name = StringUtilities::substringAfterLast($script_url, "/");

        $post = array(
            "image_name" => $container,
            "image_tag" => "latest",
            "scripturl" => $script_url,
            "scriptname" => $script_name,
            "container_args" => "python /mnt/". $script_name,
            "dataurl" => $dataFile,
            "datapath" => "/mnt/",
            "restart" => "no",
            "restartsec" =>  0
        );

        $process = curl_init();
        curl_setopt($process, CURLOPT_URL, $this->getBaseURL()."/");
        curl_setopt($process, CURLOPT_TIMEOUT, 128);
        curl_setopt($process, CURLOPT_POST, 1);
        curl_setopt($process, CURLOPT_POSTFIELDS, $post);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($process, CURLOPT_HEADER, 0);
        curl_setopt($process, CURLOPT_USERPWD, "admin:admin"); // check this

        $return = curl_exec($process);

        if($return === false)
        {
            echo 'Request Failed! We got an error from Curl: ' . curl_error($process);
            exit;
        }

        curl_close($process);

        return $return;
    }

    public function reset($resource_id)
    {
        $runScript = RunScript::model()->getByResourceID($resource_id);

        if (empty($runScript)) {
            return false;
        }

        return $this->delete("/". $runScript->run_id."/", false);
    }

    public function download($resource_id)
    {
        $runScript = RunScript::model()->getByResourceID($resource_id);

        if (empty($runScript)) {
            return false;
        }

        return $this->get("/". $runScript->run_id."/archive", false);
    }

    public function status($resource_id)
    {
        $runScript = RunScript::model()->getByResourceID($resource_id);

        if (empty($runScript)) {
            return false;
        }

        $return = $this->get("/". $runScript->run_id."/changes");

        $status = "Initialized";

        if (empty($return))
            $status = "Running";
        else
            $status = "Completed";

        echo $status;
    }

    public function stdout($resource_id)
    {
        $runScript = RunScript::model()->getByResourceID($resource_id);

        if (empty($runScript)) {
            return false;
        }

        $stdout = $this->get("/". $runScript->run_id."/stdout", false);

        if (!empty($stdout)) {
            $stdout = str_replace("\"\\\"", "", $stdout);
            $stdout = str_replace("\\n", "\n", $stdout);
            $stdout = str_replace("\\", "", $stdout);
            $stdout = substr($stdout, 3);
            $stdout = substr(0, count($stdout)-3);
            return $stdout;
        } else
            return "";
    }

    /**
     * This bypasses the backend storage service and deals with http addresses directly, for boatload.
     *
     * @param data $url
     * @return data
     */
    public function store($url) {
        return $url;
    }


    private function get($command, $json=true) {

        if ($json)
            $process = curl_init($this->getBaseURL()."/".$command."?format=json");
        else
            $process = curl_init($this->getBaseURL()."/".$command);

        curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: text/html'));
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($process, CURLOPT_USERPWD, "admin:admin");

        // Need to add auth=('admin', 'admin')

        $return = curl_exec($process);

        if($return === false)
        {
            echo 'Curl error: ' . curl_error($process);
        }

        curl_close($process);

        return $return;
    }

    private function delete($command, $json=true) {

        if ($json)
            $process = curl_init($this->getBaseURL()."/".$command."?format=json");
        else
            $process = curl_init($this->getBaseURL()."/".$command);

        curl_setopt($process, CURLOPT_HTTPHEADER, array('Content-Type: text/html'));
        curl_setopt($process, CURLOPT_TIMEOUT, 30);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($process, CURLOPT_USERPWD, "admin:admin");

        curl_setopt($process, CURLOPT_CUSTOMREQUEST, "DELETE");

        $return = curl_exec($process);

        if($return === false)
        {
            echo 'Curl error: ' . curl_error($process);
        }

        curl_close($process);

        return $return;
    }
}