<?php
/**
 * This filter enforces HTTP or HTTPS protocol for the request, by aborting and redirecting
 * the request to the same URL, but with the correct protocol specifier applied.
 *
 */
class HttpsFilter extends CFilter {

    /**
     * @var mixed if true, SSL is required - if false, SSL is not allowed - if null, both are allowed
     */
    public $https=null;


    /**
     * For development purposes - do not require https on a local host
     *
     * @return bool
     */
    function isLocalHost() {
        if (!StringUtilities::contains($_SERVER['SERVER_NAME'], "easyworkorder.com"))  {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Apply schema-based filtering
     */
    public function preFilter($filterChain)
    {
        if ($this->isLocalHost())
            return true;

        if ($this->https == false)
            return $this->requireSchema('http');
        else
            return $this->requireSchema('https');
    }

    public function getCurrentURLScheme() {
//        if ($_SERVER[SSL_TLS_SNI] == $_SERVER[SERVER_NAME])
        if (Yii::app()->getRequest()->getIsSecureConnection())
            $scheme="https";
        else
            $scheme="http";

        return $scheme;
        }

    /**
     * Aborts the request, redirects and switches schema, if required.
     *
     * @param string the required schema for this request, e.g. 'http' or 'https'
     */
    protected function requireSchema($schema)
    {
        $currentScheme = $this->getCurrentURLScheme();
        $app = Yii::app();
        $request = trim($app->urlManager->parseUrl($app->request), '/');

     //   echo "-- Filter schema = ".$schema. " and current schema = ".$currentScheme. " and Request is ".$request;

        if ($currentScheme!=$schema) {
            $c = Yii::app()->getController();
            $c->redirect($c->createAbsoluteUrl($request, $_GET, $schema), true);
            return false;
        }
        return true;
    }

}
