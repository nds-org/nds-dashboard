<?php

class ThumbnailGenerator {
    var $image;
    var $image_type;


    public static function getThumbnailFor($directory, $filename) {
     //   echo "Directory: ".$directory."<br>";
       // echo "file: ".$filename."<br>";
        if (empty($directory))  {
         //   echo "Empty Directory";
            $directory = DataDirectories::getInternalImagesDirectory();
            $image_to_work = DataDirectories::getInternalImagesDirectory()."person.png";
        } else {
            if (StringUtilities::endsWith($directory, "/"))
                $image_to_work = $directory.$filename;
            else
                $image_to_work = $directory."/".$filename;

            if (!is_file($image_to_work)) {
                $image_to_work = DataDirectories::getInternalImagesDirectory()."person.png";
            //    echo "Empty Directory";
            }
        }
        $image_filename = StringUtilities::substringAfterLast($image_to_work, "/");

        $ending = StringUtilities::substringAfterLast($image_filename, ".");
        $name = StringUtilities::substringBeforeLast($image_filename, ".");

        $thumb_loc = $directory.$name."_thumb.".$ending;

        if (file_exists($thumb_loc )) {
            return $thumb_loc ;
        } else {
            $thumbnail = new ThumbnailGenerator();
            $thumbnail->load($image_to_work);
            $thumbnail->resize(120,120);
            $thumbnail->save($thumb_loc);
            return $thumb_loc;
        }

    }

    public static function removeThumbnailFor($directory, $filename) {
         if (empty($directory))  {
             $directory = DataDirectories::getInternalImagesDirectory();
             $image_to_work = DataDirectories::getInternalImagesDirectory()."person.png";
         } else {
             if (StringUtilities::endsWith($directory, "/"))
                 $image_to_work = $directory.$filename;
             else
                 $image_to_work = $directory."/".$filename;

             if (!is_file($image_to_work))
                 $image_to_work = DataDirectories::getInternalImagesDirectory()."person.png";
         }

         $image_filename = StringUtilities::substringAfterLast($image_to_work, "/");

         $ending = StringUtilities::substringAfterLast($image_filename, ".");
         $name = StringUtilities::substringBeforeLast($image_filename, ".");

         $thumb_loc = $directory.$name."_thumb.".$ending;

         if (file_exists($thumb_loc )) {
             unlink($thumb_loc);
         }
     }

    function load($filename) {
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
            $this->image = imagecreatefromjpeg($filename);
        }
        elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        }
        elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }

    function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image,$filename);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->image,$filename);
        } if( $permissions != null) {
            chmod($filename,$permissions);
        }
    }

    function output($image_type=IMAGETYPE_JPEG) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($this->image);
        }
    }

    function getWidth() {
        return imagesx($this->image);
    }

    function getHeight() {
        return imagesy($this->image);
    }

    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }

    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio; $this->resize($width,$height);
    }

    function scale($scale) {
        $width = $this->getWidth() * $scale/100;
        $height = $this->getheight() * $scale/100;
        $this->resize($width,$height);
    }

    function resize($width,$height) {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }
}

?>