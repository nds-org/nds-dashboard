<?php

class GUIStyle {

    private static $borderChoices = array(
        'black' => 'black',
        'blue' => 'blue',
        'brown' => 'brown',
        'purple' => 'purple',
        'grey' => 'grey',
        'white' => 'white',
    );

    private static $mainContent = array(
        'light' => 'light',
        'medium' => 'medium',
        'dark' => 'dark',
        'grey' => 'grey',
        'blue' => 'blue',
    );




    /**
     * Background - menu styles
     *
     * black
     * blue
     * brown
     * purple
     * grey
     * white
     */

    /**
     * light, medium, dark
     */

    public static function getBorderChoices() {
        return GUIStyle::$borderChoices;
    }

    public static function getContentChoices() {
        return GUIStyle::$mainContent;
    }

    public static function getDefaultBorder() {
        return GUIStyle::$borderChoices['black'];
    }

    public static function getDefaultContent() {
        return GUIStyle::$mainContent['light'];
    }

    public static function getSemanticClassesForOuterStyle() {

        if (StateVariables::isGuest())
            return "";

        $URL = StateVariables::getSiteURL();

        if ((StateVariables::isWhiteLabel()) && ($URL != 'http://easyworkorder.com'))
            return "inverted color";

        $myprefs = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

        $bg_color = $myprefs->border_style;
        $inverted = "inverted";

        if ($bg_color === "white")
            $inverted = "";

        return $inverted." ".$bg_color;

    }


}