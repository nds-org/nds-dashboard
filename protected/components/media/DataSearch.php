<?php
/**
 * A directory lister that ignores certain files to just return user files within a directory.
 *
 */
class DataSearch {

    private $images;
    private $images_internal;
    private $is_internal=false;

    // Need to define these at some point.
//    private static $fileTypes = array(
  //      ".jpg", ".png", ".JPG", ".PNG"
  //  );

    // But disable for now and allow anything but certain files

    private static $disallow = array(
        ".thmb"
    );

    function __construct($directory)
    {

        $iterator = new RecursiveDirectoryIterator($directory);
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        $filter = new ImageRecursiveFilterIterator($iterator);

        $allFileObjects = new RecursiveIteratorIterator($filter, RecursiveIteratorIterator::SELF_FIRST);

        $this->images = array();

        foreach( $allFileObjects as $fullFileName => $fileSPLObject ) {
            /** @var $fileSPLObject SPLFileObject */

            if (!is_dir($fullFileName)) {
                $isOk = true;
                foreach (DataSearch::$disallow as $extension) {
                    if (StringUtilities::endsWith($fullFileName, $extension))
                        $isOk = false;
                }

                $relativeFileName =  StringUtilities::substringAfter($fullFileName, Pages::getBaseDir().DIRECTORY_SEPARATOR);

                if ($isOk) {
                    $this->images[$relativeFileName] = $this->getURLFor($fullFileName);
                    $this->images_internal[$relativeFileName] = $this->getInternalURLFor($this->getPathFrom($fullFileName), $this->getFileNameFrom($fullFileName));
                }
            }
        }
    }


    /**
     * @param boolean $is_internal
     */
    public function setIsInternal($is_internal)
    {
        $this->is_internal = $is_internal;
    }

    /**
     * @return boolean
     */
    public function getIsInternal()
    {
        return $this->is_internal;
    }


    /**
      * @return mixed
      */
     public function getImages()
     {
         if ($this->is_internal)
             return $this->getImagesInternal();
         else
            return $this->images;
     }

    /**
     * Get's an array of links to videos that are in the protected part of the website.
     *
     * @return mixed
     */
    private function getImagesInternal()
    {
        return $this->images_internal;
    }


    /**
     * Returns the name of the file without the extensions and path
     *
     * @param $pathAndFileName
     * @return string
     */
    private function getFileNameFrom($pathAndFileName) {

        return substr($pathAndFileName, StringUtilities::lastIndexOf($pathAndFileName, "/")+1);
    }

    private function getFileNameNoExtensionFrom($pathAndFileName) {
        $pathNoExtension = StringUtilities::substringBeforeLast($pathAndFileName, ".");

        return substr($pathNoExtension, StringUtilities::lastIndexOf($pathNoExtension, "/")+1);
    }

     private function getPathFrom($pathAndFileName) {
         $fullPath  = StringUtilities::substringBeforeLast($pathAndFileName, "/")."/";

         return $fullPath;
      }

    private function getURLFor($pathAndFileName) {
        $baseDir = Pages::getPublicBaseDir();

        $relative_url = substr($pathAndFileName, strlen($baseDir)+1);

        return Pages::getAbsoluteBaseURL().$relative_url;
    }

    private function getInternalURLFor($path, $fileName) {
        return Pages::getAbsoluteBaseURL()."media/thumbnail?directory=".$path."&filename=".$fileName;
    }

    public static function getTitleFrom($videoFileName) {
        $replaced_space = str_replace("-", " ", $videoFileName);
        $title = ucwords($replaced_space);

        return $title;
    }

    public static function getNumberOfImages() {
        $images = new DataSearch(DataDirectories::getMembersDirectory());
        $files = $images->getImages();

        return count($files);
    }
}

class ImageRecursiveFilterIterator extends RecursiveFilterIterator {

    public static $FILTERS = array(
        '__MACOSX', '.git', '.svn', '.thumbs', '.quarantine'
    );

    public function accept() {
        return !in_array(
            $this->current()->getFilename(),
            self::$FILTERS,
            true
        );
    }

}

?>