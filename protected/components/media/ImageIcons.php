<?php
/**
 * Created by PhpStorm.
 * User: scmijt
 * Date: 09/02/2014
 * Time: 10:24
 */

class ImageIcons {

    private static $CODE_ICONS = array(
        "java" => "java.png",
        "bash" => "bash.png",
        "sh" => "bash.png",
        "tcsh" => "bash.png",
        "csh" => "bash.png",
        "html" => "html.png",
        "json" => "json.png",
        "py" => "python.png",
        "pyc" => "python.png",
        "txt" => "txt.png"
);

    public static function getIconUrlFor($fileName) {
        $fileName = strtolower($fileName);
        if (StringUtilities::endsWith($fileName, ".pdf"))
            return Yii::app()->request->baseUrl."/images/icons/pdf.jpg";
        else if ((StringUtilities::endsWith($fileName, ".doc")) || (StringUtilities::endsWith($fileName, ".docx")))
            return Yii::app()->request->baseUrl."/images/icons/word.jpg";
        else if ((StringUtilities::endsWith($fileName, ".ppt")) || (StringUtilities::endsWith($fileName, ".pptx")))
            return Yii::app()->request->baseUrl."/images/icons/ppt.jpg";
        else if ((StringUtilities::endsWith($fileName, ".xls")) || (StringUtilities::endsWith($fileName, ".xlsx")))
            return Yii::app()->request->baseUrl."/images/icons/excel.jpg";
        else if ((StringUtilities::endsWith($fileName, ".mp4")) || (StringUtilities::endsWith($fileName, ".mov")))
            return Yii::app()->request->baseUrl."/images/icons/movie.png";
        else if ((StringUtilities::startsWith($fileName, "http")) || (StringUtilities::startsWith($fileName, "https")) || (StringUtilities::startsWith($fileName, "epiphyte"))) {
            return Yii::app()->request->baseUrl."/images/icons/http.png";
            }
        else {
            $extension = StringUtilities::substringAfterLast($fileName, ".");
            if (!empty($extension))
                $extension = trim($extension);

            if (isset(ImageIcons::$CODE_ICONS[$extension])) {
                return Yii::app()->request->baseUrl."/images/icons/".ImageIcons::$CODE_ICONS[$extension];
            }
            else
                return $fileName;
        }

    }

    public static function getIconPathFor($fileName) {
        $basePath = $rootPath = pathinfo(Yii::app()->request->scriptFile);
        $basePath = $basePath['dirname'];

        if (StringUtilities::endsWith($fileName, ".pdf"))
            return $basePath."/images/icons/pdf.jpg";
        else if ((StringUtilities::endsWith($fileName, ".doc")) || (StringUtilities::endsWith($fileName, ".docx")))
            return $basePath."/images/icons/word.jpg";
        else if ((StringUtilities::endsWith($fileName, ".ppt")) || (StringUtilities::endsWith($fileName, ".pptx")))
            return $basePath."/images/icons/ppt.jpg";
        else if ((StringUtilities::endsWith($fileName, ".xls")) || (StringUtilities::endsWith($fileName, ".xlsx")))
            return $basePath."/images/icons/excel.jpg";
        else if ((StringUtilities::endsWith($fileName, ".mp4")) || (StringUtilities::endsWith($fileName, ".mov")))
            return $basePath."/images/icons/movie.png";
        else if ((StringUtilities::startsWith($fileName, "http")) || (StringUtilities::startsWith($fileName, "https"))  || (StringUtilities::startsWith($fileName, "epiphyte"))) {
            return $basePath."/images/icons/http.png";
        } else {
            $extension = StringUtilities::substringAfterLast($fileName, ".");
            if (!empty($extension))
                $extension = trim($extension);

            if (isset(ImageIcons::$CODE_ICONS[$extension])) {
                return $basePath."/images/icons/".ImageIcons::$CODE_ICONS[$extension];
            }
            else
                return $fileName;
        }
    }

} 