<?php
/**
 * A directory lister that ignores certain files to just return user files within a directory.
 *
 */
class VideoSearch {

    private $videos;
    private $videos_internal;
    private $is_internal=false;

    private static $fileTypes = array(
        ".mp3", ".mp4", ".m4v", ".webma", ".webmv", ".oga", ".ogv", ".fla", ".wav"
    );

    // need to check on all of these to see if they are correct for the JPlayer.
    // I know the Mp4 works... but not tested the rest. TODO. Ian T.

    private static $fileTypeToJPlayerMappings = array(
        ".mp3" => "mp3",
        ".mp4" => "m4v",
        ".m4v"=> "m4v",
        ".webma"=> "webma",
        ".webmv"=> "webmv",
        ".oga"=> "oga",
        ".ogv"=> "ogv",
        ".fla"=> "fla",
        ".wav" => "wav"
    );

    function __construct($directory)
    {

        $iterator = new RecursiveDirectoryIterator($directory);
        $iterator->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);

        $filter = new VideoRecursiveFilterIterator($iterator);

        $allFileObjects = new RecursiveIteratorIterator($filter, RecursiveIteratorIterator::SELF_FIRST);

        $this->videos = array();

        foreach( $allFileObjects as $fullFileName => $fileSPLObject ) {
            /** @var $fileSPLObject SPLFileObject */

            if (!is_dir($fullFileName)) {
                $isAVideo = false;
                foreach (VideoSearch::$fileTypes as $extension) {
                    if (StringUtilities::endsWith($fullFileName, $extension))
                        $isAVideo = true;
                }

                if ($isAVideo) {
                    $this->videos[$this->getFileNameFrom($fullFileName)] = $this->getURLFor($fullFileName);
                    $this->videos_internal[$this->getFileNameFrom($fullFileName)] = $this->getInternalURLFor($fullFileName);
                }
            }
        }
    }


    /**
     * @param boolean $is_internal
     */
    public function setIsInternal($is_internal)
    {
        $this->is_internal = $is_internal;
    }

    /**
     * @return boolean
     */
    public function getIsInternal()
    {
        return $this->is_internal;
    }


    /**
      * @return mixed
      */
     public function getVideos()
     {
         if ($this->is_internal)
             return $this->getVideosInternal();
         else
            return $this->videos;
     }

    /**
     * Get's an array of links to videos that are in the protected part of the website.
     *
     * @return mixed
     */
    private function getVideosInternal()
    {
        return $this->videos_internal;
    }


    /**
     * Returns the name of the file without the extensions and path
     *
     * @param $pathAndFileName
     * @return string
     */
    private function getFileNameFrom($pathAndFileName) {
        $pathNoExtension = StringUtilities::substringBeforeLast($pathAndFileName, ".");

        return substr($pathNoExtension, StringUtilities::lastIndexOf($pathNoExtension, "/")+1);
    }

    private function getURLFor($pathAndFileName) {
        $baseDir = Pages::getPublicBaseDir();

        $relative_url = substr($pathAndFileName, strlen($baseDir)+1);

        return Pages::getAbsoluteBaseURL().$relative_url;
    }

    private function getInternalURLFor($pathAndFileName) {
        $baseDir = Pages::getPublicBaseDir();


        return Pages::getAbsoluteBaseURL()."member/video?location=".$pathAndFileName;
    }

    public static function getTitleFrom($videoFileName) {
        $replaced_space = str_replace("-", " ", $videoFileName);
        $title = ucwords($replaced_space);

        return $title;
    }

    public static function getJPlayerTypeForFile($fileName) {
        $fileEnding = StringUtilities::substringAfterLast($fileName, ".");

        return VideoSearch::$fileTypeToJPlayerMappings[".".$fileEnding];
    }

    public static function getNumberOfVideos() {
        $videos = new VideoSearch(DataDirectories::getCurrentMemberFilesDirectory());
        $files = $videos->getVideos();

        return count($files);
    }
}

class VideoRecursiveFilterIterator extends RecursiveFilterIterator {

    public static $FILTERS = array(
        '__MACOSX', '.git', '.svn', '.thumbs', '.quarantine'
    );

    public function accept() {
        return !in_array(
            $this->current()->getFilename(),
            self::$FILTERS,
            true
        );
    }

}

?>