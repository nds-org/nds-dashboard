<?php
/**
 * Created by PhpStorm.
 * User: scmijt
 * Date: 30/12/2014
 * Time: 14:26
 */

class Mobile {

    public static function isMobileDevice() {
        $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

        if ($android)
            $android = strpos($_SERVER['HTTP_USER_AGENT'],"Mobile");

        $adjust_center = false;
        if ((strpos($_SERVER['HTTP_USER_AGENT'],"Safari/533")) || (strpos($_SERVER['HTTP_USER_AGENT'],"Safari/534")))
            $adjust_center = true;

        $old_android = false;
        if ((strpos($_SERVER['HTTP_USER_AGENT'],"Android 2.")) || (strpos($_SERVER['HTTP_USER_AGENT'],"Android 3.")))
            $old_android = true;

        //$ipod = true;

        $cellphone = false;

        if ($iphone || $android || $palmpre || $ipod || $berry == true)
            $cellphone = true;
        return $cellphone;
    }
} 