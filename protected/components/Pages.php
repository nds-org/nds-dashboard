<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scmijt
 * Date: 12/09/2013
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */

class Pages {

    public static $publicPages = array(
        "",
        "product",
        "comparison",
        "product-modules",
        "case-studies",
        "pricing",
        "faq",
        "about",
        "team",
        "news",
        "contact",
        "privacy",
        "terms",
    );

    public static $customerPages = array(
        "dashboard",
        "notes",
        "finder",
        "eco",
        "motivation",
        "implementation",
        "pricing",
        "comparison",
        "case-studies/peninsula",
        "calculator",
        "privacy",
        "terms"
    );


    public static function getBaseDir() {
        return Yii::app()->basePath;
    }

    public static function getPublicBaseDir() {
        $basePath = Yii::app()->basePath;

        $one_down = substr($basePath, 0, StringUtilities::lastIndexOf($basePath, "/"));

        return $one_down."/public";
    }

    /**
     * Gets the relative URL, including the / of the current page
     * @return string
     */
    public static function getRelativeURL() {
        return StringUtilities::substringAfter(Yii::app()->request->url, Yii::app()->request->baseUrl);
    }

    /**
     * Gets the current Page without the leading /
     * @return string
     */
    public static function getPage() {
        return StringUtilities::substringAfter(Yii::app()->request->url, Yii::app()->request->baseUrl."/");
    }

    public static function themeURL() {
        return Yii::app()->theme->baseUrl."/";
    }

    public static function baseURL() {
        return Yii::app()->request->baseUrl."/";
    }

    public static function getAbsoluteBaseURL() {
        if (Yii::app()->getRequest()->getIsSecureConnection())
            $scheme="https";
        else
            $scheme="http";

        return $scheme."://".$_SERVER['SERVER_NAME'].Yii::app()->request->baseUrl."/";
    }

    public static function getAbsoluteThemeURL() {
        if (Yii::app()->getRequest()->getIsSecureConnection())
            $scheme="https";
        else
            $scheme="http";

        return $scheme."://".$_SERVER['SERVER_NAME'].Yii::app()->theme->baseUrl."/";
    }

    public static function getListItemsForCurrentController($itemID=null, $idString2=null, $idVal2=null, $idString3=null, $idVal3=null) {

        if (StateVariables::getCurrentControllerName() == 'workorder') {
            return Yii::app()->request->baseUrl."/".StateVariables::getCurrentControllerName()."/viewclosed/id/".$itemID;
        } else if (!empty($idString3))
            return Yii::app()->request->baseUrl."/".StateVariables::getCurrentControllerName()."/list/".$idString2."/".$idVal2."/".$idString3."/".$idVal3;
        else if (!empty($idString2))
            return Yii::app()->request->baseUrl."/".StateVariables::getCurrentControllerName()."/list/".$idString2."/".$idVal2;
        else
            return Yii::app()->request->baseUrl."/".StateVariables::getCurrentControllerName()."/list";
    }

    public static function isOnPublicSite() {
        if (Yii::app()->user->isGuest)
            return true;

        $publicSitePage = in_array(Pages::getPage(), Pages::$publicPages);
        $customerSitePage = in_array(Pages::getPage(), Pages::$customerPages);

        return $publicSitePage && !$customerSitePage;
    }

    public static function isAPublicPage() {
          if (Yii::app()->user->isGuest)
              return true;

          $publicSitePage = in_array(Pages::getPage(), Pages::$publicPages);

          return $publicSitePage;
      }

    public static function getIsActiveText($pageName) {
        $browserPagename = Pages::getPage();
        if ($pageName === $browserPagename)
            return 'class="active"';
        else
            return "";
    }

}