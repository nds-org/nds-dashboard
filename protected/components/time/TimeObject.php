<?php
/**
 * A generic time object that returns sensible values. Hate dealing with date and all that crap.
 */
class TimeObject
{

    private $unixTime;
    private $year;
    private $month;
    private $day;
    private $dayOfWeek;
    private $hour;
    private $minute;
    private $second;
    private $date;

    /**
     * @param $unixTime a unix time stamp with the time to represent
     */
    public function __construct($unixTime)
    {
        $this->unixTime = $unixTime;

        $this->date = date('Y-m-d-N-G-i-s', $unixTime);

        list($this->year,$this->month,$this->day,$this->dayOfWeek, $this->hour,$this->minute,$this->second)=
            explode('-',$this->date);
    }

    /**
     * Returns a string that is compatible with a mysql timestamp
     *
     * @return string
     */
    public function getTimestampMysq(){
        return date('Y-m-d H:i:s',$this->unixTime);
    }


    public function printTime() {
        echo "Year = ".$this->year."<br>";
        echo "Month= ".$this->month."<br>";
        echo "Day= ".$this->day."<br>";
        echo "DayOfWeek= ".$this->dayOfWeek."<br>";
        echo "Hour= ".$this->hour."<br>";
        echo "Minute= ".$this->minute."<br>";
        echo "Second= ".$this->second."<br>";
    }

    public function getNiceTime() {
        return $this->date;
   }

    public function getDay()
    {
        return $this->day;
    }

    public function getDayOfWeek()
    {
        return $this->dayOfWeek;
    }

    public function getDayOfWeekString()
    {
        return TimePeriods::getDayAsStringFromIndex(intval($this->dayOfWeek-1)); // this is quite bad. we should have started the week days off from 1 :(
    }

    public function getHour()
    {
        return $this->hour;
    }

    public function getMinute()
    {
        return $this->minute;
    }

    public function getMonth()
    {
        return $this->month;
    }

    public function getMonthAsString()
    {
        return TimePeriods::getMonthAsStringFromIndex(intval($this->month));
    }

    public function getSecond()
    {
        return $this->second;
    }

    public function getUnixTime()
    {
        return $this->unixTime;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getDate()
    {
        return $this->date;
    }


}
