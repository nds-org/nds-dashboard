<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scmijt
 * Date: 09/09/2013
 * Time: 14:09
 * To change this template use File | Settings | File Templates.
 */

class DateFormat {

    public static $HOUR_SECS = 3600;
    public static $DAY_SECS = 86400;
    public static $WEEK_SECS = 604800;
    public static $MONTH_SECS = 2629800;
    public static $YEAR_SECS = 31557600;
    public static $QUARTER_SECS = 7889400;

    function __construct()
    {
    }


    public static function dateFormat($ewoDate) {
        if (($ewoDate === "0000-00-00 00:00:00") || ($ewoDate == false))
            return "Not Set";

        if (StringUtilities::contains($ewoDate, ":") && StringUtilities::contains($ewoDate, "-"))
            return DateFormat::dateFormatFromUnix(DateFormat::mySQLToPhpDate($ewoDate));

        return DateFormat::dateFormatFromUnix(strtotime($ewoDate));
    }

    public static function dateFormatFromUnix($ewoDate) {
        $memberpref = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

        if (!isset($memberpref))
            $memberpref = new MemberPreferences();

        $localtime = Timezone::model()->getLocalTimeFor($ewoDate, $memberpref->zone_name);

        return date($memberpref->date_style, $localtime);
    }

    public static function getLetterDateStyleFromUnix($ewoDate) {
        $memberpref = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

        $localtime = Timezone::model()->getLocalTimeFor($ewoDate, $memberpref->zone_name);

        return date("D F j, Y", $localtime);
    }

    public static function isEmpty($ewoDate) {
        if (($ewoDate === "0000-00-00 00:00:00") || ($ewoDate == false) || ($ewoDate === "Not Set"))
            return true;

        return false;
    }


    public static function datePickerLocalTimeToMysqlUTCTime($str_time) {
        $unixTime = DateFormat::datePickerToUnix($str_time);

        $memberpref = MemberPreferences::model()->getByMemberID(Yii::app()->user->id);

        $utcTime = Timezone::model()->getUTCTimeFromLocal($unixTime, $memberpref->zone_name);

        return date( 'Y-m-d H:i:s', $utcTime);
    }

    public static function convertToUsersTimezoneAsString($timeString, Member $member) {
        $as_unix =  DateFormat::convertToUsersTimezone($timeString, $member);

        return date( 'Y-m-d H:i:s', $as_unix);
    }

    public static function convertUnixToUsersTimezoneAsString($unixSeconds, Member $member) {
        $as_unix =  DateFormat::convertUnixToUsersTimezone($unixSeconds, $member);
        return date( 'Y-m-d H:i:s', $as_unix);
     }

    /**
     * Converts the time (String) into a unix time in the correct time zone.
     *
     * @param $timeString
     * @param Member $member
     * @return int|null
     */
    public static function convertToUsersTimezone($timeString, Member $member) {
        $memberpref = MemberPreferences::model()->getByMemberID($member->member_id);

        $timeSeconds = strtotime($timeString);
//        echo "Millis time = ".$timeMillies."<br>";
  //      echo "Member = ".$member->getFullName()."<br>";
    //    echo "Zone = ".$memberpref->zone_name."<br>";

        return Timezone::model()->getLocalTimeFor($timeSeconds, $memberpref->zone_name);
     }

    /**
     * Converts the unix time into a unix time in the user's selected time zone.
     *
     * @param $unixSeconds
     * @param Member $member
     * @return int|null
     */
    public static function convertUnixToUsersTimezone($unixSeconds, Member $member) {
         $memberpref = MemberPreferences::model()->getByMemberID($member->member_id);

         return Timezone::model()->getLocalTimeFor($unixSeconds, $memberpref->zone_name);
     }

    public static function showDateOnly($ewoDate) {
        if ($ewoDate === "0000-00-00 00:00:00")
            return "Not Set";
        return date('m/j/Y', strtotime($ewoDate));
    }

    public static function dateFormattedFor($date, $time) {
        return DateFormat::dateFormat($date." ".$time);
    }

    /**
     * Gets the renewal date for the given start time and the time now.
     *
     * @param $sqlFormatStartDate
     * @param null $secs
     * @param int $quantity
     * @return bool|string
     */
    public static function getRenewalDateFor($sqlFormatStartDate, $secs=null, $quantity=1) {
        if ($secs==null)
            $secs= DateFormat::$YEAR_SECS;

        $now = time();

        $start = strtotime($sqlFormatStartDate);

        $interval =$secs * $quantity ;

        if ($now>$start)
            $timeElapsed = $now-$start;
        else
            $timeElapsed = 0;

        $yearsFromStart = intval($timeElapsed / $interval);

        $nextDue = $yearsFromStart+1;

        return DateFormat::dateFormatFromUnix(($nextDue*$interval) + $start);
    }

    /**
     * Converts from a Date and Time mySQL entries to UTC from LA time to import from the old database.
     *
     * @param $date
     * @param $time
     * @return mixed
     */
    public static function mysqlDateAndTimeFromLAToUTCInMySql($date, $time) {
        if (!isset($date))
            return NULL;

        $datetime = DateFormat::mySQLDateAndTimeToMySQLDateTime($date, $time);

        if ($datetime== NULL)
            return NULL;

        return DateFormat::mysqlDateTimeFromLAToUTCInMySql($datetime);
    }

    /**
     * Converts from a DateTime mySQL entry to UTC from LA time to import from the old database.
     *
     * @param $datetime
     * @return mixed
     */
    public static function mysqlDateTimeFromLAToUTCInMySql($datetime) {
        $timeSeconds = DateFormat::mySQLToPhpDate($datetime);

        $now = time();

        $twentyYears = 60*60*24*365*20;

        if (($timeSeconds - $now) > $twentyYears) {
            // Ric's buzz lightyear trick
            $timeSeconds = $now + $twentyYears;
        }

        $newSecs = Timezone::model()->getUTCTimeFromLocal($timeSeconds); // defaults to LA

        return DateFormat::unixTimeToMySQLDateTime($newSecs);
    }

    public static function mySQLDateAndTimeToMySQLDateTime($date, $time) {
        if ($date === "0000-00-00") // no date, no point.
            return null;

        if ($date!=null) {
            $datesplit = explode("-",$date);
            if (isset($datesplit[0])) {
                $year = $datesplit[0];
                if (intval($year) > 2038)
                    $date = str_replace($year, "2038", $date);
            }
        }

        $datetime =NULL;

        if (!empty($date) && !empty($time))
            $datetime = $date." ".$time;
        else if (!empty($date))
            // add 12 hours to go to noon if no time is given. Avoids time zones moving it to the day before.
            $datetime = date( 'Y-m-d H:i:s', strtotime($date) + (60*60*12));
        else
            return NULL; // if we only have the time, what is the point ?

        if (DateFormat::isValidDateTime($datetime))
            return $datetime;
        else
            return NULL;
    }

    public static function isValidDateTime($dateTime)
    {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $dateTime, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1])) {
                return true;
            }
        }

        return false;
    }

    public static function unixTimeToMySQLDateTime($phpdate) {
        return date( 'Y-m-d H:i:s', $phpdate );
    }

    public static function anyDateToMySQLDateTime($anyDate) {
        if ($anyDate === "0000-00-00 00:00:00")
            return null;
        $unixTime = strtotime($anyDate);
        if ($unixTime<0)
            return null;

        return date( 'Y-m-d H:i:s', $unixTime );
    }


    public static function phpToMySQLDate($phpdate) {
        return date( 'Y-m-d', $phpdate );
    }

    public static function phpToMySQLTime($phpdate) {
        return date( 'H:i:s', $phpdate );
    }

    public static function mySQLTimeNow() {
        return date( 'H:i:s', time() );
    }

    public static function mySQLDateTimeNow() {
        return date( 'Y-m-d H:i:s', time() );
    }

    public static function mySQLDateNow() {
        return date( 'Y-m-d', time() );
    }

    private static function datePickerToMySQLDateTime($pickerTime) {
        return date( 'Y-m-d H:i:s', DateFormat::datePickerToUnix($pickerTime));
    }

    // 03/24/2014 05:17

    private static function datePickerToUnix($mySQLDateTime) {

        if ((!StringUtilities::contains($mySQLDateTime," ")) || (!StringUtilities::contains($mySQLDateTime,"/")) || (!StringUtilities::contains($mySQLDateTime,":")))
            $mySQLDateTime= DateFormat::formatStringForDatePickerFormat($mySQLDateTime);

        if (($mySQLDateTime === "1970-01-01 05:00:00") || ($mySQLDateTime === "01/01/1970 01:00"))
            $mySQLDateTime = DateFormat::getNowInDatePickerFormat();


        list($date, $time) = explode(' ', $mySQLDateTime);
        list($month, $day, $year) = explode('/', $date);
        list($hour, $minute) = explode(':', $time);

        $timestamp = mktime($hour, $minute, 0, $month, $day, $year);

        return $timestamp;
    }

    public static function getNowInDatePickerFormat() {
        // 03/24/2014 05:17
        return DateFormat::getUnixTimeInDatePickerFormat(time());
    }

    public static function getUnixTimeInDatePickerFormat($unixTime) {
        // 03/24/2014 05:17
        return date( 'm/d/Y H:i', $unixTime );
    }

    public static function formatStringForDatePickerFormat($str) {
        // 03/24/2014 05:17
        return date( 'm/d/Y H:i', strtotime($str));
    }

    /*
     * Function to turn a mysql datetime (YYYY-MM-DD HH:MM:SS) into a unix timestamp
     *
     * @param str
     *
     * The string to be formatted
     *
     */
    public static function mySQLToPhpDate($mySQLDateTime) {

        list($date, $time) = explode(' ', $mySQLDateTime);
        list($year, $month, $day) = explode('-', $date);
        list($hour, $minute, $second) = explode(':', $time);

        $timestamp = mktime($hour, $minute, $second, $month, $day, $year);

        return $timestamp;
    }

}