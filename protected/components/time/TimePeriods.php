<?php
/**
 * A simple class to do, dare I say it, Bank of America, style recurring payments. We limit the options to a few
 * to avoid having to get into the complex recurring logic like we have for the messages. So, the idea here is
 * to either do reports, invoices, etc, on a specific day for weekly or monthly options. They have to stick within a calender or
 * weekly time frame ... but they cannot send reports every 3 weeks or every 23rd day for example. This class is used
 * by the invoices right now but we should expand on this to do reporting etc later too.
 */
class TimePeriods {

    public static $WEEKLY=1;
    public static $MONTHLY=2;
    static $monthly_day_options;
    static $period_options;
    static $days;
    static $months;

    private static function init() {
        TimePeriods::$monthly_day_options = array();

        TimePeriods::$monthly_day_options[1] = "1st Day";
        TimePeriods::$monthly_day_options[2] = "2nd Day";
        TimePeriods::$monthly_day_options[3] = "3rd Day";

        for ($i=4; $i<21; ++$i)
            TimePeriods::$monthly_day_options[$i] = $i."th Day";

        TimePeriods::$monthly_day_options[21] = "21st Day";
        TimePeriods::$monthly_day_options[22] = "22nd Day";
        TimePeriods::$monthly_day_options[23] = "23rd Day";

        for ($i=24; $i<31; ++$i)
            TimePeriods::$monthly_day_options[$i] = $i."th Day";

        TimePeriods::$monthly_day_options[$i] = $i."st Day";

        TimePeriods::$period_options= array();

        TimePeriods::$period_options[TimePeriods::$WEEKLY] = "Weekly";
        TimePeriods::$period_options[TimePeriods::$MONTHLY] = "Monthly";

        TimePeriods::$days = array();

        TimePeriods::$days[0] = "Sunday";
        TimePeriods::$days[1] = "Monday";
        TimePeriods::$days[2] = "Tuesday";
        TimePeriods::$days[3] = "Wednesday";
        TimePeriods::$days[4] = "Thursday";
        TimePeriods::$days[5] = "Friday";
        TimePeriods::$days[6] = "Saturday";

        TimePeriods::$months = array();

        TimePeriods::$months[1] = "January";
        TimePeriods::$months[2] = "February";
        TimePeriods::$months[3] = "March";
        TimePeriods::$months[4] = "April";
        TimePeriods::$months[5] = "May";
        TimePeriods::$months[6] = "June";
        TimePeriods::$months[7] = "July";
        TimePeriods::$months[8] = "August";
        TimePeriods::$months[9] = "September";
        TimePeriods::$months[10] = "October";
        TimePeriods::$months[11] = "November";
        TimePeriods::$months[12] = "December";
    }

    public static function getPeriodOptions() {
        TimePeriods::init();
        return TimePeriods::$period_options;
    }

    public static function getMonthlyDayOptions() {
        TimePeriods::init();
        return TimePeriods::$monthly_day_options;
    }

    public static function getWeeklyDayOptions() {
        TimePeriods::init();
        return TimePeriods::$days;
    }

    public static function getDayAsStringFromIndex($index) {
        TimePeriods::init();

        return TimePeriods::$days[$index];
    }

    public static function getMonthAsStringFromIndex($index) {
        TimePeriods::init();

        return TimePeriods::$months[$index];
    }

    public static function getPeriodOptionAsStringFromIndex($index) {
        TimePeriods::init();
        return TimePeriods::$period_options[$index];
    }

    public static function getMonthlyDayAsStringFromIndex($index) {
        TimePeriods::init();
        return TimePeriods::$monthly_day_options[$index];
    }

    public static function getWeekDayIndexFromStringDate($date) {
        return date('w', strtotime($date));
    }

    public static function getWeekDayIndexFromUnixDate($date) {
        return date('w', $date);
    }

    public static function getWeekDayIndexNow() {
        return date('w', time());
    }

    public static function getWeekDayIndexFromSQLDateTime($date) {
        return date('w', DateFormat::mySQLToPhpDate($date));
    }

    public static function getDayInMonthFromStringDate($date) {
        return date('j', strtotime($date));
    }

    public static function getDayInMonthFromUnixDate($date) {
         return date('j', $date);
     }

     public static function getDayInMonthFromSQLDateTime($date) {
         return date('j', DateFormat::mySQLToPhpDate($date));
     }

    public static function getDayInMonthIndexNow() {
        return date('j', time());
    }

    /**
     * Returns true if the both times are contained within the same day.
     *
     * @param $date1
     * @param $date2
     * @return bool|string
     */
    public static function isTheSameDay($date1, $date2) {
        return date('Y-m-d', $date1) == date('Y-m-d', $date2);
    }

}