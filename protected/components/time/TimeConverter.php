<?php
/**
 * Converts from a number of formats into a data object.
 */
class TimeConverter
{

    /**
     * Takes a RFC822 String and returns an appropriate TimeObject returning the local time, not the UTC time
     *
     * @param $rfc822
     * @return TimeObject
     */
    public static function getLocalTimeFromRFC822Time($rfc822) {
        $date = new DateTime($rfc822);
        $timezonbeoffset = $date->getOffset();

        $rawtimeSeconds = strtotime($rfc822);

        // timezoneoffset is either + or - = must be added to get local time

        $localTimeSecs  = $rawtimeSeconds + $timezonbeoffset;

        return TimeConverter::getFromUnixTime($localTimeSecs);
    }

    /**
     * Takes a RFC822 String and returns an appropriate TimeObject returning UTC time equivalent
     *
     * @param $rfc822
     * @return TimeObject
     */
    public static function getUTCFromRFC822Time($rfc822) {

         $rawtimeSeconds = strtotime($rfc822);

         return TimeConverter::getFromUnixTime($rawtimeSeconds);
     }


    /**
     * Creates a TimeObject from a unix time.
     *
     * @param $unix
     * @return TimeObject
     */
    public static function getFromUnixTime($unix) {
        return new TimeObject($unix);
    }


    public static function getLocalUnixTimeFromUTCTimestamp($utctimestamp, $localTimezone) {
        date_default_timezone_set("UTC");

        $mytime = strtotime($utctimestamp);

        $localtime = new DateTime();
        $localtime->setTimestamp( $time=$mytime );

        $localtime->setTimeZone(new DateTimeZone($localTimezone));

        date_default_timezone_set(date_default_timezone_get());

        return date_format($localtime, 'm/d/y H:i:s');
    }
}
