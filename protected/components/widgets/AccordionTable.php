<?php
/**
 * A table list with an accordion dropdown, which is the third generation of this widget.  This
 * moves away from attempting to cluster IDs for specific entities and allows the view button
 * to be changed within the row metadata.  This should allow more generic use as you should be able to
 * simply pass a list of items to display.
 *
 */

class AccordionTable {

    protected $resources;
    protected $title;
    protected static $jqueryidcount=1;
    protected $myTablejqueryid;
    protected $myAccordionjqueryid;

    protected $baseURL;
    protected $totalItemsToView;
    protected $range;
    protected $rangeList;

    protected $createButtonControllerName;
    protected $createButtonActionName;
    protected $createButtonActionName2;
    protected $createButtonURL;
    protected $createButtonURL2;

    protected $rangeDropdownURL;

    protected $hasCreateButton = true;

    // This is all of the records not just the ones you are sending to view on this instance.
    protected $totalNumberOfRows;

    /** @var  $rangeobj Range */
    protected $rangeobj;

    function __construct($title, $resources, $totalItems, $scope_string_ids=null, $scope_ids=null, $createButtonController=NULL, $createButtonActionName="create",
                           $createButtonActionName2=null) {
        $this->title = $title;
        $this->baseURL = Yii::app()->request->baseUrl;

        $this->myTablejqueryid = "tablelist".AccordionTable::$jqueryidcount;
        $this->myAccordionjqueryid= "accordionlist".AccordionTable::$jqueryidcount;
        ++AccordionTable::$jqueryidcount;

        $this->createButtonActionName=$createButtonActionName;
        $this->createButtonControllerName=$createButtonController;

        if ($createButtonController==NULL) {
            $this->hasCreateButton=false;
        }
        if ($createButtonController == NULL)
            $createButtonController = StateVariables::getCurrentControllerName();

        $this->createButtonURL = $this->baseURL.'/'.$createButtonController.'/'.$createButtonActionName;

        if (!empty($createButtonActionName2)) {
            $this->createButtonActionName2 = $createButtonActionName2;
            $this->createButtonURL2 = $this->baseURL.'/'.$createButtonController.'/'.$createButtonActionName2;
        }

        // print_r($scope_string_ids);

        if (!empty($scope_string_ids)) { // if there is a scope, you will need this for all actions including create
            foreach ($scope_string_ids as $index => $scope_string_id) {
          //      echo "String id ".$scope_string_id."<br>";
                $scope_id = $scope_ids[$index];
            //    echo "scope id ".$scope_id."<br>";

                if (!empty($scope_id)) {
                    $this->createButtonURL .= '/'.$scope_string_id.'/'.$scope_id;
                    $this->createButtonURL2 .= '/'.$scope_string_id.'/'.$scope_id;
                }
            }
        }

        // this should be the calling URL i.e. the controller/action that is hosting this table view.

        $this->rangeDropdownURL = $this->baseURL."/".StateVariables::getCurrentControllerName()."/".StateVariables::getCurrentActionName();
        $this->rangeDropdownURL  = $this->rangeDropdownURL."?type=".urlencode($this->title)."&"."range=";
        $this->resources=$resources;

        $this->totalItemsToView = $totalItems;

        $baseurl = Pages::baseURL();

        $cs = Yii::app()->getClientScript();
        /** @var $cs CClientScript */

        $cs->registerCssFile($baseurl."vendor/bootstrap/css/bootstrap.css");
        $cs->registerCssFile($baseurl."vendor/data-tables/DT_bootstrap.css");
        $cs->registerCssFile($baseurl."css/accordion.css");
        $cs->registerCssFile($baseurl."vendor/data-tables/dataTables.tableTools.css");

        $cs->registerScriptFile($baseurl."vendor/bootstrap/js/bootstrap.js", CClientScript::POS_BEGIN );
        $cs->registerScriptFile($baseurl."vendor/data-tables/jquery.dataTables.js", CClientScript::POS_BEGIN);
        $cs->registerScriptFile($baseurl."vendor/data-tables/DT_bootstrap.js", CClientScript::POS_BEGIN );

        $cs->registerScript("tablelist".AccordionTable::$jqueryidcount, $this->getJS());

        $this->rangeobj = new Range($this->totalNumberOfRows, $this);
        $this->range = $this->rangeobj->getRangeString();

 //       echo $this->totalItems;
   //     exit;

        $numberOfPages = floor($this->totalNumberOfRows/Range::getUpperDefaultRange($this));
        $remainder = $this->totalNumberOfRows % Range::getUpperDefaultRange($this);

        if ($remainder>0)
            ++$numberOfPages;

        $this->rangeList = "";

        for ($i=0; $i<$numberOfPages; ++$i) {
            $startRange = $i*Range::getUpperDefaultRange($this);

            if ($i==($numberOfPages-1))
                $endrange = $startRange+$remainder;
            else
                $endrange = $startRange+Range::getUpperDefaultRange($this);

            ++$startRange;

            $thisRange = $startRange.'-'.$endrange;
            $this->rangeList.= '<li><a href="'.$this->rangeDropdownURL.$thisRange.'">'.$thisRange.'</a></li>';
        }
    }



    /**
     * @param mixed $totalNumberOfRows
     */
    public function setTotalNumberOfRows($totalNumberOfRows)
    {
        $this->totalNumberOfRows = $totalNumberOfRows;
    }

    /**
     * @return mixed
     */
    public function getTotalNumberOfRows()
    {
        return $this->totalNumberOfRows;
    }



    /**
     * @return mixed
     */
    public function getCreateButtonURL()
    {
        return $this->createButtonURL;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }


    private function convertActionName($actionName) {
        return ucfirst($actionName);

    }
    public function getWidget() {
        if (!isset($this->resources))
            return "No items available";

        $createButtonHtml = "";

        if ($this->hasCreateButton) {
            $createButtonHtml = '
                    <div class="btn-group">
                        <a class="btn" href="'.$this->createButtonURL.'"><i class="icon-plus"></i> '.$this->convertActionName($this->createButtonActionName).'</a>
<!-- Remove controller display name as it is already on the screen
                         <a class="btn" href="'.$this->createButtonURL.'"><i class="icon-plus"></i> '.$this->convertActionName($this->createButtonActionName).' '.TextFormat::getDisplayName($this->createButtonControllerName).'</a>
-->
                    </div>';
        }

        if (!empty($this->createButtonActionName2)) {
            $createButtonHtml .= '
                    <div class="btn-group">
                        <a class="btn" href="'.$this->createButtonURL2.'"><i class="icon-plus"></i> '.$this->convertActionName($this->createButtonActionName2).' '.TextFormat::getDisplayName($this->createButtonControllerName).'</a>
                    </div>';

        }

        return '

<div class="ui page">

            <div class="portlet-body" style="font-weight: 100">
                <table class="table table-bordered" id="'.$this->myTablejqueryid.'">
                    <thead>
                        <th>Select For Expanded View
                        </th>
                    </thead>
                    <tbody class="'.$this->myAccordionjqueryid.'">
                                '.$this->getRows($this->range).'
                    </tbody>
                </table>
            </div>
    </div>
			';
    }


    private function getRows($range) {
        $rows="";

        $lowerRange = $this->rangeobj->getLowerLimit();
        $upperRange = $this->rangeobj->getUpperLimit();

        if (!isset($this->resources)) return "No items available";

        foreach ($this->resources as $resource) {
                $rows.= '
                    '.$this->getRow($resource);
            }
        return $rows;
    }

    private function getRow($resource) {

        return '
                <tr>
                <td class="accordion">
                <div class="inforow">
                    <div id="hand" class="icon-plus" style="float:left; position :relative; padding-right:5px; padding-top:5px;"> </div>
                       '.$resource->getTitle().'
                     <div class="tools pull-right">'
                        .$this->getTools($resource).'
                     </div>
                     <div class="clearfix"></div>
                </div>
                <div class="showme well well-small" style="display:none;">
                    <div class="row-fluid">
                        <div class="span6">
                            '.$resource->getDescriptionLeft().'
                        </div>
                        <div class="span5">
                            '.$resource->getDescriptionRight().'
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                </td>
                </tr>
        ';

    }

    protected function getTools($resource) {

        if ($resource instanceof Article)
            $controller = "article";
        else
            $controller = "resource";

        $run = '';

        if ($controller == "resource") {
            /** @var  $resource Resource */
            $content_type = ContentType::model()->getByContentTypeID($resource->content_type_id);
            if (isset($content_type) && $content_type->name === "Source Code" )
                $run = '         <a href="'.$this->baseURL.'/resource/run/resource_id/'.$resource->id.'"> <div class="ui tiny red icon button"> Run <i class="play icon"></i></div></a>

            ';
        }

        return '
        '.$run.'
         <a href="'.$this->baseURL.'/'.$controller.'/'.$resource->id.'"> <div class="ui tiny icon basic button">View <i class="unhide icon"></i></div></a>
         <a href="'.$this->baseURL.'/'.$controller.'/update/id/'.$resource->id.'"> <div class="ui tiny icon basic button">Update <i class="edit icon"></i></div></a>
         <a href="'.$this->baseURL.'/'.$controller.'/delete/'.$controller.'_id/'.$resource->id.'"> <span class="ui tiny icon basic button" id="deleteme">Delete <i class="trash icon"></i></span></a>
        ';
    }


    protected function trimText($description) {

        $description = trim($description);

        if (strlen($description) > 80) {
            return substr($description,0,80).'...';
        } else {
            return $description;
        }
    }

    // le javascript

    private function getJS() {
        $baseURL = Pages::baseURL();
        $dtpath = $baseURL."vendor/data-tables/copy_csv_xls_pdf.swf";

        return
            '

          $(".'.$this->myAccordionjqueryid.' td.accordion").click(function(event){
              var target = $(event.target);
              if (target.is("div")) {
                  var current = $(this).find("#hand").attr("class");
                  if (current == "icon-plus")
                      $(this).find("#hand").attr("class", "icon-minus");
                  else
                      $(this).find("#hand").attr("class", "icon-plus");
                  var selection = $(this).find(".showme");
                  selection.fadeToggle(500);
              }
            });

            oTable = $(\'#'.$this->myTablejqueryid.'\').dataTable({
                            "bJQueryUI": true,
                            "bSort": false,
                            "bPaginate": false,
                            "oLanguage": {
                                "sSearch": "",
                                "sLengthMenu": "_MENU_",
                                "oPaginate": {
                                    "sPrevious": "Prev",
                                    "sNext": "Next"
                                    }
                                },
                                "aoColumnDefs": [{
                                    "bSortable": false,
                                     "aTargets": [0]
                                     }
                                ]
                            });

                $(".dataTables_filter input").attr("placeholder", "Search Displayed Entries...")

                $(\'#'.$this->myTablejqueryid.'group-checkable\').change(function () {
                        var set = jQuery(this).attr("data-set");
                        var checked = jQuery(this).is(":checked");
                        jQuery(set).each(function () {
                            if (checked) {
                                $(this).attr("checked", true);
                            } else {
                                $(this).attr("checked", false);
                            }
                        });
                        jQuery.uniform.update(set);
                    });

                    jQuery(\'#'.$this->myTablejqueryid.'_wrapper > div > div\').removeClass("span6"); // modify table search input

                    jQuery(\'#'.$this->myTablejqueryid.' .dataTables_filter input\').addClass("m-wrap small"); // modify table search input
                    jQuery(\'#'.$this->myTablejqueryid.' .dataTables_length select\').addClass("m-wrap small"); // modify table per page dropdown

                    $(document).on("click","#deleteme",function() {

                    	var item = $(this).parent().parent().parent().parent(); // the accordion class
                    	if(!confirm("Are you sure you want to delete item?")) {
                            // stopSpinner();
                            return false;
                        }
                    	var th=this;
                    	var afterDelete=function(){
                    	};
                        $.ajax({
                    		type:"POST",
                    		url:$(this).parent().attr("href"),
                    		success:function(data) {
                                item.remove();
                    			afterDelete(th,true,data);
                    		},
                    		error:function(XHR) {
                    			return afterDelete(th,false,XHR);
                    		}
                    	});
                    	return false;
                    });

                   $(\'#'.$this->myTablejqueryid.' tbody tr\').click( function( e ) {

                        if ( $(this).hasClass("row_selected") ) {
                            $(this).removeClass("row_selected");
                        }
                        else {
                            oTable.$("tr.row_selected").removeClass("row_selected");
                            $(this).addClass("row_selected");
                        }
                    });

              ';
    }

}