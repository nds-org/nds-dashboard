<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scmijt
 * Date: 18/09/2013
 * Time: 09:30
 * To change this template use File | Settings | File Templates.
 */

class Range {

    static $UPPER_DEFAULT_RANGE=500;  // Work Orders
    public static $UPPER_TABLE_DEFAULT_RANGE=1200; // Other

    static $UPPER_DEFAULT_RANGE_CELL=150; // Work Orders - Cellphone
    public static $UPPER_TABLE_DEFAULT_RANGE_CELL=200; // Others - Cellphone

    public static $LOWER_DEFAULT_RANGE=0;

    public $lowerLimit;
    public $upperLimit;

    public $range;
    public $totalItems;
    public $rangeString;

    /**
     * @return int
     */
    public static function getUpperDefaultRange($widget=null)
    {
        $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
        $android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
        $palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
        $berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
        $ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

        if ($android)
            $android = strpos($_SERVER['HTTP_USER_AGENT'],"Mobile");

        $Cellphone = false;
        if ($iphone || $android || $palmpre || $ipod || $berry == true)
            $Cellphone = true;

        if ($Cellphone)     {
            if  (!isset($widget)) {
                return self::$UPPER_DEFAULT_RANGE_CELL;
            } else if  ($widget instanceof TableAccordionList) {
                return self::$UPPER_TABLE_DEFAULT_RANGE_CELL;
            } else {
                return self::$UPPER_DEFAULT_RANGE_CELL;
            }
        } else {
            if  (!isset($widget)) {
                return self::$UPPER_DEFAULT_RANGE;
            } else if  ($widget instanceof TableAccordionList) {
                return self::$UPPER_TABLE_DEFAULT_RANGE;
            } else {
                return self::$UPPER_DEFAULT_RANGE;
            }
        }
    }

    function __construct($totalitems, $widget=null)
    {
        $this->totalItems=$totalitems;

        $searchString = StateVariables::getSearchString();

        if (!empty($searchString)) {
            $this->lowerLimit=0;
            $this->upperLimit=($this->totalItems>Range::getUpperDefaultRange())? Range::getUpperDefaultRange() : $this->totalItems;
        } else {
            /** @var $widget TableAccordionWidget */

            if (isset($_REQUEST['range']) && (isset($_REQUEST['type'])) && (isset($widget)) && ($_REQUEST['type'] === $widget->getTitle())) {
                $this->range = $_REQUEST['range'];
                $this->upperLimit=$this->getUpperRange($this->range);
                $this->lowerLimit=$this->getLowerRange($this->range);
            } else {
                $this->upperLimit=Range::getUpperDefaultRange($widget);
                $this->lowerLimit=Range::$LOWER_DEFAULT_RANGE;
            }

           if ($this->upperLimit > $this->totalItems)
                   $this->upperLimit = $this->totalItems;
        }

        if (($this->upperLimit > 0) && ($this->lowerLimit != 0))
            $this->rangeString = $this->lowerLimit."-".$this->upperLimit;
        else if (($this->upperLimit > 0) && ($this->lowerLimit ==0))
            $this->rangeString = "1-".$this->upperLimit;
        else
            $this->rangeString = "0";

    }


    /**
     * @return mixed
     */
    public function getLowerLimit()
    {
        return $this->lowerLimit;
    }

    /**
     * @return mixed
     */
    public function getRange()
    {
        return $this->range;
    }

    /**
     * @return mixed
     */
    public function getRangeString()
    {
        return $this->rangeString;
    }

    /**
     * @return mixed
     */
    public function getTotalItems()
    {
        return $this->totalItems;
    }

    /**
     * @return mixed
     */
    public function getUpperLimit()
    {
        return $this->upperLimit;
    }

    private function getLowerRange($range) {
        $limits = explode("-", $range);

        return intval($limits[0]);
    }

    private function getUpperRange($range) {
        $limits = explode("-", $range);

        return intval($limits[1]);
    }

}