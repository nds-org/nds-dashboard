<?php
/**
 * Created by PhpStorm.
 * User: scmijt
 * Date: 07/10/2014
 * Time: 17:19
 */

class Breadcrumbs {

    public static function myBreadcrumbs($crumbs) {
        $count = count($crumbs);

        $rows = "";
        $i=0;
        foreach ($crumbs as $name => $act_cont) {
            if (is_array($act_cont) && (count($act_cont) == 1))
                $vals = trim($act_cont[0]);
            else if (is_array($act_cont) && (count($act_cont) == 0))
                $vals = "";
            else
                $vals = $act_cont;


//            echo $vals;
  //          print_r($act_cont);

            if ($vals === "index") {
                $href = "#";
            } else if (is_array($vals))
                $href = Yii::app()->baseUrl."/".array_shift($vals)."/".array_shift($vals);
            else if (StringUtilities::contains($vals, "/"))
                $href = Yii::app()->baseUrl."/".$vals;
            else
                $href = Yii::app()->baseUrl."/".StateVariables::getCurrentControllerName()."/".$vals;

            ++$i;

            if ($i == $count)
                $rows .= Breadcrumbs::getRow($href, $name, true);
            else
                $rows .= Breadcrumbs::getRow($href, $name);
        }

        return '
        <div style="padding-bottom: 4px;">
        </div>

         <div class="ui breadcrumb">
             '.$rows.'
         </div>
         ';

    }

    private static function getRow($href, $name, $active=false) {
        if ($active)
            return '<a class="active section">'.$name.'</a>';

        return '
          <a href = "'.$href.'" class="section">'.$name.'</a>
          <div class="divider"> / </div>
        ';
    }

} 