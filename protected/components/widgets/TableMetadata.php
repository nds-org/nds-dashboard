<?php
/**
 * User: scmijt
 */

class TableMetadata {
    private $id;
    private $image;
    private $parameters;
    private $viewButtonURL;

    function __construct($parameters, $id, $image, $viewButtonURL=null)
    {
        $this->parameters = $parameters;
        $this->id = $id;
        $this->image = $image;
        $this->viewButtonURL=$viewButtonURL;
    }

    /**
     * @return mixed
     */
    public function getParameters()
    {
        return $this->parameters;
    }


    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null
     */
    public function getViewButtonURL()
    {
        return $this->viewButtonURL;
    }


}