<?php
/**
 * A table to show the following columns:
 *
Work Ord
Updated
Required
Problem
Urgency
Tenant
Description

 * but cut down to:
 *
Work Ord
Updated
Required
Problem
Urgency
Tenant
Description
 *
 */

class ResourceFormWidget extends TableList {

    private $article_id;
    private $data_items;
    private $resources;
    /** @var  $controller CController */
    private $controller;
    private $form;

    /**
     * @param $title
     * @param $controller
     * @param $form
     * @param $article_id
     * @param $resource_list
     */
    function __construct($title, $controller, $form, $article_id, $resource_list)
     {
         parent::__construct($title);

         $this->data_items = array();
         $this->resources = array();

         $this->article_id=$article_id;
         $this->controller = $controller;
         $this->form = $form;

         if (empty($resource_list))
             return;

         foreach ($resource_list as $file_loc => $image) {
             $resource = Resource::model()->getByResourceLocation($file_loc);
             $this->resources[] = $resource;
             $this->data_items[] = $image;
         }
     }


    function getThumbnail($image) {
          return '<div class="ui medium image">
          <img src="'.$image.'"></div>';
    }

    public function getNumberOfResources() {
        return count($this->data_items);
    }


    public function getContent($controllerName="screen", $idCallbackString="id") {
        $rows=array();

        $layout =  $this->controller->layout;
        $this->controller->layout="//layouts/none";

        if (!isset($this->data_items)) return "No items available";

        $totalItems = count($this->data_items);
        $rangeobj = new Range($totalItems);

        $lowerRange = $rangeobj->getLowerLimit();
        $upperRange = $rangeobj->getUpperLimit();

        $j=0;
        for ($i = $lowerRange; $i < $upperRange; $i++) {
            $resource = $this->resources[$i];
            $image = $this->data_items[$i];
            /** @var $resource Resource */

            if (isset($resource)) {


                $parameters = $this->controller->render("/resource/_form",
                        array("model" => $resource, "form" => $this->form, "resource" => $resource),
                        true).'
                ';

                $imageref = '<div class="ui header">Data Item '.($j+1).'</div>';

                if ($resource->content_type_id == ContentType::$URL)
                    $imageref .= " " .$this->getThumbnail(Yii::app()->request->baseUrl."/images/icons/http.png");
                else
                    $imageref .= " " .$this->getThumbnail($image);

                $rows[$j] = new TableMetadata($parameters, $resource->id, $imageref);

                ++$j;
            }
        }

        $this->controller->layout=$layout;

        parent::init($rows, null, null, $controllerName);
        $widget = parent::getWidget();

        return $widget;
    }



}