<?php
/**
 * Created by PhpStorm.
 * User: scmijt
 * Date: 26/09/2014
 * Time: 10:21
 */

class SemanticWidgets {

        public static function getDropDown(CActiveRecord $model_class, $column, $topics, $name=null, $id=null) {

            $model = get_class($model_class);

            if (!isset($name))
                $name = $model.'['.$column.']';

            if (!isset($id))
                $id = $model.'_'.$column;

            $current_value = $model_class->getAttribute($column);

            $vals = "";

            if (!empty($current_value)) {
                $default_text = $topics[$current_value];
                $default_val = $current_value;
            } else {
                // get first one - do not attempt indexes as this is an associative array
                $default_text = array_values($topics)[0];
                $default_val = 0;
                }

            foreach ($topics as $key => $val) {
                $vals .='<div class="item" data-value="'.$key.'">'.$val.'</div>
                ';
            }

            return '

             <div class="ui selection dropdown">
          <input  type="hidden" name="'.$name.'" id="'.$id.'" value="'.$default_val.'">
          <div class="default text">'.$default_text.'</div>
          <i class="dropdown icon"></i>
          <div class="menu">
          '.$vals.'
          </div>
        </div>
        ';

        }


} 