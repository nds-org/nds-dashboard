<?php
/**
 * A table list with an accordion dropdown, which is the third generation of this widget.  This
 * moves away from attempting to cluster IDs for specific entities and allows the view button
 * to be changed within the row metadata.  This should allow more generic use as you should be able to
 * simply pass a list of items to display.
 *
 */

abstract class TableList {

    protected $objects;
    protected $title;
    protected static $jqueryidcount=1;
    protected $myTablejqueryid;
    protected $myAccordionjqueryid;

    protected $baseURL;
    protected $totalItems;
    protected $range;
    protected $rangeList;

    protected $createButtonControllerName;
    protected $createButtonActionName;
    protected $createButtonActionName2;
    protected $createButtonURL;
    protected $createButtonURL2;

    protected $rangeDropdownURL;

    protected $hasCreateButton = true;

    /** @var  $rangeobj Range */
    protected $rangeobj;

    function __construct($title)
    {
        $this->title = $title;
        $this->baseURL = Yii::app()->request->baseUrl;
    }


    /**
     * @return mixed
     */
    public function getCreateButtonURL()
    {
        return $this->createButtonURL;
    }


    /**
     * There are two identifiers actions supported:
     *
     * 1 - the scope_id_string and scope_id, used if you need more parameters to be passed before targetting the actual object from the list that
     * you want to enter within e.g. a building_id is needed sometimes.
     * 2 - the view_id_string - the identifier string for the view button
     */
    protected function init($rows, $scope_string_ids=null, $scope_ids=null, $createButtonController=NULL, $createButtonActionName="create",
                           $createButtonActionName2=null) {
        $this->myTablejqueryid = "tablelist".TableList::$jqueryidcount;
        $this->myAccordionjqueryid= "accordionlist".TableList::$jqueryidcount;
        ++TableList::$jqueryidcount;

        $this->createButtonActionName=$createButtonActionName;
        $this->createButtonControllerName=$createButtonController;

        if ($createButtonController==NULL) {
            $this->hasCreateButton=false;
        }
        if ($createButtonController == NULL)
            $createButtonController = StateVariables::getCurrentControllerName();

        $this->createButtonURL = $this->baseURL.'/'.$createButtonController.'/'.$createButtonActionName;

        $this->hasCreateButton=false;

        if (!empty($createButtonActionName2)) {
            $this->createButtonActionName2 = $createButtonActionName2;
            $this->createButtonURL2 = $this->baseURL.'/'.$createButtonController.'/'.$createButtonActionName2;
        }

        // print_r($scope_string_ids);

        if (!empty($scope_string_ids)) { // if there is a scope, you will need this for all actions including create
            foreach ($scope_string_ids as $index => $scope_string_id) {
          //      echo "String id ".$scope_string_id."<br>";
                $scope_id = $scope_ids[$index];
            //    echo "scope id ".$scope_id."<br>";

                if (!empty($scope_id)) {
                    $this->createButtonURL .= '/'.$scope_string_id.'/'.$scope_id;
                    $this->createButtonURL2 .= '/'.$scope_string_id.'/'.$scope_id;
                }

            }
        }

        // this should be the calling URL i.e. the controller/action that is hosting this table view.

        $this->rangeDropdownURL = $this->baseURL."/".StateVariables::getCurrentControllerName()."/".StateVariables::getCurrentActionName();
        $this->rangeDropdownURL  = $this->rangeDropdownURL."?range=";
        $this->objects=$rows;

        $this->totalItems = count($rows);

        $this->rangeobj = new Range($this->totalItems);
        $this->range = $this->rangeobj->getRangeString();


        $cs = Yii::app()->getClientScript();
        /** @var $cs CClientScript */

        $cs->registerCssFile($this->baseURL."/vendor/bootstrap/css/bootstrap.css");
        $cs->registerCssFile($this->baseURL."/vendor/data-tables/DT_bootstrap.css");
        $cs->registerCssFile($this->baseURL."/css/accordion.css");

        $cs->registerScriptFile($this->baseURL."/vendor/bootstrap/js/bootstrap.js", CClientScript::POS_BEGIN );
        $cs->registerScriptFile($this->baseURL."/vendor/data-tables/jquery.dataTables.js", CClientScript::POS_BEGIN);
        $cs->registerScriptFile($this->baseURL."/vendor/data-tables/DT_bootstrap.js", CClientScript::POS_BEGIN );

        $cs->registerScript("tablelist".TableList::$jqueryidcount, $this->getJS());

        $numberOfPages = floor($this->totalItems/Range::$UPPER_DEFAULT_RANGE);
        $remainder = $this->totalItems % Range::$UPPER_DEFAULT_RANGE;

        if ($remainder>0)
            ++$numberOfPages;

        $this->rangeList = "";

        for ($i=0; $i<$numberOfPages; ++$i) {
            $startRange = $i*Range::$UPPER_DEFAULT_RANGE;

            if ($i==($numberOfPages-1))
                $endrange = $startRange+$remainder;
            else
                $endrange = $startRange+Range::$UPPER_DEFAULT_RANGE;

            ++$startRange;

            $thisRange = $startRange.'-'.$endrange;
            $this->rangeList.= '<li><a href="'.$this->rangeDropdownURL.$thisRange.'">'.$thisRange.'</a></li>';
        }
    }


    private function convertActionName($actionName) {
        return ucfirst($actionName);

    }
    protected function getWidget() {
        if (!isset($this->objects))
            return "No items available";


        return '

<div class="ui fluid basic segment">
        <table class="table" id="'.$this->myTablejqueryid.'" width="100%">
            <thead>
                <th><h2>Describe Your Data</h2></th>
            </thead>
            <tbody class="'.$this->myAccordionjqueryid.'">
                        '.$this->getRows($this->range).'
            </tbody>
        </table>
</div>
			';
    }


    private function getRows($range) {
        $rows="";

        $lowerRange = $this->rangeobj->getLowerLimit();
        $upperRange = $this->rangeobj->getUpperLimit();

        if (!isset($this->objects)) return "No items available";

        for ($i = $lowerRange; $i < $upperRange; $i++) {
            $object = $this->objects[$i];
            $rows.= '
                '.$this->getRow($object);
        }
        return $rows;
    }

    private function getRow(TableMetadata $object) {

        return '
                <tr>
                <td>
                    <div class="ui stackable fluid grid">
                            <div class="four wide column">
                                '.$object->getImage().'
                            </div>
                            <div class="twelve wide column">
                               '.$object->getParameters().'
                            </div>
                    </div>
                </td>
                </tr>
        ';

    }

    protected function getTools(TableMetadata $object) {
        return '
         <a href="'.$object->getViewButtonURL().'"> <div class="btn btn-mini"> <span class="icon-eye-open icon-large"></span> View</div></a>
        ';
    }


    protected function trimText($description) {

        $description = trim($description);

        if (strlen($description) > 80) {
            return substr($description,0,80).'...';
        } else {
            return $description;
        }
    }

    // le javascript

    private function getJS() {
        return
            '
            oTable = $(\'#'.$this->myTablejqueryid.'\').dataTable({
                "bJQueryUI": true,
                "bSort": false,
                "bPaginate": false,
                "oLanguage": {
                    "sSearch": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                        }
                    },
                    "aoColumnDefs": [{
                        "bSortable": false,
                         "aTargets": [0]
                         }
                    ]
                });

                $(".dataTables_filter input").attr("placeholder", "Search Here...")




                    $(document).on("click","#deleteme",function() {
                    	var item = $(this).parent().parent().parent(); // the accordion class
                    	var itemName = item.find(".badge").html();
                    	if(!confirm("Are you sure you want to delete " + itemName + "?")) return false;
                    	var th=this;
                    	var afterDelete=function(){
                    	};
                        $.ajax({
                    		type:"POST",
                    		url:$(this).attr("href"),
                    		success:function(data) {
                                item.remove();
                    			afterDelete(th,true,data);
                    		},
                    		error:function(XHR) {
                    			return afterDelete(th,false,XHR);
                    		}
                    	});
                    	return false;
                    });

              ';
    }

}