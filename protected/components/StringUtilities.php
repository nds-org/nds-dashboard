<?php
class StringUtilities {

    /**
     * Returns true if the haystack starts with the needle.
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle)
     {
         $length = strlen($needle);
         return (substr($haystack, 0, $length) === $needle);
     }

    public static function endsWith($haystack, $needle) {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }

    /**
     * Java ...
     *
     * @static
     * @param $haystack
     * @param $needle
     * @return int
     */
    public static function lastIndexOf($haystack, $needle) {
        return strripos($haystack, $needle, -1);
    }

    public static function indexOf($haystack, $needle) {
        return strpos($haystack, $needle);
    }

    /**
         * Returns the string after the occurrence of the last index of the $needle.
         * @param $haystack
         * @param $needle
         * @return bool
         */
    public static function substringAfter($haystack, $needle)
   {
       $pos=StringUtilities::indexOf($haystack, $needle);

       if ($pos !== false)
           return substr($haystack, $pos+strlen($needle));
       else
           return FALSE;
   }

    /**
          * Returns the string after the occurence of the last index of the $needle.
          * @param $haystack
          * @param $needle
          * @return bool
          */
     public static function substringBefore($haystack, $needle)
    {
        $pos=StringUtilities::indexOf($haystack, $needle);

        if ($pos !== false)
            return substr($haystack, 0, $pos-1);
        else
            return FALSE;
    }


    /**
         * Returns the string after the occurence of the last index of the $needle.
         * @param $haystack
         * @param $needle
         * @return bool
         */
    public static function substringAfterLast($haystack, $needle)
   {
       $pos=StringUtilities::lastIndexOf($haystack, $needle);

       if ($pos !== false)
           return substr($haystack, $pos+strlen($needle));
       else
           return FALSE;
   }



     /**
          * Returns the string after the occurence of the last index of the $needle.
          * @param $haystack
          * @param $needle
          * @return bool
          */
     public static function substringBeforeLast($haystack, $needle)
    {
        $pos=StringUtilities::lastIndexOf($haystack, $needle);

        if ($pos !== false)
            return substr($haystack, 0, $pos);
        else
            return FALSE;
    }
     public static function equals($string1, $string2) {
         if (strcmp($string1, $string2) == 0) return true;
         return false;
     }


     public static function contains($haystack, $needle) {
         if (strpos($haystack, $needle) !== FALSE)
             return true;
         else
             echo false;
     }

     public static function removewhitespace($string) {
           return str_replace(" ", "", $string);
       }

}

