<?php

/**
 * An interface to an underlying weather API. Currently we use wunderground.com, but this can be swapped out
 * later if we choose.
 */
class Weather
{
    private $baseurl = "http://api.wunderground.com/api/";
    private $key = "222f1ef1782d2474/"; // EWO key
    private $json_string; // RAW JSON

    /** @var WeatherConditions $weatherConditions */
    private $weatherConditions;
    private $features = 'conditions/forecast/';
    private $error;

    function __construct() {

    }

    function initUsingIPAddress($ipaddress) {
        $this->json_string = $this->getURLContents($this->baseurl.$this->key.$this->features."q/"."autoip.json?geo_ip=".$ipaddress);
        $parsed_json = json_decode($this->json_string);
        $this->weatherConditions= new WeatherConditions($parsed_json);
    }

    /**
     *
        The location for which you want weather information. Examples:
        CA/San_Francisco	US state/city
        60290	US zipcode
        Australia/Sydney	country/city
        37.8,-122.4	latitude,longitude
        KJFK	airport code
        pws:KCASANFR70	PWS id
        autoip	AutoIP address location
        autoip.json?geo_ip=38.102.136.138	specific IP address location
     * @param $string
     * @return true if succeeded
     */
    function init($string) {
        $request = $this->baseurl.$this->key.$this->features."q/".$string.".json";
       // echo "<br>Request is ".$request."<br>";

        try {
            $this->json_string = $this->getURLContents($request);
        } catch (Exception $e) {
            $this->error=$e->getMessage();
        }

        if ((isset($this->json_string)) && (!isset($this->error))) {
            $parsed_json = json_decode($this->json_string);
            $this->weatherConditions = new WeatherConditions($parsed_json);
            return true;
        }

        return false;
    }

    function initLatLong($lat, $long) {
        if (empty($lat))
            return false;

        return $this->init($lat.",".$long);
    }



     public function initUsingJson($json) {
         $this->json_string = $json;
         $parsed_json = json_decode($this->json_string);
         $this->weatherConditions = new WeatherConditions($parsed_json);
     }

    public function getJsonString()
    {
        return $this->json_string;
    }


    /**
     * @return \WeatherConditions
     */
    public function getWeatherConditions()
    {
        return $this->weatherConditions;
    }


    public function getURLContents($url, array $options = array()) {
            $defaults = array(
                CURLOPT_URL => $url,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_TIMEOUT => 4
            );

            $ch = curl_init();
            curl_setopt_array($ch, ($options + $defaults));
            if( ! $result = curl_exec($ch))
            {
                $this->error = curl_error($ch);
            }
            curl_close($ch);
            return $result;
        }

    /**
     * @return null
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param null $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }



    public function getWeatherSnapShot(Building $building) {
        if (empty($this->weatherConditions))
            return "<br>Weather not available at this time...";
        return '
            <div class="row-fluid">
                   <div class="span2">
                        <div style="font-size: 120%; margin:5px; padding:5px; text-align: center;">Weather</div>
                        <div style="width: 50px; height:50px; margin: auto">
                         <img src="'.$this->weatherConditions->getIconUrl().'" style="width: 50px; height:50px;" width=50 height=50>
                         </div>
                   </div>
                   <div class="span4">
                    <div class="nice-text">'.
                        TextFormat::getBackToBackTwoColumn('Temperature',$this->weatherConditions->getTempF(),85).
                        TextFormat::getBackToBackTwoColumn('Feels Like ',$this->weatherConditions->getFeelslikeF(),85).
                        TextFormat::getBackToBackTwoColumn('Conditions ',$this->weatherConditions->getWeather(),85).'
                   </div></div>
                   <div class="span4">
                    <div class="nice-text">'.
                        TextFormat::getBackToBackTwoColumn('Day Low',$this->weatherConditions->getDayLowTemperature()).
                        TextFormat::getBackToBackTwoColumn('Day High',$this->weatherConditions->getDayHighTemperature()).'
                   </div></div>
                   <div class="span2">
                    <div class="nice-text">
                        <strong>
                        <a href="'.$this->weatherConditions->getForecastUrl().'" target="_blank">Goto Weather Forecast</a>
                        </strong>
                   </div></div>
               <div class="clearfix"></div>
           </div>
        ';
    }

}
