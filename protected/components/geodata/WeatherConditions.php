<?php
/**
 * Parses the output from Wunderground and exposes the values as class variables.
 */
class WeatherConditions
{

    private $elevation;
    private $longitude;
    private $latitude;
    private $zip;
    private $heat_index_f;
    private $weather;
    private $local_time_rfc822;
    private $pressure_in;
    private $dewpoint_f;
    private $precip_today_metric;
    private $feelslike_f;
    private $feelslike_c;
    private $wind_mph;
    private $wind_gust_mph;
    private $temp_f;
    private $temp_c;

    private $visibility_mi;

    private $pressure_mb;
    private $wind_dir;
    private $uv;
    private $relative_humidity;

    /** @var $localTimeForObservation TimeObject */
    private $localTimeForObservation;

    private $location;
    private $wind_degrees;

    private $dayHighTemperature;
    private $dayLowTemperature;

    private $rfcTime;
    private $errorType;
    private $errorDescription;
    private $txOffset;
    private $timeZone;

    private $icon_url;
    private $forecast_url;

    function __construct($weatherjson)
    {
       // print_r($weatherjson);
        $this->elevation = $weatherjson->{'current_observation'}->{'observation_location'}->{'elevation'};
        $this->longitude = $weatherjson->{'current_observation'}->{'observation_location'}->{'longitude'};
        $this->latitude = $weatherjson->{'current_observation'}->{'observation_location'}->{'latitude'};
        $this->zip = $weatherjson->{'current_observation'}->{'display_location'}->{'zip'};

        $this->rfcTime =$weatherjson->{'current_observation'}->{'local_time_rfc822'};
        $this->txOffset =$weatherjson->{'current_observation'}->{'local_tz_offset'};
        $this->timeZone =$weatherjson->{'current_observation'}->{'local_tz_long'};

        $this->localTimeForObservation = TimeConverter::getLocalTimeFromRFC822Time($this->rfcTime);

        $this->heat_index_f = $weatherjson->{'current_observation'}->{'heat_index_f'};
        $this->weather = $weatherjson->{'current_observation'}->{'weather'}; # e.g. a str valing like "Mostly Cloudy"
        $this->local_time_rfc822 = $weatherjson->{'current_observation'}->{'local_time_rfc822'};
        $this->pressure_in = $weatherjson->{'current_observation'}->{'pressure_in'};
        $this->dewpoint_f = $weatherjson->{'current_observation'}->{'dewpoint_f'};
        $this->precip_today_metric = $weatherjson->{'current_observation'}->{'precip_today_metric'};
        $this->feelslike_f = $weatherjson->{'current_observation'}->{'feelslike_f'};
        $this->feelslike_c = $weatherjson->{'current_observation'}->{'feelslike_c'};
        $this->wind_mph = $weatherjson->{'current_observation'}->{'wind_mph'};
        $this->wind_gust_mph = $weatherjson->{'current_observation'}->{'wind_gust_mph'};
        $this->temp_f = $weatherjson->{'current_observation'}->{'temp_f'};
        $this->temp_c = $weatherjson->{'current_observation'}->{'temp_c'};
        $this->visibility_mi = $weatherjson->{'current_observation'}->{'visibility_mi'};
        $this->pressure_mb = $weatherjson->{'current_observation'}->{'pressure_mb'};
        $this->wind_dir = $weatherjson->{'current_observation'}->{'wind_dir'};
        $this->wind_degrees = $weatherjson->{'current_observation'}->{'wind_degrees'};
        $this->uv = $weatherjson->{'current_observation'}->{'UV'};

        $this->forecast_url = $weatherjson->{'current_observation'}->{'forecast_url'};
        $this->icon_url = $weatherjson->{'current_observation'}->{'icon_url'};

        $this->location = $weatherjson->{'current_observation'}->{'display_location'}->{'full'};

        $humidity_percent = $weatherjson->{'current_observation'}->{'relative_humidity'};

        $this->relative_humidity = StringUtilities::substringBefore($humidity_percent,"%");

        $this->dayHighTemperature = $weatherjson->{'forecast'}->{'simpleforecast'}->{'forecastday'}[0]->{'high'}->{'fahrenheit'};
        $this->dayLowTemperature = $weatherjson->{'forecast'}->{'simpleforecast'}->{'forecastday'}[0]->{'low'}->{'fahrenheit'};

 //       $this->errorType->{'error'}->{'type'};
   //     $this->errorDescription->{'error'}->{'description'};
    }

    public function isValid() {
        $location = $this->getLocation();
        if (empty($location)) return false;
        return true;
    }

    function toString() {
        $output="";
        $output.="Elevation = ".strval($this->elevation)."<br>";
        $output.="Longitude = ".strval($this->longitude)."<br>";
        $output.="Latitude = ".strval($this->latitude)."<br>";
        $output.="Location = ".strval($this->location)."<br>";
        $output.="Zip Code = ".strval($this->zip)."<br>";
        $output.="Heat Index Farenheit = ".strval($this->heat_index_f)."<br>";
        $output.="Weather = ".strval($this->weather)."<br>";
        $output.="Local Time = ".strval($this->local_time_rfc822)."<br>";
        $output.="Dewpoint Farenheit = ".strval($this->dewpoint_f)."<br>";
        $output.="Precipitation Today = ".strval($this->precip_today_metric)."<br>";
        $output.="Feels Like Farenheit = ".strval($this->feelslike_f)."<br>";
        $output.="Wind Speed MPH = ".strval($this->wind_mph)."<br>";
        $output.="Wind Gust MPH = ".strval($this->wind_gust_mph)."<br>";
        $output.="Temperature Farenheit = ".strval($this->temp_f)."<br>";
        $output.="Visibility in Miles = ".strval($this->visibility_mi)."<br>";
        $output.="Pressure in Inches = ".strval($this->pressure_in)."<br>";
        $output.="Pressure MB = ".strval($this->pressure_mb)."<br>";
        $output.="Wind Direction = ".strval($this->wind_dir)."<br>";
        $output.="Ultaviolet Radiation = ".strval($this->uv)."<br>";
        $output.="Relative Humidity = ".strval($this->relative_humidity)."<br>";
        $output.="Local Time = ".$this->localTimeForObservation->getNiceTime()."<br>";

        $output.="Daily High Temperature= ".$this->dayHighTemperature."<br>";
        $output.="Daily Low Temperature= ".$this->dayLowTemperature."<br>";

   //     $output.="Error Type= ".strval($this->errorType)."<br>";
     //   $output.="Error Description= ".strval($this->errorDescription)."<br>";

        return $output;
    }

    public function getDewpointF()
    {
        return $this->dewpoint_f;
    }

    public function getElevation()
    {
        return $this->elevation;
    }

    public function getFeelslikeF()
    {
        return $this->feelslike_f;
    }

    public function getHeatIndexF()
    {
        return $this->heat_index_f;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLocalTimeRfc822()
    {
        return $this->local_time_rfc822;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getPrecipTodayMetric()
    {
        return $this->precip_today_metric;
    }

    public function getPressureIn()
    {
        return $this->pressure_in;
    }

    public function getPressureMb()
    {
        return $this->pressure_mb;
    }



    public function getTempF()
    {
        return $this->temp_f;
    }

    public function getUv()
    {
        return $this->uv;
    }

    public function getVisibilityMi()
    {
        return $this->visibility_mi;
    }

    public function getWeather()
    {
        return $this->weather;
    }

    public function getWindDir()
    {
        return $this->wind_dir;
    }

    public function getWindGustMph()
    {
        return $this->wind_gust_mph;
    }

    public function getWindMph()
    {
        return $this->wind_mph;
    }



    public function getZip()
    {
        return $this->zip;
    }

    public function getDayHighTemperature()
    {
        return $this->dayHighTemperature;
    }

    public function getDayLowTemperature()
    {
        return $this->dayLowTemperature;
    }

    /**
     * @return \TimeObject
     */
    public function getLocalTimeForObservation()
    {
        return $this->localTimeForObservation;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function getWindDegrees()
    {
        return $this->wind_degrees;
    }

    public function getRfcTime()
    {
        return $this->rfcTime;
    }

    public function setDayHighTemperature($dayHighTemperature)
    {
        $this->dayHighTemperature = $dayHighTemperature;
    }

    public function setDayLowTemperature($dayLowTemperature)
    {
        $this->dayLowTemperature = $dayLowTemperature;
    }

    public function setDewpointF($dewpoint_f)
    {
        $this->dewpoint_f = $dewpoint_f;
    }

    public function setElevation($elevation)
    {
        $this->elevation = $elevation;
    }

    public function setErrorDescription($errorDescription)
    {
        $this->errorDescription = $errorDescription;
    }

    public function setErrorType($errorType)
    {
        $this->errorType = $errorType;
    }

    public function setFeelslikeF($feelslike_f)
    {
        $this->feelslike_f = $feelslike_f;
    }

    public function setHeatIndexF($heat_index_f)
    {
        $this->heat_index_f = $heat_index_f;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @param \TimeObject $localTimeForObservation
     */
    public function setLocalTimeForObservation($localTimeForObservation)
    {
        $this->localTimeForObservation = $localTimeForObservation;
    }

    public function setLocalTimeRfc822($local_time_rfc822)
    {
        $this->local_time_rfc822 = $local_time_rfc822;
    }

    public function setLocation($location)
    {
        $this->location = $location;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    public function setPrecipTodayMetric($precip_today_metric)
    {
        $this->precip_today_metric = $precip_today_metric;
    }

    public function setPressureIn($pressure_in)
    {
        $this->pressure_in = $pressure_in;
    }

    public function setPressureMb($pressure_mb)
    {
        $this->pressure_mb = $pressure_mb;
    }

    public function setRelativeHumidity($relative_humidity)
    {
        $this->relative_humidity = $relative_humidity;
    }

    public function setRfcTime($rfcTime)
    {
        $this->rfcTime = $rfcTime;
    }

    public function setSunriseHour($sunrise_hour)
    {
        $this->sunrise_hour = $sunrise_hour;
    }

    public function setSunriseMin($sunrise_min)
    {
        $this->sunrise_min = $sunrise_min;
    }

    public function setSunsetHour($sunset_hour)
    {
        $this->sunset_hour = $sunset_hour;
    }

    public function setSunsetMin($sunset_min)
    {
        $this->sunset_min = $sunset_min;
    }

    public function setTempF($temp_f)
    {
        $this->temp_f = $temp_f;
    }

    public function setUv($uv)
    {
        $this->uv = $uv;
    }

    public function setVisibilityMi($visibility_mi)
    {
        $this->visibility_mi = $visibility_mi;
    }

    public function setWeather($weather)
    {
        $this->weather = $weather;
    }

    public function setWindDegrees($wind_degrees)
    {
        $this->wind_degrees = $wind_degrees;
    }

    public function setWindDir($wind_dir)
    {
        $this->wind_dir = $wind_dir;
    }

    public function setWindGustMph($wind_gust_mph)
    {
        $this->wind_gust_mph = $wind_gust_mph;
    }

    public function setWindMph($wind_mph)
    {
        $this->wind_mph = $wind_mph;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return mixed
     */
    public function getTxOffset()
    {
        return $this->txOffset;
    }

    /**
     * @return mixed
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param mixed $feelslike_c
     */
    public function setFeelslikeC($feelslike_c)
    {
        $this->feelslike_c = $feelslike_c;
    }

    /**
     * @return mixed
     */
    public function getFeelslikeC()
    {
        return $this->feelslike_c;
    }

    /**
     * @param mixed $temp_c
     */
    public function setTempC($temp_c)
    {
        $this->temp_c = $temp_c;
    }

    /**
     * @return mixed
     */
    public function getTempC()
    {
        return $this->temp_c;
    }

    /**
     * @return mixed
     */
    public function getForecastUrl()
    {
        return $this->forecast_url;
    }

    /**
     * @return mixed
     */
    public function getIconUrl()
    {
        return $this->icon_url;
    }

}
