<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scmijt
 * Date: 15/09/2013
 * Time: 22:31
 * To change this template use File | Settings | File Templates.
 */

class AddressToGPS {

    static $mykey = "AIzaSyD-eMemmwnrdyvjniOvOwMlbjhq6OJzATo";
    static $urlbase = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    static $endurl = "&sensor=false&key=";


    public static function leJavascriptWay($address) {
        return '
            var geocoder;
            var map;
            function initialize() {
              geocoder = new google.maps.Geocoder();

            alert("'.$address.'");
            geocoder.geocode( { "address": "'.$address.'"}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {
                    var loc = results[0].geometry.location;

                  //  $("#gpslocation").text( toString(loc));

                //    alert($("#gpslocation").html());

                    //gmap3


                  } else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
              });
            }

            google.maps.event.addDomListener(window, "load", initialize);

        ';
    }

    /**
     * @param $building
     * @return GPS
     */
    public static function buildingAddressToGPS($building) {
        /** @var  $building Building */
        $lang = 'en';

        $googleApiKey= '';

        $google = new GoogleGeoLocator(null,$googleApiKey);

        $countryName ="USA";
        $country = Country::model()->getByCountryCode($building->country);
        if (!empty($country))
            $countryName = $country->country_name;


        // Google API does not like apartment numbers, so let's use the first part of the address and
        // cut out any obvious # numbers...

        $addressFirstLine = $building->address1;

        if (strpos($addressFirstLine, "#"))
            $addressFirstLine = StringUtilities::substringBefore($addressFirstLine, "#");

        $results = $google->searchByAddress($addressFirstLine, $building->city, $countryName);

        if (isset($results['results']))
            return new GPS($results['results'][0]['lat'], $results['results'][0]['long']);
        else
            return null;
    }
}