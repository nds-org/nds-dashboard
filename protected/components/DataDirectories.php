<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scmijt
 * Date: 05/09/2013
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */

class DataDirectories {

    function __construct()
    {
    }

    public static function getBaseUrl() {
        return Yii::app()->baseUrl;
    }

    public static function getThemeUrl() {
        return Yii::app()->theme->baseUrl;
    }

    public static function getBaseDir() {
        return Yii::app()->basePath;
    }

    public static function getInternalImagesDirectory() {
        return Yii::app()->basePath . "/data/images/";
    }



    public static function getDataDirectory() {
        $fileCache = Yii::app()->basePath . "/data/filecache/";

        if (!file_exists($fileCache)) {
            mkdir($fileCache, 0777, true);
        }

        return $fileCache;
    }


    public static function getSlushDirectory() {
        $slushDir = DataDirectories::getDataDirectory()."Albums/";

        if (!file_exists($slushDir)) {
            mkdir($slushDir, 0777, true);
        }

        return $slushDir;
    }

    public static function getMembersDirectory() {
        $members = DataDirectories::getDataDirectory()."Members/";

        if (!file_exists($members)) {
            mkdir($members, 0777, true);
        }

        return $members;
    }

    public static function getUserDirectoryForMemberID($member_id) {

             $userDir = DataDirectories::getMembersDirectory().$member_id."/";

             if (!file_exists($userDir)) {
                 mkdir($userDir, 0777, true);
             }

             return $userDir;
         }

    public static function getCurrentUserDirectory() {
        $me = Member::model()->getByMemberID(Yii::app()->user->id);

        $userDir = DataDirectories::getMembersDirectory().$me->member_id."/";

        if (!file_exists($userDir)) {
            mkdir($userDir, 0777, true);
        }

        return $userDir;
    }

    public static function getArticleDir($album_id) {

        $memberDir = DataDirectories::getSlushDirectory().$album_id."/";

        if (!file_exists($memberDir)) {
            mkdir($memberDir, 0777, true);
        }

        return $memberDir;
    }

    public static function getUserArticleDir($album_id) {

           $memberDir = DataDirectories::getCurrentUserDirectory().$album_id."/";

           if (!file_exists($memberDir)) {
               mkdir($memberDir, 0777, true);
           }

           return $memberDir;
       }

    public static function getCurrentMemberProfilePictureDirectory() {
          $me = Member::model()->getByMemberID(Yii::app()->user->id);

          $oldPicDir = DataDirectories::getMembersDirectory().$me->getFullName()."-".$me->member_id."-pic/";
          $picDir = DataDirectories::getMembersDirectory().$me->member_id."-pic/";

          if (file_exists($oldPicDir))
              rename($oldPicDir, $picDir);

          if (!file_exists($picDir )) {
              mkdir($picDir , 0777, true);
          }

          return $picDir;
      }

    public static function getUserProfilePictureDirectoryForMemberID($member_id) {
        return DataDirectories::getMembersDirectory().$member_id."-pic/";

    }

}