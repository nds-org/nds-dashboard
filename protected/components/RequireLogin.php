<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scmijt
 * Date: 09/02/2012
 * Time: 14:25
 * To change this template use File | Settings | File Templates.
 */
class RequireLogin extends CBehavior
{

    public function attach($owner)
    {
        $owner->attachEventHandler('onBeginRequest', array($this, 'handleBeginRequest'));
    }


    public function handleBeginRequest($event) {

        Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');

        $app = Yii::app();
        $user = $app->user;

        $request = trim($app->urlManager->parseUrl($app->request), '/');
        $login = trim($user->loginUrl[0], '/');

        $baseurls = array(
            'site/',
            'article/search',
            'article/view',
            'member',
            'revive',
            'media',
            'gii',
            );

        $urlStartpasses=false;

        foreach ($baseurls as $url) {
            if (StringUtilities::startsWith($request, $url)) {
                $urlStartpasses=true;
            }
       }

    //    echo "Request...: ".$request;


        if (!$urlStartpasses) {
      //     echo "Failing ...: ".$request;

        } else {
        //    echo "Passed ".$urlStartpasses;
        }


        // Restrict guests to public pages.
        $allowed = array($login, '');

        // if the user is not logged in and s/he is not accessing the login page and
        // one of the site pages, then they must log in.
        if ($user->isGuest && (!in_array($request, $allowed)) && (!$urlStartpasses)) {
            if ($user->isGuest) {
       //         echo "Need to log in !";
                $user->loginRequired();
            }
        }

  //     echo "heading to normal ...";

//        exit(0);
    }


}
?>