<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
    private $_id;
    private $_username;

    public function authenticate()
    {
        $app = Yii::app();
        $this->init();

        if (($this->username=="gii") && ($this->password=="gii")) {
            $this->setState("isproxy", "false");
            $this->_id=111;
            $this->_username=$this->username;

            $this->errorCode=self::ERROR_NONE;
            return !$this->errorCode;
        }

        $bcrypt = new Bcrypt();

        $member=Member::model()->getByUsername($this->username);

 //       echo "Member = ".$member->username;
   //     echo "Member = ".$member->first_name;
     //   echo "Pass:". $bcrypt->verify($this->password, $member->password);
//        exit;
        // For encrypted:
        //

       if (($member!=null && ($bcrypt->verify($this->password, $member->password)))) {
            $this->_id=$member->member_id;
            $this->_username=$member->username;
           $role_obj= Role::model()->getByRoleID($member->role_id);
           $role = $role_obj->name;

           $auth=Yii::app()->authManager;

           if(!$auth->isAssigned($role,$this->_id))
           {
               if($auth->assign($role,$this->_id))
               {
                   Yii::app()->authManager->save();
               }
           }

            $this->setState("userrole", $role);

            $this->errorCode=self::ERROR_NONE;
        } else
            $this->errorCode=self::ERROR_PASSWORD_INVALID;

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }

    private function init() {
         /** @var  $auth IAuthManager */
         $auth=Yii::app()->authManager;

         $roles = Role::model()->findAll();

         foreach ($roles as $role) {
             /** @var $role Role */

             if (!$auth->getAuthItem($role->name))
                  $auth->createRole($role->name);
         }

      }

}