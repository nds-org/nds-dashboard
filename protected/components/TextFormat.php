<?php

class TextFormat {

    public static function getNiceAddressAndContact($name, $firstName, $lastName, $title, $address, $city, $state, $postalcode, $country, $tel, $fax, $email, $left_width=80, $name_label="Company") {
        $deadcontact=false;
        if (empty($firstName) && empty($title) && empty($tel) && empty($fax) && empty($email))
            $deadcontact=true;

        $toreturn = '
               '.(empty($name)?'':TextFormat::getBackToBackTwoColumn($name_label,
                $name, $left_width) ).'
               '.(empty($address)?'':TextFormat::getBackToBackTwoColumn('Address',
                $address, $left_width)).'
               '.(empty($city)?'':TextFormat::getBackToBackTwoColumn('City',
                $city, $left_width)).'
               '.(empty($state)?'':TextFormat::getBackToBackTwoColumn('State',
                $state, $left_width)).'
               '.(empty($postalcode)?'':TextFormat::getBackToBackTwoColumn('Zip',
                $postalcode, $left_width)).'
               '.(empty($country)?'':TextFormat::getBackToBackTwoColumn('Country',
                $country, $left_width));

        if ($deadcontact) return $toreturn;

        $toreturn .=
            (empty($firstName)?'':TextFormat::getBackToBackTwoColumn('Name',
                $firstName.' '.$lastName, $left_width)).
            (empty($title)?'':TextFormat::getBackToBackTwoColumn('Title',
                $title, $left_width)).
            (empty($tel)?'':TextFormat::getBackToBackTwoColumn('Tel',
                $tel, $left_width)).
            (empty($fax)?'':TextFormat::getBackToBackTwoColumn('Fax',
                $fax, $left_width)).
            (empty($email)?'':TextFormat::getBackToBackTwoColumn('Email',
                $email, $left_width)).'
        ';

        return $toreturn;

    }

    public static function getNiceAddress($name, $address, $city, $state, $postalcode, $country, $leftwidth=110, $name_label="Dealer") {

        return '
               '.(empty($name)?'':TextFormat::getBackToBackTwoColumn($name_label,
            $name, $leftwidth)).'
               '.(empty($address)?'':TextFormat::getBackToBackTwoColumn('Address',
            $address, $leftwidth)).'
               '.(empty($city)?'':TextFormat::getBackToBackTwoColumn('City',
            $city, $leftwidth)).'
               '.(empty($state)?'':TextFormat::getBackToBackTwoColumn('State',
            $state, $leftwidth)).'
               '.(empty($postalcode)?'':TextFormat::getBackToBackTwoColumn('Zip',
            $postalcode, $leftwidth)).'
               '.(empty($country)?'':TextFormat::getBackToBackTwoColumn('Country',
            $country, $leftwidth));
    }


    public static function getNiceContact($firstName, $lastName, $title, $tel, $fax, $email, $leftwidth=110) {
        if (empty($firstName) && empty($title) && empty($tel) && empty($fax) && empty($email))
            return ""; // safe ...
        return
            (empty($firstName)?'':TextFormat::getBackToBackTwoColumn('Name',
                $firstName.' '.$lastName, $leftwidth)).
            (empty($title)?'':TextFormat::getBackToBackTwoColumn('Title',
                $title, $leftwidth)).
            (empty($tel)?'':TextFormat::getBackToBackTwoColumn('Tel',
                $tel, $leftwidth)).
            (empty($fax)?'':TextFormat::getBackToBackTwoColumn('Fax',
                $fax, $leftwidth)).
            (empty($email)?'':TextFormat::getBackToBackTwoColumn('Email',
                $email, $leftwidth)).'
        ';

    }

    public static function getNiceFullContact($firstName, $lastName, $title, $tel, $extension, $mobile_phone, $fax, $email, $leftwidth=100) {
        if (empty($firstName) && empty($title) && empty($tel) && empty($fax) && empty($email) && empty($mobile_phone))
            return ""; // safe ...

        if (!empty($extension) && !empty($tel))
            $tel.= " Ext:".$extension;
        return
            (empty($firstName)?'':TextFormat::getBackToBackTwoColumn('Name',
                $firstName.' '.$lastName, $leftwidth)).
            (empty($title)?'':TextFormat::getBackToBackTwoColumn('Title',
                $title, $leftwidth)).
            (empty($tel)?'':TextFormat::getBackToBackTwoColumn('Tel',
                $tel, $leftwidth)).
            (empty($mobile_phone)?'':TextFormat::getBackToBackTwoColumn('Mobile',
                $mobile_phone, $leftwidth)).
            (empty($fax)?'':TextFormat::getBackToBackTwoColumn('Fax',
                $fax, $leftwidth)).
            (empty($email)?'':TextFormat::getBackToBackTwoColumn('Email',
                $email, $leftwidth)).'
        ';

    }


    public static function getContactInfo(Member $member, $leftwidth=110) {
        if (empty($member->pager) && empty($member->emergency_phone) && empty($member->tel) && empty($member->fax)
                        && empty($member->email) && empty($member->mobile_phone))
            return ""; // safe ...

        if (!empty($member->extension) && !empty($member->tel))
            $member->tel.= " Ext:".$member->extension;
        return
            (empty($member->tel)?'':TextFormat::getBackToBackTwoColumn('Tel',
                $member->tel, $leftwidth)).
            (empty($member->mobile_phone)?'':TextFormat::getBackToBackTwoColumn('Mobile',
                $member->mobile_phone, $leftwidth)).
            (empty($member->emergency_phone)?'':TextFormat::getBackToBackTwoColumn('Emergency',
                $member->emergency_phone, $leftwidth)).
            (empty($member->pager)?'':TextFormat::getBackToBackTwoColumn('Pager',
                $member->pager, $leftwidth)).
            (empty($member->fax)?'':TextFormat::getBackToBackTwoColumn('Fax',
                $member->fax, $leftwidth)).
            (empty($member->email)?'':TextFormat::getBackToBackTwoColumn('Email',
                $member->email, $leftwidth)).'
        ';

    }

    public static function getSignatureFor(Member $member) {

          if (!empty($member->extension) && !empty($member->tel))
              $member->tel.= " Ext:".$member->extension;
          return
              (empty($member->first_name)?'':$member->first_name." ".$member->last_name).
              (empty($member->title)?'': ", ".$member->title)."<br>".
              (empty($member->tel)?'':'Phone: '.$member->tel."<br>").
              (empty($member->mobile_phone)?'':'Mobile: '.$member->mobile_phone."<br>").
              (empty($member->email)?'':'Email: '.$member->email);

      }

    /**
     * The passing object could be a building, a vendor, etc that uses the common address format
     *
     * @param $addressObject
     * @return string
     */
    public static function getBasicHTMLAddressFor($addressObject) {
          return
              (empty($addressObject->address1)?'':$addressObject->address1."<br>").
              (empty($addressObject->address2)?'':$addressObject->address2."<br>").
              (empty($addressObject->city)?'':$addressObject->city.",").
              (empty($addressObject->state)?'':$addressObject->state." ").
              (empty($addressObject->postal_code)?'':$addressObject->postal_code."<br>").
              (empty($addressObject->country)?'':$addressObject->country."<br>");

      }

    public static function getBackToBackTwoColumn($left, $right, $leftwidth=110) {
        $lf=(string)$leftwidth;

        return '
             <div>
                 <span style="display: inline-block; min-width: '.$lf.'px; width: '.$lf.'px; text-align: right; padding-right: 10px;">
                     <strong>'.
                    $left.
                    '</strong>
                 </span>'.
                    $right. '
             </div>
             ';
    }

    public static function getBackToBackTwoColumnNoStyle($left, $right, $leftwidth=110) {
        $lf=(string)$leftwidth;

        return '
             <div>
                 <span style="display: inline-block; min-width: '.$lf.'px; width: '.$lf.'px; text-align: right; padding-right: 10px;">
                     '.
                    $left.
                    '
                 </span>'.
                    $right. '
             </div>
             ';
    }


    public static function getStreetFromAddress($model) {
        /** @var $model Building */
        if (empty($model->address2))
            return $model->address1;
        else
            return $model->address1.", ".$model->address2;
    }

    public static function trimText($description, $length=80) {

        $description = trim($description);

        if (StringUtilities::contains($description, "img src")) {
            $description = StringUtilities::substringBefore($description, "img src");
        }

        if (StringUtilities::endsWith($description, "<div>")) {
            $description = substr($description, 0, strlen($description) - strlen("<div>"));
        }

        if (strlen($description) > $length) {
            return substr($description,0,$length).'...';
        } else {
            return $description;
        }
    }

}