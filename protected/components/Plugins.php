<?php
/**
 * Created by PhpStorm.
 * User: scmijt
 * Date: 30/12/2014
 * Time: 15:03
 */

class Plugins {


    public static function isGuest() {

        $isGuest = StateVariables::isGuest();

        if ($isGuest) return false;

        return true;
    }

    public static function shouldShowAlerts() {
        return Plugins::isGuest();
    }

} 