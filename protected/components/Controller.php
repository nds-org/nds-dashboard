<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';

	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();

    /**
     * @return array
     */
    public function accessRules()
       {
           return array(
               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions'=>array('view', 'index','admin'),
                   'roles'=>array('admin'),
               ),
               array('allow', // allow admin user to perform 'admin' and 'delete' actions
                   'actions'=>array('view', 'index', 'admin', 'create', 'update', 'delete'),
                   'roles'=>array('siteadmin', 'admin'),
               ),
               array('deny',  // deny all users
                   'users'=>array('*'),
               ),
           );
       }

      public function filters()
      {
          return array(
              'accessControl', // perform access control for CRUD operations
              array('application.components.HttpsFilter', 'https'=>true),
          );
      }

      /**
       * Returns the Yii application
       *
       * @return mixed
       */
      protected function getYiiApp() {
          return Yii::app();
      }

      /**
       * Returns the Yii user object
       *
       * @return mixed
       */
      protected function getYiiUserObj() {
          return Yii::app()->user;
      }


      /**
       * Returns the ID of the current logged in user
       *
       * @return Integer
       */
      protected function getMemberID() {
          return Yii::app()->user->id;
      }

      /**
       * Gets the current member
       *
       * @return Member
       */
      protected function getMember() {
          return Member::model()->getByMemberID($this->getMemberID());
      }



}