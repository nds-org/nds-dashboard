<?php
/**
 * Created by JetBrains PhpStorm.
 * User: scmijt
 * Date: 05/09/2013
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */

class StateVariables {

    public static function getCurrentControllerName() {
        return Yii::app()->controller->id;
    }

    public static function getCurrentActionName() {
        return Yii::app()->controller->action->id;
    }

    public static function getCurrentControllerActionName() {
        return StateVariables::getCurrentControllerName()."/".StateVariables::getCurrentActionName();
    }

    public static function getCurrentURL() {
        return Yii::app()->request->requestUri;
    }

    public static function isSuperUser(Member $member) {
        return (($member->role === "admin") || ($member->role=="siteadmin") );
    }

    public static function canManageAccounts(Member $member) {
        return (($member->role === "admin") || ($member->role=="siteadmin") || ($member->role=="office") );
    }

    public static function isGuest() {
        $isGuest = Yii::app()->user->isGuest;

        return $isGuest;
    }

    public static function getSiteURL() {
        return Pages::baseURL();
    }


    public static function getBaseUrl() {
        return Yii::app()->request->baseUrl;
    }

    public static function advance() {
        $current_action = StateVariables::getCurrentActionName();
        $current_controller = StateVariables::getCurrentControllerName();
        $currentURL = StateVariables::getCurrentURL();

        $lastWorkOrderList = StateVariables::getCurrentControllerActionName();

        if ($lastWorkOrderList)

            if ($current_action !== Yii::app()->user->getState('previous-action')) {
                Yii::app()->user->setState('previous-action', $current_action);
                Yii::app()->user->setState('current-action', $current_action);
            }
        if ($current_controller !== Yii::app()->user->getState('previous-controller')) {
            Yii::app()->user->setState('previous-controller', $current_controller);
            Yii::app()->user->setState('current-controller', $current_controller);
        }

        if ($currentURL!== Yii::app()->user->getState('previous-url')) {
            Yii::app()->user->setState('previous-url', $currentURL);
            Yii::app()->user->setState('current-url', $currentURL);
        }

    }

    public static function getPreviousControllerName() {
        return Yii::app()->user->getState('previous-controller');
    }

    public static function getPreviousUrl() {
        return Yii::app()->user->getState('previous-url');
    }

    public static function getPreviousActionName() {
        return Yii::app()->user->getState('previous-action');
    }

    public static function getPreviousControllerActionName() {
        return Yii::app()->user->getState('previous-controller')."/".Yii::app()->user->getState('previous-action');
    }

    public static function getPreviousControllerActionAsDisplayableName() {
        return ucfirst(Yii::app()->user->getState('previous-controller'))." ".ucfirst(Yii::app()->user->getState('previous-action'));
    }

    public static function listDebug() {
        return "Previous Controller: ".StateVariables::getPreviousControllerName()."<br>".
        "Previous Action: ".StateVariables::getPreviousActionName()."<br>".
        "Current Controller: ".StateVariables::getCurrentControllerName()."<br>".
        "Current Action: ".StateVariables::getCurrentActionName()."<br>";
    }


    public static function setSecurity($key) {
        Yii::app()->user->setState('security-key', $key);
    }

    public static function getSecurityKey() {
        $securityKey = trim(Yii::app()->user->getState('security-key'));

        if (empty($securityKey))
            return NULL;
        else
            return $securityKey;
    }

    public static function clearState($variableName) {
        Yii::app()->user->setState($variableName, "");
    }

    public static function setState($variableName, $val) {
        Yii::app()->user->setState($variableName, $val);
    }

    public static function getState($variableName) {
        $thingy = trim(Yii::app()->user->getState($variableName));

        if (empty($thingy))
            return NULL;
        else
            return $thingy;
    }



    public static function getCompanyIDTempVariable() {
        $tempCompanyID = trim(Yii::app()->user->getState('temp_company_id'));

        if (empty($tempCompanyID))
            return NULL;
        else
            return $tempCompanyID;
    }


    public static function setCompanyIDTempVariable($company_id) {
        Yii::app()->user->setState('temp_company_id', $company_id);
    }

    public static function clearCompanyIDTempVariable() {
        Yii::app()->user->setState('temp_company_id', "");
    }


    public static function hasAdminAccess() {
        $role = Yii::app()->user->getState('userrole');

        return ($role=="admin") || ($role=="siteadmin");
    }

    public static function isWhiteLabel() {
        if (isset($_GET['CompanyURL']) && $_GET['CompanyURL'] === "http://nearon.com")
            return true;
        else
            return false;
    }

    public static function getSearchString() {
             if (isset($_REQUEST['search']))
                 return $_REQUEST['search'];
             else
                 return NULL;
         }

}