<?php
/**
 * Simple mail sending message using a template.
 *
 * User: Ian
 * Date: 14/05/2014
 * Time: 16:57
 */

class SendMailMessage {

    public static function sendEmail($recipient_id, $content, $subject) {
         $message = new YiiMailMessage();

         $errors = array();
         $errorCount=0;

         /** @var $contact Member */

         $message->view = 'message';
         $recipient = Member::model()->getByMemberID($recipient_id);

         $sender = Member::model()->getByMemberID(Yii::app()->user->id);

         $message->setBody(array(
             'recipient'=>$recipient,
             'sender'=>$sender,
             'message'=>$content,
         ), 'text/html');

         if (!empty($sender))
             $message->setFrom($sender->email);
         else
             $message->setFrom(Yii::app()->params['adminEmail']);

         $message->setSubject($subject);

         $message->addTo($recipient->email);
         //      $message->addTo("ian.j.taylor@gmail.com");

         try {
             return Yii::app()->mail->send($message);
         } catch (Exception $e) {
             Yii::log('Caught Exception: '.$e->getTraceAsString(),
                 "error", "components.ewo.mailhandler.sendVendorRequest");
             $errors[$errorCount] = $recipient->getFullName()." using their Email address: ".$recipient->email;
             ++$errorCount;
         }

         return $errors;
     }

} 