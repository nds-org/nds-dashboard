<?php

/**
 * SignupForm class.
 * SignupForm is the data structure for keeping
 * user signup form data. It is used by the 'signup' action of 'MemberController'.
 */
class SignupForm extends CFormModel
{
	public $first_name;
	public $last_name;
	public $email;

	public $username;
	public $password;
	public $password2;

	public $institute;

	private $_identity;

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array
		(
			
			// Required fields.
			array( 'username, password, password2, email', 'required' ),
			
			// Optional fields.
			array( 'first_name, last_name, institute', 'safe' ),
			
			// password needs to match.
			array( 'password2', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ),

			array( 'email', 'email' ),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
		);
	}

    public function getAttribute($attribute) {
        if ($attribute == "institute") {
            return $this->institute;
        }
        return NULL;

    }

	/**
	 * Creates the user using the given user information in the model.
	 * @return boolean whether signup is successful
	 */
	public function signup()
	{
		// Check for an existing account with the same email address.
		$member = Member::model()->getByEmail( $this->email );
		if( empty( $member ) )
		{
			// Email is unused, create a new account.
			$member = new Member( 'registration' );
		}
		elseif( $member->role === 'Guest' )
		{
			// This email is being used in a guest account.
			// Adopt this guest account.
		}
		else
		{
			// This email is already being used by a non-guest account.
			$this->addError( 'member', 'This email address is already in use by another account.' );
			return false;
		}
		
		// Populate the account detail.
		$member->first_name = $this->first_name;
		$member->last_name = $this->last_name;
		$member->email = $this->email;
		$member->username = $this->username;
		$member->password = $this->password;
		$member->institute = $this->institute;
        $member->role = "researcher";

		// Save the new member.
		if( !$member->save() )
		{
			// Bubble the validation errors up to be displayed on the web page.
			foreach( $member->errors as $facility => $error_list )
			{
				foreach( $error_list as $error_line )
				{
					$this->addError( $facility, $error_line );
				}
			}
			return false;
		}

		// Login with the new member information.
		$this->_identity = new UserIdentity( $this->username, $this->password );
		$this->_identity->authenticate();

		if( $this->_identity->errorCode === UserIdentity::ERROR_NONE )
		{
			$duration = (3600 * 24 * 30); // 30 days
			Yii::app()->user->login( $this->_identity, $duration );
			return true;
		}
		else
		{
			return false;
		}
	}
}
