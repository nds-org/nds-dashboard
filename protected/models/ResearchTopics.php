<?php

/**
 * This is the model class for table "research_topics".
 *
 * The followings are the available columns in table 'research_topics':
 * @property integer $id
 * @property string $key
 * @property string $name
 */
class ResearchTopics extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'research_topics';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('key', 'length', 'max'=>50),
            array('name', 'length', 'max'=>200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, key, name', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array
     */
    public function getAsArray() {
        $records = ResearchTopics::model()->findAll();

        $recordArray=array();

        foreach ($records as $record) {
            /** @var $record ResearchTopics */

            $recordArray[$record->id] = $record->name;
        }

        return $recordArray;
    }

    public function getResearchTopicFor($index) {
        $researchTopic = $this->getByResearchTopicID($index);

        if (empty($researchTopic))
            return "";
        else
            return $researchTopic->name;

    }

    /**
     * @param $research_topic_id
     * @return ResearchTopics
     */
    public function getByResearchTopicID($research_topic_id) {
         return ResearchTopics::model()->find('id=:id', array(':id'=>$research_topic_id));
     }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'key' => 'Key',
            'name' => 'Name',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('key',$this->key,true);
        $criteria->compare('name',$this->name,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ResearchTopics the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
