<?php

/**
 * This is the model class for table "currencycodes".
 *
 * The followings are the available columns in table 'currencycodes':
 * @property integer $id
 * @property string $code
 * @property string $country
 */
class Currencycode extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Currencycode the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function defaultScope()
     {
         return array(
             "order" => "country",
         );
     }
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'currency_codes';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('code, country', 'required'),
            array('code', 'length', 'max'=>4),
            array('country', 'length', 'max'=>100),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, code, country', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'code' => 'Code',
            'country' => 'Country',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('code',$this->code,true);
        $criteria->compare('country',$this->country,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>100,
            ),
        ));
    }


    /**
     * @return array
     */
    public function getCurrencyCodes() {
        $countryCodes = array();

        $countries =  Currencycode::model()->findAll();

        //$countryCodes['USD'] = "United States Dollar";
        //$countryCodes['USD'] = "USD";

        foreach ($countries as $country) {
            /** @var $country Currencycode */

          //  if ($country->code != "USD")
                $countryCodes[$country->code] = $country->code;
        }

        return $countryCodes;
    }

    /**
      * @return array
      */
     public function getCurrencyCodesDetail() {
         $countryCodes = array();

         $countries =  Currencycode::model()->findAll();

         foreach ($countries as $country) {
             /** @var $country Currencycode */
             $countryCodes[$country->code] = $country->country. "(".$country->code.")";
         }

         return $countryCodes;
     }
}