<?php

/**
 * This is the model class for table "resource".
 *
 * The followings are the available columns in table 'resource':
 * @property integer $id
 * @property integer $article_id
 * @property integer $content_type_id
 * @property string $title
 * @property string $description
 * @property string $location
 * @property string $date_added
 */
class Resource extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'resource';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('article_id, content_type_id', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>500),
			array('description', 'length', 'max'=>5000),
			array('location', 'length', 'max'=>200),
			array('date_added, description, title', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, article_id, content_type_id, title, description, location, date_added', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    /**
      * @param $location
      * @return Resource
      */
     public function getByResourceLocation($location) {
         return Resource::model()->find('location=:location', array(':location'=>$location));
     }

     /**
      * @param $article_id
      * @return Resource
      */
     public function getAllByArticleID($article_id) {
         return Resource::model()->findAll('article_id=:article_id', array(':article_id'=>$article_id));
     }

    /**
     * @param $article_id
     * @return Resource
     */
    public function getAllDataResourcesFor($article_id) {

          $criteria = new CDbCriteria();
          $criteria->addCondition('article_id='.$article_id);
          $criteria->addInCondition('content_type_id', array(ContentType::$URL, ContentType::$DATA_SET));

          return Resource::model()->findAll($criteria);

      }


     /**
      * @param $id
      * @return Resource
      */
     public function getByResourceID($id) {
         return Resource::model()->find('id=:id', array(':id'=>$id));
     }

    /**
       * @param $id
       * @return Resource
       */
      public function getArticleForResourceID($id) {
          $resource = $this->getByResourceID($id);

          if (!isset($resource)) return NULL;

          return Article::model()->getByArticleID($resource->article_id);
      }

     /**
      * @return array
      */
     public function getAsArray() {
         $records = Resource::model()->findAll();

         $recordArray=array();

         foreach ($records as $record) {
             /** @var $record Resource */

             $recordArray[$record->id] = $record->title;
         }

         return $recordArray;
     }

    public function getTitle() {
        if (empty($this->title))
            return "No Title";
        return $this->title;
      }

    public function getContentType() {
        $contentType = ContentType::model()->getByContentTypeID($this->content_type_id);

        if (empty($contentType))
            return "NA";
        else
            return $contentType->name;
      }


     public function getDescriptionLeft() {

         if ($this->content_type_id == ContentType::$URL)
             return $this->getThumbnail(Yii::app()->request->baseUrl."/images/icons/http.png");
         else
             return $this->getThumbnail($this->getResourceContentURL());
     }


    private function getItem($title, $content) {
        if (empty($content))
            return "";

        return '
        <div class="item">
            <i class="map marker icon"></i>
            <div class="content">
                <b class="header">'.$title.'</b>
                <div class="description">'.$content.'</div>
            </div>
        </div>
        ';
    }
     public function getDescriptionRight() {
         return '
                <div class="ui divided list">
                  '.$this->getItem("Resource Name", $this->getFileName()).'
                  '.$this->getItem("Title", $this->title).'
                  '.$this->getItem("Content Type", $this->getContentType()).'
                  '.$this->getItem("Description", $this->description).'
                </div>';
     }

    public function isALocalFile() {
        return ($this->content_type_id != ContentType::$URL);
    }

     public function getResourceContentURL() {
         $directory = Pages::getBaseDir().DIRECTORY_SEPARATOR.$this->getPath();
         $filename = $this->getFileName();

         return Pages::baseURL().'media/thumbnail?directory='.$directory.'&filename='.$filename;
     }

    public function getResourceLocationFile() {
        $directory = Pages::getBaseDir().DIRECTORY_SEPARATOR.$this->getPath();
        $filename = $this->getFileName();

        return $directory.$filename;
    }


    public function getPath() {
        return StringUtilities::substringBeforeLast($this->location, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
    }

    public function getFileName() {
        if ($this->isALocalFile())
            return StringUtilities::substringAfterLast($this->location, DIRECTORY_SEPARATOR);
        else
            return $this->location;
    }

    public function getDownloadURL() {
        return Pages::getAbsoluteBaseURL()."resource/url/resource_id/".$this->id;
    }

    private function getThumbnail($image) {
              return '<div class="ui medium images">
              <img src="'.$image.'"></div>';
        }


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'article_id' => 'Article',
			'content_type_id' => 'Content Type',
			'title' => 'Title',
			'description' => 'Description',
			'location' => 'Location',
			'date_added' => 'Date Added',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('article_id',$this->article_id);
		$criteria->compare('content_type_id',$this->content_type_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('date_added',$this->date_added,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Resource the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
