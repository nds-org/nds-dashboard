<?php

/**
 * This is the model class for table "date_types".
 *
 * The followings are the available columns in table 'date_types':
 * @property integer $id
 * @property string $style
 * @property string $date_format
 */
class DateTypes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DateTypes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'date_types';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('style, date_format', 'required'),
			array('style', 'length', 'max'=>100),
			array('date_format', 'length', 'max'=>25),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, style, date_format', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'style' => 'Date Style',
			'date_format' => 'Date Format',
		);
	}

    /**
         * @return array
         */
        public function getDateTypes() {
            $dateStyles = array();

            $styles =  DateTypes::model()->findAll();

            foreach ($styles as $style) {
                /** @var $style DateTypes */
                $dateStyles[$style->date_format] = $style->style;
            }

            return $dateStyles;
        }


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('style',$this->style,true);
		$criteria->compare('date_format',$this->date_format,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}