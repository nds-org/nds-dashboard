<?php

/**
 * This is the model class for table "content_type".
 *
 * The followings are the available columns in table 'content_type':
 * @property integer $id
 * @property string $type
 * @property string $name
 * @property string $description
 */
class ContentType extends CActiveRecord
{

    // CONTENT TYPES
    public static $ARTICLE = 1;
    public static $MEDIA = 2;
    public static $SOURCE_CODE = 3;
    public static $DATA_SET = 4;
    public static $PRESENTATION = 5;
    public static $BOOK_COVER = 6;
    public static $URL = 7;
    public static $TEXT_DESCRIPTION = 8;
    public static $HTML_FILE = 9;


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'content_type';
    }


    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('type', 'length', 'max'=>25),
            array('name', 'length', 'max'=>50),
            array('description', 'length', 'max'=>200),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, type, name, description', 'safe', 'on'=>'search'),
        );
    }


    /**
     * @return array
     */
    public function getAsArray() {
        $records = ContentType::model()->findAll();

        $recordArray=array();

        foreach ($records as $record) {
            /** @var $record ContentType */

            $recordArray[$record->id] = $record->name;
        }

        return $recordArray;
    }

    /**
     * @param $id
     * @return ContentType
     */
    public function getByContentTypeID($id) {
        return ContentType::model()->find('id=:id', array(':id'=>$id));
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'description' => 'Description',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('type',$this->type,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('description',$this->description,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ContentType the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
