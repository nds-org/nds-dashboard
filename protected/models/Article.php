<?php

/**
 * This is the model class for table "article".
 *
 * The followings are the available columns in table 'article':
 * @property integer $id
 * @property string $status
 * @property integer $article_type
 * @property string $date_added
 * @property string $short_name
 * @property string $title
 * @property string $abstract
 * @property string $authors
 * @property string $url
 * @property string $doi
 * @property string $journal_name
 * @property integer $month
 * @property integer $year
 * @property string $pages
 * @property string $volume
 * @property integer $number
 * @property integer $primary_research_field
 * @property integer $secondary_research_field
 * @property string $notes
 * @property string $tags
 */
class Article extends CActiveRecord
{
    public $article_type_name;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'article';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('article_type, abstract', 'required'),
            array('article_type, month, year, number, primary_research_field, secondary_research_field', 'numerical', 'integerOnly'=>true),
            array('status, pages, volume', 'length', 'max'=>10),
            array('short_name, url, journal_name', 'length', 'max'=>200),
            array('title', 'length', 'max'=>1000),
            array('abstract, notes', 'length', 'max'=>5000),
            array('authors', 'length', 'max'=>2000),
            array('doi', 'length', 'max'=>50),
            array('tags', 'length', 'max'=>500),
            array('date_added', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, status, article_type, date_added, short_name, title, abstract, authors, url, doi, journal_name, month, year, pages, volume, number, primary_research_field, secondary_research_field, notes, tags', 'safe', 'on'=>'search'),
        );
    }

    public function beforeSave() {

        if ($this->isNewRecord) {
            //  $this->date_added = DateFormat::anyDateToMySQLDateTime(time());
        }

        return parent::beforeSave();
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'status' => 'Status',
            'article_type' => 'Article Type',
            'article_type_name' => 'Article Type',
            'date_added' => 'Date Added',
            'short_name' => 'Short Name',
            'title' => 'Title',
            'abstract' => 'Abstract',
            'authors' => 'Authors',
            'url' => 'Url',
            'doi' => 'Doi',
            'journal_name' => 'Journal',
            'month' => 'Month',
            'year' => 'Year',
            'pages' => 'Pages',
            'volume' => 'Volume',
            'number' => 'Number',
            'primary_research_field' => 'Primary Research Field',
            'secondary_research_field' => 'Secondary Research Field',
            'notes' => 'Notes',
            'tags' => 'Tags',
        );
    }


    public function afterFind() {
        $article_type = ArticleType::model()->getByArticleTypeID($this->article_type);

        if (!empty($article_type))
            $this->article_type_name = $article_type->name;

        return parent::afterFind();
    }


    /**
     * @return array
     */
    public function getAsArray() {
        $records = Article::model()->findAll();

        $recordArray=array();

        foreach ($records as $record) {
            /** @var $record Article */

            $recordArray[$record->id] = $record->title;
        }

        return $recordArray;
    }

    /**
     * @return array
     */
    public function getStatusTypesAsArray() {
        $recordArray=array();

        $recordArray["Draft"] = "Draft";
        $recordArray["Active"] = "Active";

        return $recordArray;
    }

    /**
     * @param $article_id
     * @return Article
     */
    public function getByArticleID($article_id) {
        return Article::model()->find('id=:id', array(':id'=>$article_id));
    }

    public function getTitle() {
        return $this->title. " by ".$this->authors;
    }

    public function getDescriptionLeft() {
        return $this->abstract;
    }

    public function getDescriptionRight() {
        return $this->getCitation();
    }

    public function getStatus() {
        return ($this->status == 0 ? "Draft" : "Active");
    }

    public function getCitation() {
        // Volume 25 Issue 5, Pages 528-540, May, 2009,
        return
            (empty($this->journal_name) ? "" : $this->journal_name)." ".
            (empty($this->volume) ? "" : "Volume ".$this->volume)." ".
            (empty($this->number) ? "" : "No. ".$this->number)." ".
            (empty($this->pages) ? "" : "Pages ".$this->pages)." ".
            (empty($this->month) ? "" : $this->month)." ".
            (empty($this->year) ? "" : $this->year);

    }


    /**
     * @param $member_id
     * @return Article
     */
    public function getArticlesFor($member_id) {
        $maps = ArticleMap::model()->getArticleMapsFor($member_id);

        $articles = array();

        foreach ($maps as $map) {
            $articles[] = Article::model()->getByArticleID($map->article_id);
        }

        return $articles;
    }


    public function getResources() {
        return Resource::model()->getAllByArticleID($this->id);
    }


    public function getResourcesForAllMemberArticles($member_id) {
        $articles = Article::model()->getArticlesFor($member_id);
        $resources = array();

        foreach ($articles as $article) {
            $art_resources = Resource::model()->getAllByArticleID($article->id);

            if (!empty($art_resources))
                $resources = array_merge($resources, $art_resources);

        }
        return $resources;
    }


    public function findAllArticlesContaining($search_string) {

        $criteria = new CDbCriteria();
        $criteria->compare('title', $search_string, TRUE, 'OR');
        $criteria->compare('authors', $search_string, TRUE, 'OR');
        $criteria->compare('abstract', $search_string, TRUE, 'OR');

        return Article::model()->findAll($criteria);

    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('status',$this->status,true);
        $criteria->compare('article_type',$this->article_type);
        $criteria->compare('date_added',$this->date_added,true);
        $criteria->compare('short_name',$this->short_name,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('abstract',$this->abstract,true);
        $criteria->compare('authors',$this->authors,true);
        $criteria->compare('url',$this->url,true);
        $criteria->compare('doi',$this->doi,true);
        $criteria->compare('journal_name',$this->journal_name,true);
        $criteria->compare('month',$this->month);
        $criteria->compare('year',$this->year);
        $criteria->compare('pages',$this->pages,true);
        $criteria->compare('volume',$this->volume,true);
        $criteria->compare('number',$this->number);
        $criteria->compare('primary_research_field',$this->primary_research_field);
        $criteria->compare('secondary_research_field',$this->secondary_research_field);
        $criteria->compare('notes',$this->notes,true);
        $criteria->compare('tags',$this->tags,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Article the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
