<?php

/**
 * This is the model class for table "country".
 *
 * The followings are the available columns in table 'country':
 * @property $country_code string
 * @property $country_name string
 */
class Country extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Country the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'country';
	}

    public function defaultScope()
      {
          return array(
              'order'=>"country_name ASC",
          );
      }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('country_code, country_name', 'required'),
			array('country_code', 'length', 'max'=>2),
			array('country_name', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('country_code, country_name', 'safe', 'on'=>'search'),
		);
	}


    /**
       * @param country_name
       * @return string
       */
      public function getCountryCodeFor($country_name) {
          /** @var  $country Country */
          $country =Country::model()->find('country_name=:country_name', array(':country_name'=>$country_name));

          if (isset($country))
              return $country->country_code;
          else
              return "";
      }


       /**
        * @param $country_code
        * @return Country
        */
       public function getByCountryCode($country_code) {
           return Country::model()->find('country_code=:country_code', array(':country_code'=>$country_code));
       }

    /**
      * @param $country_code
      * @return Country
      */
     public function getCountryNameForCountryCode($country_code) {
         /** @var $country Country */
         $country = Country::model()->find('country_code=:country_code', array(':country_code'=>$country_code));

         return $country->country_name;
     }

      /**
       * @return array
       */
      public function getCountryNames() {
          $countryNames = array();

          $countries =  Country::model()->findAll();

          $countryNames['US'] = "United States";

          foreach ($countries as $country) {
              $countryNames[$country->country_code] = $country->country_name;
          }


          return $countryNames;
          }

      /**
       * @param $default default country selected
       * @return string
       */
      public function getCountryOptionList($default="US") {
          $countryNames = array();

          $countries =  Country::model()->findAll();

          $countryNames['US'] = "United States";

          $html="";

          foreach ($countries as $country) {
              if ($country->country_code === $default)
                $deftext = 'selected="selected"';
              else
                  $deftext ="";
              $html.='  <option '.$deftext.' value="'.$country->country_code.'">'.$country->country_name.'</option>
              ';
          }

          return $html;
          }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'country_code' => 'country_code',
			'country_name' => 'Country Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('country_code',$this->country_code,true);
		$criteria->compare('country_name',$this->country_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}