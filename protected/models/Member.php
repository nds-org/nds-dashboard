<?php

/**
 * This is the model class for table "members".
 *
 * The followings are the available columns in table 'members':
 * @property integer $member_id
 * @property string $username
 * @property string $password
 * @property integer $institute
 * @property string $role_id
 * @property string $first_name
 * @property string $last_name
 * @property string $title
 * @property string $profile_picture_loc
 * @property string $date_registered
 * @property string $email
 * @property string $active
 * @property string $notes
 * @property string email_hash
 */
class Member extends CActiveRecord
{
    public $is_existing_member=false;
    public $existing_member;
    static public $key;
    public $role;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Member the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'member';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, password, email', 'required', 'on' => 'registration'),

            array('username', 'unique', 'message' => "This user's name already exists.", 'on' => 'registration'),
            array('username', 'length', 'max'=>50, 'min' => 3,'message' => "Incorrect username (length between 6 and 50 characters)."),
            array('username', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u','message' =>"Incorrect symbols (A-z0-9)."),

            array('institute', 'length', 'max'=>250),
            array('title, profile_picture_loc', 'length', 'max'=>50),

            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('institute, is_existing_member, existing_member, username, institute, first_name, last_name, title, date_registered, email, active, notes, role_id', 'safe'),
            array('is_existing_member, existing_member, member_id, username, password, role_id, profile_picture_loc, institute, first_name, last_name, title, date_registered, email, active, notes', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'member_id' => 'Member',
            'username' => 'Username',
            'password' => 'Password',
            'institute' => 'Your Institute',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'title' => 'Title',
            'profile_picture_loc' => 'Profile Picture Location',
            'date_registered' => 'Date registered',
            'active' => 'Active',
            'notes' => 'Notes',
            'role_id' => 'Role',
            'is_existing_member' => 'Is an Existing Member',
            'existing_member' => 'Existing Member'
        );
    }

    /**
     * @param $username
     * @return Member
     */
    public function getByUsername($username) {
        return Member::model()->find('username=:username', array(':username'=>$username));
    }

    /**
     * @param $member_id
     * @return Member
     */
    public function getByMemberID($member_id) {
        return Member::model()->find('member_id=:member_id', array(':member_id'=>$member_id));
    }

    public static function getCurrentMember() {
        return Member::model()->getByMemberID(Yii::app()->user->id);
    }

    /**
     * @param $institute
     * @return Member
     */
    public function getByCompanyID($institute) {
        return Member::model()->findAll('institute=:institute', array(':institute'=>$institute));
    }



    /**
     * @param $first_name
     * @param $last_name
     * @param $mobile_phone
     * @return Member
     */
    public function getByNameAndPhone($first_name, $last_name, $mobile_phone) {
        $criteria = new CDbCriteria;

        $criteria->compare("first_name", $first_name, false);
        $criteria->compare("last_name", $last_name, false);
        $criteria->compare("mobile_phone", $mobile_phone, false);

        return Member::model()->findAll($criteria);
    }

    public function getMemberID() {
        return $this->member_id;
    }

    static function compare($a, $b)
    {

        $user1split = explode(' ', strtolower(trim($a)));
        $user2split = explode(' ', strtolower(trim($b)));

        $user1 = end($user1split);
        $user2 = end($user2split);

        return strcmp($user1, $user2);

//        return strcmp(end(explode(' ', $a)), end(explode(' ', $b)));
    }

    /**
     * Takes in an array of names and sorts them by lastname
     *
     * @param $names
     * @return bool
     */
    public static function sortMemberNames($names) {

        $nodups = array_unique($names);

        uasort($nodups, array("Member", "compare"));

        return $nodups;
    }

    static function compareMember($memberA, $memberB)
    {
        if (!isset($memberA))
            return 1;
        if (!isset($memberB))
            return -1;

        $a = $memberA->getFullName();
        $b = $memberB->getFullName();

        $user1split = explode(' ', strtolower(trim($a)));
        $user2split = explode(' ', strtolower(trim($b)));

        $user1 = end($user1split);
        $user2 = end($user2split);

        return strcmp($user1, $user2);

        //        return strcmp(end(explode(' ', $a)), end(explode(' ', $b)));
    }

    /**
     * Takes in an array of names and sorts them by lastname
     *
     * @param $members
     * @return bool
     */
    public static function sortMembers($members, $retainIDs=true, $no_duplicates=true) {

        if ($no_duplicates)
            $nodups = array_unique($members);
        else
            $nodups=$members;

        if ($retainIDs)
            uasort($nodups, array("Member", "compareMember"));
        else
            usort($nodups, array("Member", "compareMember"));

        return $nodups;
    }


    public function getBriefContactHTML() {
        return TextFormat::getNiceContact(
            $this->first_name, $this->last_name, $this->title,$this->mobile_phone, $this->fax, $this->email);
    }

    public function getFullContactHTML() {
        return TextFormat::getNiceFullContact(
            $this->first_name, $this->last_name, $this->title, $this->work_phone, $this->extension, $this->mobile_phone, $this->fax, $this->email);
    }

    public function getSignature() {
         return TextFormat::getSignatureFor($this);
     }

    public static function getContactInfo(Member $member, $leftwidth=100) {
        return TextFormat::getContactInfo($member, $leftwidth);
    }



    public function beforeSave() {

        // A new record i.e. does not exist in the database.
        //   echo "Before save ...";

        if ($this->isNewRecord) {
            $bcrypt = new Bcrypt();
            $this->password = $bcrypt->hash($this->password);
        }

        $this->first_name = Yii::app()->encrypter->encrypt($this->first_name);
        $this->last_name= Yii::app()->encrypter->encrypt($this->last_name);
        $email_address = $this->email;
		$this->email= Yii::app()->encrypter->encrypt($email_address);
		$this->email_hash = hash( 'sha512', strtolower( $email_address ) );

        return parent::beforeSave();
    }

    public function afterSave() {
          $this->doDecrypt();
          return parent::afterSave();
      }


    private function doDecrypt() {
        $this->first_name = Yii::app()->encrypter->decrypt($this->first_name);
        $this->last_name= Yii::app()->encrypter->decrypt($this->last_name);
        $this->email= Yii::app()->encrypter->decrypt($this->email);
    }

    public function afterFind() {
        $this->doDecrypt();

        $role_obj= Role::model()->getByRoleID($this->role_id);

         if(!empty($role_obj))
             $this->role = $role_obj->name;

        return parent::afterFind();
    }

    public function getFullName() {
        if (isset($this->first_name))
            return $this->first_name." ".$this->last_name;
        else return "NA";
    }

    public function getFullNameAndUsername() {
        return $this->first_name." ".$this->last_name." (".$this->username.")";
    }



    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('member_id',$this->member_id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('institute',$this->institute);
        $criteria->compare('first_name',$this->first_name,true);
        $criteria->compare('last_name',$this->last_name,true);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('profile_picture_loc',$this->profile_picture_loc,true);

        $criteria->compare('date_registered',$this->date_registered,true);

        $criteria->compare('email',$this->email,true);
        $criteria->compare('active',$this->active,true);
        $criteria->compare('notes',$this->notes,true);
        $criteria->compare('role_id',$this->role_id,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>1000,
            ),
        ));
    }


    public function getPictureURL() {
           return Yii::app()->request->baseUrl."/member/getpicture/id/".$this->member_id;
       }

    public function getProfilePhotoFilePath() {

        if (!empty($this->profile_picture_loc)) {
            $dataPic_dir = DataDirectories::getCurrentMemberProfilePictureDirectory();

            $picloc = $dataPic_dir.$this->profile_picture_loc;

            //   echo $picloc."<br>";

            if (file_exists($picloc)) {
                return $picloc;
            }
        }

        return DataDirectories::getInternalImagesDirectory()."person.png";
    }


    public function getProfileThumbnail() {
        return ThumbnailGenerator::getThumbnailFor(DataDirectories::getUserProfilePictureDirectoryForMemberID($this->member_id), $this->profile_picture_loc);
    }

    /**
      * @param $email_address
      * @return Member
      */
     public function getByEmail( $email_address )
 	{
 		$value = hash( 'sha512', strtolower( $email_address ) );
         return Member::model()->find( 'email_hash=:email_hash', array( ':email_hash' => $value ) );
     }

}