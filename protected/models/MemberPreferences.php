<?php

/**
 * This is the model class for table "member_preferences".
 *
 * The followings are the available columns in table 'member_preferences':
 * @property integer $id
 * @property integer $member_id
 * @property string $currency_code
 * @property string $date_style
 * @property string $border_style
 * @property string $content_style
 * @property string $country_code
 * @property string $zone_name
 * @property int days_for_recurring_events
 */
class MemberPreferences extends CActiveRecord
{
    private static function getDefaultTimeZone() {
        return  "America/New_York";
    }

    private static function getDefaultCurrencyCode() {
          return  "USD";
      }
    private static function getDefaultDateStyle() {
          return  "D F j, Y, g:i a";
      }
    private static function getDefaultRecurringEventDays() {
          return  14;
      }

    public static function getDefaultBorder() {
         return GUIStyle::getDefaultBorder();
     }

     public static function getDefaultContent() {
         return GUIStyle::getDefaultContent();
     }

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MemberPreferences the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'member_preferences';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member_id, date_style', 'required'),
			array('member_id, days_for_recurring_events', 'numerical', 'integerOnly'=>true),
			array('currency_code', 'length', 'max'=>3),
			array('date_style', 'length', 'max'=>100),
            array('country_code', 'length', 'max'=>2),
            array('zone_name', 'length', 'max'=>35),
			array('border_style, content_style', 'length', 'max'=>50),
            array('currency_code, days_for_recurring_events, date_style, border_style, content_style', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, member_id, country_code, zone_name, currency_code, date_style, border_style, content_style', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

    /**
     * Default parameters for a member
     *
     * @param $member_id
     * @return MemberPreferences
     */
    public function getByMemberID($member_id) {
         $pref = MemberPreferences::model()->find('member_id=:member_id', array(':member_id'=>$member_id));

        if (!isset($pref)) {
            $model=new MemberPreferences();
            $model->member_id=$member_id;
            $model->border_style=MemberPreferences::getDefaultBorder();
            $model->content_style=MemberPreferences::getDefaultContent();
            $model->currency_code=MemberPreferences::getDefaultCurrencyCode();
            $model->date_style=MemberPreferences::getDefaultDateStyle();
            $model->days_for_recurring_events= MemberPreferences::getDefaultRecurringEventDays();
            $model->zone_name = MemberPreferences::getDefaultTimeZone();
            $pref=$model;
        }
        return $pref;
     }


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'currency_code' => 'Your Desired Currency',
			'date_style' => 'Date Style',
			'border_style' => 'Border Style',
			'content_style' => 'Content Style',
            'country_code' => 'Your Country',
            'zone_name' => 'Your Timezone',
            'days_for_recurring_events' => 'Days To View Recurring Events In Advance'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('currency_code',$this->currency_code,true);
		$criteria->compare('date_style',$this->date_style,true);
		$criteria->compare('border_style',$this->border_style,true);
		$criteria->compare('content_style',$this->content_style,true);
        $criteria->compare('country_code',$this->country_code,true);
        $criteria->compare('zone_name',$this->zone_name,true);
        $criteria->compare('days_for_recurring_events',$this->days_for_recurring_events,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}