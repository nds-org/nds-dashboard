<?php

/**
 * This is the model class for table "timezone".
 *
 * The followings are the available columns in table 'timezone':
 * @property integer $zone_id
 * @property string $abbreviation
 * @property integer $time_start
 * @property integer $gmt_offset
 * @property string $dst
 */
class Timezone extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Timezone the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'timezone';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('zone_id, abbreviation, time_start, gmt_offset, dst', 'required'),
            array('zone_id, time_start, gmt_offset', 'numerical', 'integerOnly'=>true),
            array('abbreviation', 'length', 'max'=>6),
            array('dst', 'length', 'max'=>1),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('zone_id, abbreviation, time_start, gmt_offset, dst', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /** @var $connection CDbConnection */

    private $connection;

    private function initDBConnection() {
        $this->connection=Yii::app()->db;   // assuming you have configured a "db" connection
        $this->connection->active=true;
    }

    private function closeDBConnection() {
        $this->connection->active=false;
    }


    /**
     * @param $cdbcommand
     * @return CDbCommand
     */
    private function getCommandCommand($cdbcommand) {
        return $this->connection->createCommand($cdbcommand);
    }



    /**
     * @param $sql
     * @return string
     */
    public function querysql($sql) {
        $this->initDBConnection();

        $command=$this->connection->createCommand($sql);

        $row =$command->queryRow();       // query and return the first row of result
        $this->closeDBConnection();
        return $row;
    }


    public function getZoneInfoFor($unix_timestamp=-1, $place="America/Los_Angeles") {
        if ($unix_timestamp == -1)
            $time = "UNIX_TIMESTAMP(UTC_TIMESTAMP())";
        else
            $time = strval($unix_timestamp);

        $sql = "SELECT z.country_code, z.zone_name, tz.abbreviation, tz.gmt_offset, tz.dst
          FROM `timezone` tz JOIN `zone` z
          ON tz.zone_id=z.zone_id
          WHERE tz.time_start < ".$time." AND z.zone_name='".$place."'
          ORDER BY tz.time_start DESC LIMIT 1";

        return $this->querysql($sql);
    }

    public function getLocalTimeFor($unix_timestamp, $place="America/Los_Angeles") {

        $sql = "SELECT FROM_UNIXTIME(".$unix_timestamp." + tz.gmt_offset, '%a, %d %b %Y, %H:%i:%s') AS local_time
                FROM `timezone` tz JOIN `zone` z
                ON tz.zone_id=z.zone_id
                WHERE tz.time_start < ".$unix_timestamp." AND z.zone_name='".trim($place)."'
                ORDER BY tz.time_start DESC LIMIT 1";

        $result= $this->querysql($sql);

        return strtotime($result['local_time']);
    }

    public function getUTCTimeFromLocal($unix_timestamp, $place="America/Los_Angeles") {

        $zone_info = $this->getZoneInfoFor($unix_timestamp, $place);

        return $unix_timestamp - $zone_info['gmt_offset'];
     }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'zone_id' => 'Zone',
            'abbreviation' => 'Abbreviation',
            'time_start' => 'Time Start',
            'gmt_offset' => 'Gmt Offset',
            'dst' => 'Dst',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('zone_id',$this->zone_id);
        $criteria->compare('abbreviation',$this->abbreviation,true);
        $criteria->compare('time_start',$this->time_start);
        $criteria->compare('gmt_offset',$this->gmt_offset);
        $criteria->compare('dst',$this->dst,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}